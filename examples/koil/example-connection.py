# This Python file uses the following encoding: utf-8

#if__name__ == "__main__":
from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QColor,QColorConstants
from dolomites import koil
from dolomites.koil_ui import *
import sys


wind = koil.m_phase_winding()
# wind_b.compute_winding(3,12,5,single_layer=False)
# wind.compute_winding(3,36,2,single_layer=False)
wind.compute_winding(3,24,4,single_layer=False)
wind.star.plot()

# exit()
if __name__ == "__main__":
    app = QApplication([])
    window = koil_widget()
    colors=[QColor(QColorConstants.Svg.steelblue),
            QColor(QColorConstants.Svg.firebrick),
            QColor(QColorConstants.Svg.darkorange)]
    window.draw_winding(wind,colors)
    window.add_connection(4,7,2,1,200,colors[1])
    window.add_connection(10,13,2,1,200,colors[1])
    window.add_connection(16,19,2,1,200,colors[1])

    window.add_connection(6,9,2,1,220,colors[2])
    window.add_connection(12,15,2,1,220,colors[2])
    window.add_connection(18,21,2,1,220,colors[2])

    # window.clear_empty_slots()

    window.show()
    sys.exit(app.exec())

# This Python file uses the following encoding: utf-8

#if__name__ == "__main__":
from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QColor,QColorConstants
from dolomites import koil
from dolomites.koil_ui import *
import sys

Q = 36 # define the number of slots
p = 2  # define the number of pole pairs

# add two phases
wa = koil.winding(Q,p)
wb = koil.winding(Q,p)
wc = koil.winding(Q,p)

# add two coils to phase 'a'
#
wa.add_coil(koil.coil(1,12,1))
wa.add_coil(koil.coil(2,11,1))
wa.add_coil(koil.coil(3,10,1))
wa.add_coil(koil.coil(19,30,1))
wa.add_coil(koil.coil(20,29,1))
wa.add_coil(koil.coil(21,28,1))

print(wa.coils)




win = koil.m_phase_winding()
win.add_winding(wa)

# exit()
if __name__ == "__main__":
    app = QApplication([])
    window = koil_widget()
    colors=[QColor(QColorConstants.Svg.steelblue),
            QColor(QColorConstants.Svg.firebrick),
            QColor(QColorConstants.Svg.darkorange)]
    window.draw_winding(win,colors)
    window.add_connection(12,2,2,1,200,colors[0])
    window.add_connection(11,3,2,1,220,colors[0])
    window.add_connection(10,19,2,1,220,colors[0])
    # window.add_connection(10,13,2,1,200,colors[1])
    # window.add_connection(16,19,2,1,200,colors[1])
    #
    # window.add_connection(6,9,2,1,220,colors[2])
    # window.add_connection(12,15,2,1,220,colors[2])
    # window.add_connection(18,21,2,1,220,colors[2])

    # window.clear_empty_slots()

    window.show()
    sys.exit(app.exec())

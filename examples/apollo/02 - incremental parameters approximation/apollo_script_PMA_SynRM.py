'''
apollo_script_PMA-SynRM.py
(https://gitlab.com/LuigiAlberti/dolomites-python)

Fast Evaluation of Self-Sensing Capability of Synchronous Machines (PMA-SynRM):
reconstruct the convergence region of the motor from few samples of the inverse incremental inductances [1].
'''
# References:

# [1] M. Berto, L. Alberti and A. Maimeri,
# "Fast Evaluation of Self-Sensing Capability of Synchronous Machines,"
# in IEEE Transactions on Industrial Electronics (Early Access),
# https://doi.org/10.1109/TIE.2024.3497315


## import some libraries
import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy.interpolate import RectBivariateSpline
from scipy.optimize import curve_fit


## import apollo
from dolomites import apollo

## load the data from a file
file_name = 'PMA_SynRM_data.txt'
data = pd.read_csv(file_name, sep=None, engine='python', names=["theta_m", "i_d", "i_q", "lambda_d", "lambda_q", "torque"])
mot = apollo.fm(data)
mot.create_maps(_theta_m=0)

## compute the incremental inductances mot.ldd, mot.ldq, mot.lqd, mot.lqd, mot.lqq, mot.lsigma, mot.ldelta
mot.calc_incremental_inductances(method = 'gradient')

## Compute the mtpa trajectory
mot.calc_MTPA(method = "analytical")

## compute estimation error mot.epsilon
mot.calc_sensored_error()

## set the MTPA as reference trajectory
mot.i_d_REF = mot.i_d_MTPA
mot.i_q_REF = mot.i_q_MTPA

## compute sensored trajectory t1
mot.calc_sensored_trajectory()

## compute convergence region
Uh=40
fh=1000
mot.calc_convergence_region(Uh, fh)

## compute the inverse inductances
mot.calc_inverse_incremental_inductances() # mot.gamma_dq, mot.gamma_delta



## Fourier at In/10 = 0.6 A (extract dominant harmonics)
mot.calc_fourier_inverse_inductances(I=0.6, plot=False)
mot.calc_Ihq_few_points(I=0.6,plot=False)
## extract gamma delta
gamma_delta_polar_A = mot.gamma_delta_polar_I
gamma_delta_90_A = np.interp([90], mot.alpha_breakpoints*180/np.pi, mot.gamma_delta_polar_I)
gamma_delta_135_A = np.interp([135], mot.alpha_breakpoints*180/np.pi, mot.gamma_delta_polar_I)
a0_delta_A = mot.gamma_delta_a0
a2_delta_A = mot.gamma_delta_a2
## extract gamma dq
gamma_dq_polar_A = mot.gamma_dq_polar_I
gamma_dq_90_A = np.interp([90], mot.alpha_breakpoints*180/np.pi, mot.gamma_dq_polar_I)
gamma_dq_135_A = np.interp([135], mot.alpha_breakpoints*180/np.pi, mot.gamma_dq_polar_I)
b1_dq_A = mot.gamma_dq_b1
b2_dq_A = mot.gamma_dq_b2
## extract Ihq
Ihq_polar_A = mot.Ihq_polar_I
Ihq_fourier_2meas_A = mot.Ihq_fourier_2meas
theta_REF_polar_A = np.interp(0.6, mot.i_REF, mot.theta_REF)


## Fourier at In/2 = 3 A (extract dominant harmonics)
mot.calc_fourier_inverse_inductances(I=3, plot=False)
mot.calc_Ihq_few_points(I=3,plot=False)
## extract gamma delta
gamma_delta_polar_B = mot.gamma_delta_polar_I
gamma_delta_90_B = np.interp([90], mot.alpha_breakpoints*180/np.pi, mot.gamma_delta_polar_I)
gamma_delta_135_B = np.interp([135], mot.alpha_breakpoints*180/np.pi, mot.gamma_delta_polar_I)
a0_delta_B = mot.gamma_delta_a0
a2_delta_B = mot.gamma_delta_a2
## extract gamma dq
gamma_dq_polar_B = mot.gamma_dq_polar_I
gamma_dq_90_B = np.interp([90], mot.alpha_breakpoints*180/np.pi, mot.gamma_dq_polar_I)
gamma_dq_135_B = np.interp([135], mot.alpha_breakpoints*180/np.pi, mot.gamma_dq_polar_I)
b1_dq_B = mot.gamma_dq_b1
b2_dq_B = mot.gamma_dq_b2
## extract Ihq
Ihq_polar_B = mot.Ihq_polar_I
Ihq_fourier_2meas_B = mot.Ihq_fourier_2meas
theta_REF_polar_B = np.interp(3, mot.i_REF, mot.theta_REF)


## Fourier at In = 6 A (extract dominant harmonics)
mot.calc_fourier_inverse_inductances(I=6, plot=False)
mot.calc_Ihq_few_points(I=6,plot=False)
## extract gamma delta
gamma_delta_polar_C = mot.gamma_delta_polar_I
gamma_delta_90_C = np.interp([90], mot.alpha_breakpoints*180/np.pi, mot.gamma_delta_polar_I)
gamma_delta_135_C = np.interp([135], mot.alpha_breakpoints*180/np.pi, mot.gamma_delta_polar_I)
a0_delta_C = mot.gamma_delta_a0
a2_delta_C = mot.gamma_delta_a2
## extract gamma dq
gamma_dq_polar_C = mot.gamma_dq_polar_I
gamma_dq_90_C = np.interp([90], mot.alpha_breakpoints*180/np.pi, mot.gamma_dq_polar_I)
gamma_dq_135_C = np.interp([135], mot.alpha_breakpoints*180/np.pi, mot.gamma_dq_polar_I)
b1_dq_C = mot.gamma_dq_b1
b2_dq_C = mot.gamma_dq_b2
## extract Ihq
Ihq_polar_C = mot.Ihq_polar_I
Ihq_fourier_2meas_C = mot.Ihq_fourier_2meas
theta_REF_polar_C = np.interp(6, mot.i_REF, mot.theta_REF)


## gamma delta plot
fig, axs = plt.subplots(3)
axs[0].plot(mot.alpha_breakpoints*180/np.pi, gamma_delta_polar_A, 'k--', label="$\\gamma_\\Delta$ Actual at 10%In")
axs[0].plot(mot.alpha_breakpoints*180/np.pi, a0_delta_A + a2_delta_A*np.cos(2*mot.alpha_breakpoints), 'k', label="$\\gamma_\\Delta$ Approximation at 10%In")
axs[0].plot(90, gamma_delta_90_A, 'r*', linewidth=2)
axs[0].plot(135, gamma_delta_135_A, 'r*', linewidth=2)
axs[0].set(xlabel='alpha (deg el)', ylabel='$\\gamma_\\Delta (H^{-1})$')
axs[0].legend(loc="upper right")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, gamma_delta_polar_B, 'k--', label="$\\gamma_\\Delta$ Actual at 50%In")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, a0_delta_B + a2_delta_B*np.cos(2*mot.alpha_breakpoints), 'k', label="$\\gamma_\\Delta$ Approximation at 50%In")
axs[1].plot(90, gamma_delta_90_B, 'r*', linewidth=2)
axs[1].plot(135, gamma_delta_135_B, 'r*', linewidth=2)
axs[1].set(xlabel='alpha (deg el)', ylabel='$\\gamma_\\Delta (H^{-1})$')
axs[1].legend(loc="upper right")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, gamma_delta_polar_C, 'k--', label="$\\gamma_\\Delta$ Actual at In")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, a0_delta_C + a2_delta_C*np.cos(2*mot.alpha_breakpoints), 'k', label="$\\gamma_\\Delta$ Approximation at In")
axs[2].plot(90, gamma_delta_90_C, 'r*', linewidth=2)
axs[2].plot(135, gamma_delta_135_C, 'r*', linewidth=2)
axs[2].set(xlabel='alpha (deg el)', ylabel='$\\gamma_\\Delta (H^{-1})$')
axs[2].legend(loc="upper right")
plt.show()


## gamma dq plot
fig, axs = plt.subplots(3)
axs[0].plot(mot.alpha_breakpoints*180/np.pi, gamma_dq_polar_A, 'k--', label="$\\gamma_{dq}$ Actual at 10%In")
axs[0].plot(mot.alpha_breakpoints*180/np.pi, b1_dq_A*np.sin(mot.alpha_breakpoints) + b2_dq_A*np.sin(2*mot.alpha_breakpoints), 'k', label="$\\gamma_{dq}$ Approximation at 10%In")
axs[0].plot(90, gamma_dq_90_A, 'r*', linewidth=2)
axs[0].plot(135, gamma_dq_135_A, 'r*', linewidth=2)
axs[0].set(xlabel='alpha (deg el)', ylabel='$\\gamma_{dq} (H^{-1})$')
axs[0].legend(loc="upper right")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, gamma_dq_polar_B, 'k--', label="$\\gamma_{dq}$ Actual at 50%In")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, b1_dq_B*np.sin(mot.alpha_breakpoints) + b2_dq_B*np.sin(2*mot.alpha_breakpoints), 'k', label="$\\gamma_{dq}$ Approximation at 50%In")
axs[1].plot(90, gamma_dq_90_B, 'r*', linewidth=2)
axs[1].plot(135, gamma_dq_135_B, 'r*', linewidth=2)
axs[1].set(xlabel='alpha (deg el)', ylabel='$\\gamma_{dq} (H^{-1})$')
axs[1].legend(loc="upper right")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, gamma_dq_polar_C, 'k--', label="$\\gamma_{dq}$ Actual at In")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, b1_dq_C*np.sin(mot.alpha_breakpoints) + b2_dq_C*np.sin(2*mot.alpha_breakpoints), 'k', label="$\\gamma_{dq}$ Approximation at In")
axs[2].plot(90, gamma_dq_90_C, 'r*', linewidth=2)
axs[2].plot(135, gamma_dq_135_C, 'r*', linewidth=2)
axs[2].set(xlabel='alpha (deg el)', ylabel='$\\gamma_{dq} (H^{-1})$')
axs[2].legend(loc="upper right")
plt.show()


## Ihq plot
fig, axs = plt.subplots(3)
axs[0].plot(mot.alpha_breakpoints*180/np.pi, Ihq_polar_A*1000, 'k--', label="$I_{hq}$ Actual at 10%In")
axs[0].plot(mot.alpha_breakpoints*180/np.pi, Ihq_fourier_2meas_A*1000, 'k', label="$I_{hq}$ Approximation at 10%In")
axs[0].plot(mot.alpha_breakpoints*180/np.pi, mot.alpha_breakpoints*0, 'k', linewidth=0.5)
axs[0].plot(theta_REF_polar_A*180/np.pi, 0, 'r.', linewidth=2)
axs[0].set(xlabel='alpha (deg el)', ylabel='$I_{hq} (mA)$')
axs[0].legend(loc="upper right")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, Ihq_polar_B*1000, 'k--', label="$I_{hq}$ Actual at 50%In")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, Ihq_fourier_2meas_B*1000, 'k', label="$I_{hq}$ Approximation at 50%In")
axs[1].plot(mot.alpha_breakpoints*180/np.pi, mot.alpha_breakpoints*0, 'k', linewidth=0.5)
axs[1].plot(theta_REF_polar_B*180/np.pi, 0, 'r.', linewidth=2)
axs[1].set(xlabel='alpha (deg el)', ylabel='$I_{hq} (mA)$')
axs[1].legend(loc="upper right")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, Ihq_polar_C*1000, 'k--', label="$I_{hq}$ Actual at In")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, Ihq_fourier_2meas_C*1000, 'k', label="$I_{hq}$ Approximation at In")
axs[2].plot(mot.alpha_breakpoints*180/np.pi, mot.alpha_breakpoints*0, 'k', linewidth=0.5)
axs[2].plot(theta_REF_polar_C*180/np.pi, 0, 'r.', linewidth=2)
axs[2].set(xlabel='alpha (deg el)', ylabel='$I_{hq} (mA)$')
axs[2].legend(loc="upper right")
plt.show()



# ## Consistently with the proposed method rationale, the samples of the inverse incremental inductances could have been collected
# ## without any Fourier analyis. In the following, the samples are collected interpolating the available maps.
# ## In a real application, they could have been collected by granular measurements only on the few target points.
# gamma_delta_interp_spline = RectBivariateSpline(mot.i_d[:,0], mot.i_q[0,:], mot.gamma_delta)
# gamma_delta_90_A = gamma_delta_interp_spline(0, 0.6)[0]
# gamma_delta_90_B = gamma_delta_interp_spline(0, 3)[0]
# gamma_delta_90_C = gamma_delta_interp_spline(0, 6)[0]
# gamma_delta_135_A = gamma_delta_interp_spline(-0.6*np.sqrt(2)/2, 0.6*np.sqrt(2)/2)[0]
# gamma_delta_135_B = gamma_delta_interp_spline(  -3*np.sqrt(2)/2,   3*np.sqrt(2)/2)[0]
# gamma_delta_135_C = gamma_delta_interp_spline(  -6*np.sqrt(2)/2,   6*np.sqrt(2)/2)[0]
# gamma_dq_interp_spline = RectBivariateSpline(mot.i_d[:,0], mot.i_q[0,:], mot.gamma_dq)
# gamma_dq_90_A = gamma_dq_interp_spline(0, 0.6)[0]
# gamma_dq_90_B = gamma_dq_interp_spline(0, 3)[0]
# gamma_dq_90_C = gamma_dq_interp_spline(0, 6)[0]
# gamma_dq_135_A = gamma_dq_interp_spline(-0.6*np.sqrt(2)/2, 0.6*np.sqrt(2)/2)[0]
# gamma_dq_135_B = gamma_dq_interp_spline(  -3*np.sqrt(2)/2,   3*np.sqrt(2)/2)[0]
# gamma_dq_135_C = gamma_dq_interp_spline(  -6*np.sqrt(2)/2,   6*np.sqrt(2)/2)[0]
# ## compute the Fuorier coefficients
# a0_delta_A = gamma_delta_135_A[0]
# a0_delta_B = gamma_delta_135_B[0]
# a0_delta_C = gamma_delta_135_C[0]
# a2_delta_A = gamma_delta_135_A[0] - gamma_delta_90_A[0]
# a2_delta_B = gamma_delta_135_B[0] - gamma_delta_90_B[0]
# a2_delta_C = gamma_delta_135_C[0] - gamma_delta_90_C[0]
# b1_dq_A = gamma_dq_90_A[0]
# b1_dq_B = gamma_dq_90_B[0]
# b1_dq_C = gamma_dq_90_C[0]
# b2_dq_A = -gamma_dq_135_A[0] + gamma_dq_90_A[0]*np.sin(3*np.pi/4)
# b2_dq_B = -gamma_dq_135_B[0] + gamma_dq_90_B[0]*np.sin(3*np.pi/4)
# b2_dq_C = -gamma_dq_135_C[0] + gamma_dq_90_C[0]*np.sin(3*np.pi/4)



## fit the Fourier coefficients to get the slopes, the vertical intercepts and the coefficients of the polynomial

def polynomial(I, r,s,t):
    return np.array(r*I**2 + s*I + t)
    
def linear(I, m,s):
    return np.array(m*I + s)

coef_a0delta_rec, pcov1 = curve_fit(polynomial, [0.6, 3, 6], [a0_delta_A, a0_delta_B, a0_delta_C])
a0delta_r=coef_a0delta_rec[0] # coefficient of the second-order polynomial
a0delta_s=coef_a0delta_rec[1] # coefficient of the second-order polynomial
a0delta_t=coef_a0delta_rec[2] # coefficient of the second-order polynomial

coef_a2delta_rec, pcov1 = curve_fit(linear, [0.6, 3, 6], [a2_delta_A, a2_delta_B, a2_delta_C])
a2delta_m = coef_a2delta_rec[0] # slope of a2delta
a2delta_s = coef_a2delta_rec[1] # vertical intercept of a2delta

coef_b1dq_rec, pcov1 = curve_fit(linear, [0.6, 3, 6], [b1_dq_A, b1_dq_B, b1_dq_C])
b1dq_m = coef_b1dq_rec[0] # slope of b1dq_rec
b1dq_s = coef_b1dq_rec[1] # vertical intercept of b1dq_rec

coef_b2dq_rec, pcov1 = curve_fit(linear, [0.6, 3, 6], [b2_dq_A, b2_dq_B, b2_dq_C])
b2dq_m = coef_b2dq_rec[0] # slope of b2dq_rec
b2dq_s = coef_b2dq_rec[1] # vertical intercept of b2dq_rec


## create the target polar grid
alpha_ie_breakpoints = np.linspace(start=0, stop=359, num=360)
I_breakpoints = np.linspace(start=0, stop=8, num=51)
alpha_ie_grid, I_grid = np.meshgrid(alpha_ie_breakpoints, I_breakpoints, indexing='ij')

## evaluate and reshape the Fourier coefficients on the polar grid
a0delta = polynomial(I_breakpoints, a0delta_r, a0delta_s, a0delta_t)
a2delta = linear(I_breakpoints, a2delta_m, a2delta_s)
b1dq = linear(I_breakpoints, b1dq_m, b1dq_s)
b2dq = linear(I_breakpoints, b2dq_m, b2dq_s)

a0delta_polar_grid = np.tile(a0delta, (alpha_ie_breakpoints.size,1))
a2delta_polar_grid = np.tile(a2delta, (alpha_ie_breakpoints.size,1))
b1dq_polar_grid = np.tile(b1dq, (alpha_ie_breakpoints.size,1))
b2dq_polar_grid = np.tile(b2dq, (alpha_ie_breakpoints.size,1))

## convert the Fourier coefficients from polar to cartesian grid
a0delta_grid = apollo.polar_to_cartesian(alpha_ie_grid, I_grid, a0delta_polar_grid, mot.i_d, mot.i_q)
a2delta_grid = apollo.polar_to_cartesian(alpha_ie_grid, I_grid, a2delta_polar_grid, mot.i_d, mot.i_q)
b1dq_grid = apollo.polar_to_cartesian(alpha_ie_grid, I_grid, b1dq_polar_grid, mot.i_d, mot.i_q)
b2dq_grid = apollo.polar_to_cartesian(alpha_ie_grid, I_grid, b2dq_polar_grid, mot.i_d, mot.i_q)

## reconstruct the inverse incremental inductances in the dq plane
gamma_delta_rec = a0delta_grid + a2delta_grid*np.cos(2*mot.alpha_ie)
gamma_dq_rec    = b1dq_grid*np.sin(mot.alpha_ie) + b2dq_grid*np.sin(2*mot.alpha_ie)

## reconstruct the current signal Ihq
Ihq_rec = mot.Uh/(2*np.pi*mot.fh) * (-gamma_delta_rec*np.sin(2*mot.delta_theta) + gamma_dq_rec*np.cos(2*mot.delta_theta))


## FIND Ihq=0 WITH NEGATIVE SLOPE (convergence region computation)

# calc Ihq_dt, tangential derivative (with respect to angle) of Ihq
Ihq_rec_dx = np.gradient(Ihq_rec, mot.i_d[:,0], axis=0)
Ihq_rec_dy = np.gradient(Ihq_rec, mot.i_q[0,:], axis=1)
Ihq_rec_d_alpha_ie = - Ihq_rec_dx * mot.i_q + Ihq_rec_dy * mot.i_d

## extract Ihq locus with negative slope
Ihq_rec_neg = copy.deepcopy(Ihq_rec)
Ihq_rec_neg[Ihq_rec_d_alpha_ie > 0] = np.nan



# legend
# black: reference trajectory REF
# yellow: sensored trajectory t1
# blue: sensorless t2: continuous where Ihq=0 with negative slope (convergence region) and dashed where Ihq=0 with positive slope
# cyan: reconstructed (approximated) t2
fig, ax = plt.subplots()
ax.plot(mot.i_d_REF, mot.i_q_REF, 'k', label='REF',linewidth=2.0)
ax.plot(mot.i_d_sensored, mot.i_q_sensored, 'y', label='t1',linewidth=2.0)
ax.contour(mot.i_d, mot.i_q, mot.Ihq, levels=[0], colors='b', linestyles='dashed')
ax.contour(mot.i_d, mot.i_q, mot.Ihq_neg, levels=[0], colors='b',linewidths=2.5)
ax.contour(mot.i_d, mot.i_q, Ihq_rec, levels=[0], colors='cyan', linestyles='dashed')
ax.contour(mot.i_d, mot.i_q, Ihq_rec_neg, levels=[0], colors='cyan',linewidths=2.5)
plt.xlabel('$i_d$ (A)')
plt.ylabel('$i_q$ (A)')
ax.set_aspect('equal')
plt.title('Convergence region plot')
plt.legend(loc="lower right")
plt.grid(linewidth=0.2)
plt.show()

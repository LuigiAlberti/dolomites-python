Install Python, Jupyter Notebook, and dolomites-python following the installation steps in:
https://gitlab.com/LuigiAlberti/dolomites-python

Once finished the installation, run the examples in folder "00 - ideal motors" with:
py apollo-example-IPMSM-ideal.py
py apollo-example-SynRM-ideal.py
py -m notebook apollo-test-ideal-motor.ipynb

Feel free to edit the scripts.
Motor maps and results can be exported using the following functions:
self.save_maps()
apollo.export_2d_data()
apollo.export_gnuplot_1d_data()
apollo.export_gnuplot_2d_data()

You are encouraged to view the source code of apollo, which can be found in:
dolomites-python-master/dolomites/apollo.py

You are also encouraged to run the examples in folder "01 - computation on real motors" 
which deal with real motor data.

Motor data (from FEA or measurements) are stored in a .txt file with Tab-separated values as follows:
1st Column: rotor position (mechanical degrees)
2nd Column: d-axis current (A peak)
3rd Column: q-axis current (A peak)
4th Column: d-axis flux linkage (Vs)
5th Column: q-axis flux linkage (Vs)
6th Column: Maxwell/measured torque (Nm)

This repository is intended to share the findings and source data of our research with the academic community.
In the headings of each example (.py or .ipynb script), you will find the relevant reference paper.
We sincerely hope that your research on electric drives may gain valuable insights from this.
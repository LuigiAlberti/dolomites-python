'''
apollo-example-SynRM-ideal.py
using dolomites-apollo to study an ideal motor (SynRM)
https://gitlab.com/LuigiAlberti/dolomites-python
'''

# import some libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# import apollo
from dolomites import apollo

# d-q current grid
Id_min = -10; Id_max = 10; n_Id = 151
Iq_min = -10; Iq_max = 10; n_Iq = 131

# the motor (constant) parameters
Ld = 30e-3
Lq = 2e-3
lambda_PM = 0
p = 2

# create maps of an ideal SynRM (no saturation)
data = apollo.create_dataframe_SynRM_ideal(Ld, Lq, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq)

mot = apollo.fm(data)
mot.create_maps(0)  # we have defined only data for theta_m = 0

# 3d-plot the flux-linkage and torque characteristics (loaded data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lambda_d)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lambda_q)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
plt.show()

torque_dq = 3/2*p*(mot.lambda_d*mot.i_q-mot.lambda_q*mot.i_d)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, torque_dq)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'torque (Nm)', rotation=90)
plt.show()


# compute the incremental inductances mot.ldd, mot.ldq, mot.lqd, mot.lqd, mot.lqq, mot.lsigma, mot.ldelta
mot.calc_incremental_inductances(method = 'gradient')


# Compute the mtpa trajectory
# method: choose between "gradient" and "analytical"
mot.calc_MTPA(method = "analytical")


# compute saliency mot.xi and estimation error mot.epsilon
mot.calc_saliency()
mot.calc_sensored_error()


# set the MTPA as reference trajectory
mot.i_d_REF = mot.i_d_MTPA
mot.i_q_REF = mot.i_q_MTPA


# compute sensored trajectory t1
mot.calc_sensored_trajectory()

# compute convergence region
Uh = 40
fh = 1000
mot.calc_convergence_region(Uh, fh)


# legend
# black: reference trajectory REF
# yellow: sensored trajectory t1
# blue: Ihq=0 with negative slope (convergence region)
# red: Ihq=0 with poisitive slope (no convergence)
fig, ax = plt.subplots()
ax.plot(mot.i_d_REF, mot.i_q_REF, 'k', label='REF')
ax.plot(mot.i_d_sensored, mot.i_q_sensored, 'y', label='t1')
ax.contour(mot.i_d, mot.i_q, mot.Ihq, levels=[0], colors='r', linestyles='dashed')
ax.contour(mot.i_d, mot.i_q, mot.Ihq_neg, levels=[0], colors='b')
plt.xlabel(r'$i_d$ (A)')
plt.ylabel(r'$i_q$ (A)')
ax.set_aspect('equal')
plt.title('Convergence region plot')
plt.legend(loc="lower right")
plt.grid(linewidth=0.2)
plt.show()
# LEGEND:
# black: reference trajectory REF
# yellow: sensored trajectory t1
# blue: Ihq=0 with negative slope (convergence region)
# red: Ihq=0 with positive slope (no convergence)

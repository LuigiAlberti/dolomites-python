'''
apollo-test-SynRM.py
(https://gitlab.com/LuigiAlberti/dolomites-python)

In this script, simulated SynRM flux linkage maps are post-processed using apollo.
Inductances, MTPA and self-sensing capabilities are computed according to [1].
SynRM flux linkage maps have been obtained with FEMM simulations.
Maxwell torque is available.
'''

# References:

# [1] M. Berto, L. Alberti, V. Manzolini and S. Bolognani,
# "Computation of Self-Sensing Capabilities of Synchronous Machines for Rotating High Frequency Voltage Injection Sensorless Control,"
# in IEEE Transactions on Industrial Electronics, vol. 69, no. 4, pp. 3324-3333, April 2022,
# https://doi.org/10.1109/TIE.2021.3071710


# import some libraries
import pandas as pd
import matplotlib.pyplot as plt

# import apollo
from dolomites import apollo

# load the data from a file
file_name = 'SynRM_data.txt'

data = pd.read_csv(file_name, sep=None, engine='python', names=["theta_m", "i_d", "i_q", "lambda_d", "lambda_q", "torque"])

mot = apollo.fm(data)
mot.create_maps()




# 3d-plot the flux-linkage and torque characteristics (loaded data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lambda_d)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lambda_q)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.torque)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'torque (Nm)', rotation=90)
plt.show()



# compute the apparent inductances mot.Ld and mot.Lq
mot.calc_apparent_inductances()

# compute the incremental inductances mot.ldd, mot.ldq, mot.lqd, mot.lqd, mot.lqq, mot.lsigma, mot.ldelta
mot.calc_incremental_inductances(method = 'gradient')


# Compute the mtpa trajectory
# method: choose between "gradient" and "analytical"
mot.calc_MTPA(method = "analytical")

# # Plot the MTPA trajectory
# plt.plot(mot.i_d_MTPA, mot.i_q_MTPA, 'k')
# plt.gca().set_aspect('equal', adjustable='box')
# plt.xlabel(r'$i_d$ (A)'); plt.ylabel(r'$i_q$ (A)')
# plt.show()

# import numpy as np
# plt.plot(mot.i_MTPA, mot.theta_MTPA*180/np.pi, 'k')
# plt.xlabel(r'Stator current amplitude (A)'); plt.ylabel(r'Stator current angle (deg)')
# plt.show()


# compute saliency mot.xi and estimation error mot.epsilon
mot.calc_saliency()
mot.calc_sensored_error()


# set the MTPA as reference trajectory
mot.i_d_REF = mot.i_d_MTPA
mot.i_q_REF = mot.i_q_MTPA


# compute sensored trajectory t1
mot.calc_sensored_trajectory()

# compute convergence region
Uh = 40
fh = 1000
mot.calc_convergence_region(Uh, fh)


# legend
# black: reference trajectory REF
# yellow: sensored trajectory t1
# blue: Ihq=0 with negative slope (convergence region)
# red: Ihq=0 with poisitive slope (no convergence)
fig, ax = plt.subplots()
ax.plot(mot.i_d_REF, mot.i_q_REF, 'k', label='REF')
ax.plot(mot.i_d_sensored, mot.i_q_sensored, 'y', label='t1')
ax.contour(mot.i_d, mot.i_q, mot.Ihq, levels=[0], colors='r', linestyles='dashed')
ax.contour(mot.i_d, mot.i_q, mot.Ihq_neg, levels=[0], colors='b')
plt.xlabel(r'$i_d$ (A)')
plt.ylabel(r'$i_q$ (A)')
ax.set_aspect('equal')
#ax.set_xlim(-6, 6)
#ax.set_ylim(-6, 6)
plt.title('Convergence region plot')
plt.legend(loc="lower right")
plt.grid(linewidth=0.2)
plt.show()
# LEGEND:
# black: reference trajectory REF
# yellow: sensored trajectory t1
# blue: Ihq=0 with negative slope (convergence region)
# red: Ihq=0 with positive slope (no convergence)


# # Ihq 3D surface plot
# from matplotlib import cm
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.plot_surface(mot.i_d, mot.i_q, mot.Ihq*1000, cmap=cm.coolwarm, lw=0, rstride=1, cstride=1)
# ax.view_init(elev=75, azim=-135)
# ax.contour(mot.i_d, mot.i_q, mot.Ihq, levels=[0], colors="k")
# plt.xlabel(r'$i_d$ (A)')
# plt.ylabel(r'$i_q$ (A)')
# ax.zaxis.set_rotate_label(False)
# ax.set_zlabel(r'$I_{hq}$ (mA)', rotation=90)
# plt.show()


# # OPTIONAL: compute the inverse incremental inductances gamma
# mot.calc_inverse_incremental_inductances()



# # OPTIONAL: export maps for gnuplot
# from pathlib import Path
# Path("gnuplot").mkdir(parents=True, exist_ok=True)
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.lambda_d, 'gnuplot/FluxD.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.lambda_q, 'gnuplot/FluxQ.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.Ld, 'gnuplot/Ld.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.Lq, 'gnuplot/Lq.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.ldd, 'gnuplot/ldd.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.ldq, 'gnuplot/ldq.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.lqd, 'gnuplot/lqd.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.lqq, 'gnuplot/lqq.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.lsigma, 'gnuplot/lsigma.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.ldelta, 'gnuplot/ldelta.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.torque, 'gnuplot/torque.txt')
# apollo.export_gnuplot_1d_data(mot.i_d_MTPA, mot.i_q_MTPA, 'gnuplot/MTPA_cartesian.txt')
# apollo.export_gnuplot_1d_data(mot.i_MTPA, mot.theta_MTPA, 'gnuplot/MTPA_polar.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.xi, 'gnuplot/xi.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.epsilon_deg, 'gnuplot/epsilon_deg.txt')
# apollo.export_gnuplot_1d_data(mot.i_d_REF, mot.i_q_REF, 'gnuplot/REF_cartesian.txt')
# apollo.export_gnuplot_1d_data(mot.i_d_sensored, mot.i_q_sensored, 'gnuplot/sensored.txt')
# apollo.export_gnuplot_contour_0_data(mot.i_d, mot.i_q, mot.Ihq, 'gnuplot/Ihq.txt')
# apollo.export_gnuplot_contour_0_data(mot.i_d, mot.i_q, mot.Ihq_neg, 'gnuplot/Ihq_neg.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.gamma_dd, 'gnuplot/gamma_dd.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.gamma_dq, 'gnuplot/gamma_dq.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.gamma_qd, 'gnuplot/gamma_qd.txt')
# apollo.export_gnuplot_2d_data(mot.i_d, mot.i_q, mot.gamma_qq, 'gnuplot/gamma_qq.txt')



# # OPTIONAL: plot HF ellipses
# # (current response to rotating voltage injection)
# import numpy as np
# x = np.linspace(0, 6, 13) # set i_d query points
# y = np.linspace(0, 6, 17) # set i_q query points
# Uh = 40   # set the amplitude of the HF voltage injection
# fh = 1000 # set the frequency of the HF voltage injection
# k = 1     # set a scaling factor (optional)
# # call plot_ellipses function
# ellipse_plot = mot.plot_ellipses(x, y, Uh, fh, k)
# plt.xlabel(r'$i_d$ (A)')
# plt.ylabel(r'$i_q$ (A)')
# plt.show()

'''
apollo_script_WEMDCD.py
(https://gitlab.com/LuigiAlberti/dolomites-python)

The proposed method allows the reconstruction of the incremental inductances maps starting
from just the knowledge of the flux linkages along the current domain borders [1].
'''

# References:

# [1] M. Berto, L. Ortombina, L. Alberti,
# "Inductance and Self-Sensing Capabilities Computation for Synchronous Reluctance Motors based on Coenergy Model,"
# submitted to 2025 IEEE Workshop on Electrical Machines Design, Control and Diagnosis (WEMDCD),
# conference to be held at Valletta, Malta, between 9-10 April 2025


# import some libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# import apollo
from dolomites import apollo

# load the data from a file
file_name = 'SynRM_data.txt'

data = pd.read_csv(file_name, sep=None, engine='python', names=["theta_m", "i_d", "i_q", "lambda_d", "lambda_q", "torque"])

mot = apollo.fm(data)
mot.create_maps()


# 3d-plot the flux-linkage characteristics (loaded data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lambda_d)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lambda_q)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
plt.show()


# compute the incremental inductances mot.ldd, mot.ldq, mot.lqd, mot.lqd, mot.lqq
mot.calc_incremental_inductances(method = 'gradient')
mot.ldq = np.clip(mot.ldq, a_min=None, a_max=0)
mot.lqd = np.clip(mot.lqd, a_min=None, a_max=0)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.ldd)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'ldd (H)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.ldq)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'ldq (H)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lqd)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'lqd (H)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.lqq)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'lqq (H)', rotation=90)
plt.show()


# Compute the mtpa trajectory
mot.calc_MTPA(method = "analytical")

# compute estimation error mot.epsilon
mot.calc_sensored_error()

# set the MTPA as reference trajectory
mot.i_d_REF = mot.i_d_MTPA
mot.i_q_REF = mot.i_q_MTPA

# compute sensored trajectory t1
mot.calc_sensored_trajectory()

# compute convergence region
Uh=40
fh=1000
mot.calc_convergence_region(Uh, fh)

# plot convergence region
fig, ax = plt.subplots()
ax.plot(mot.i_d_REF, mot.i_q_REF, 'k', label='REF')
ax.plot(mot.i_d_sensored, mot.i_q_sensored, 'y', label='t1')
ax.contour(mot.i_d, mot.i_q, mot.Ihq, levels=[0], colors='r', linestyles='dashed')
ax.contour(mot.i_d, mot.i_q, mot.Ihq_neg, levels=[0], colors='b')
plt.xlabel(r'$i_d$ (A)')
plt.ylabel(r'$i_q$ (A)')
ax.set_aspect('equal')
plt.title('Convergence region plot')
plt.legend(loc="lower right")
plt.grid(linewidth=0.2)
plt.show()


### COENERGY MODEL ###

# compute the coenergy of the SynRM on the 1st quadrant
mot.calc_coenergy_SynRM()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.coenergy)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'Coenergy (J)', rotation=90)
plt.show()


# compute the coenergy variation, and the functions f and g
mot.calc_coenergy_variation()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.delta_coenergy)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'Delta Coenergy (J)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.f_id)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'f', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, mot.g_iq)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'g', rotation=90)
plt.show()



### PROPOSED METHOD ###
# Reconstruction of the incremental inductances and the convergence region from
# just the knowledge of the flux linkages along the current domain borders.

## compute the derivatives of lambda_d_self and lambda_q_self
lambda_d_self_der_map = np.gradient(mot.lambda_d_self, mot.i_d[:,0], axis=0)
lambda_q_self_der_map = np.gradient(mot.lambda_q_self, mot.i_q[0,:], axis=1)

## compute the derivatives of functions f and g
f_id_der_map = np.gradient(mot.f_id, mot.i_d[:,0], axis=0)
g_iq_der_map = np.gradient(mot.g_iq, mot.i_q[0,:], axis=1)

## compute the second derivatives of f_id and g_iq
f_id_2nd_der_map = np.gradient(f_id_der_map, mot.i_d[:,0], axis=0)
g_iq_2nd_der_map = np.gradient(g_iq_der_map, mot.i_q[0,:], axis=1)

## reconstruct the incremental inductances
ldd_rec = lambda_d_self_der_map - 2/3 * f_id_2nd_der_map * mot.g_iq * mot.delta_coenergy[-1,-1]
ldq_rec = - 2/3 * f_id_der_map * g_iq_der_map * mot.delta_coenergy[-1,-1]
lqd_rec = - 2/3 * f_id_der_map * g_iq_der_map * mot.delta_coenergy[-1,-1]
lqq_rec = lambda_q_self_der_map - 2/3 * g_iq_2nd_der_map * mot.f_id * mot.delta_coenergy[-1,-1]

ldq_rec = np.clip(ldq_rec, a_min=None, a_max=0)
lqd_rec = np.clip(lqd_rec, a_min=None, a_max=0)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, ldd_rec)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'ldd (H)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, ldq_rec)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'ldq (H)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, lqd_rec)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'lqd (H)', rotation=90)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(mot.i_d, mot.i_q, lqq_rec)
ax.view_init(elev=45, azim=-135)
ax.set_xlabel(r'$i_d$ (A)')
ax.set_ylabel(r'$i_q$ (A)')
ax.zaxis.set_rotate_label(False)
ax.set_zlabel(r'lqq (H)', rotation=90)
plt.show()


### COMPUTE THE SELF-SENSING CAPABILITIES USING THE RECONSTRUCTED INDUCTANCES ###

# overwrite the incremental inductances
mot.ldd = ldd_rec
mot.ldq = ldq_rec
mot.lqd = lqd_rec
mot.lqq = lqq_rec
mot.lsigma = (mot.lqq + mot.ldd) / 2
mot.ldelta = (mot.lqq - mot.ldd) / 2
mot.lneg = np.sqrt(mot.ldelta**2 + mot.ldq**2)

# compute sensored trajectory t1
mot.calc_sensored_trajectory()

# compute convergence region
Uh=40
fh=1000
mot.calc_convergence_region(Uh, fh)

# plot convergence region (reconstructed)
fig, ax = plt.subplots()
ax.plot(mot.i_d_REF, mot.i_q_REF, 'k', label='REF')
ax.plot(mot.i_d_sensored, mot.i_q_sensored, 'y', label='t1')
ax.contour(mot.i_d, mot.i_q, mot.Ihq, levels=[0], colors='r', linestyles='dashed')
ax.contour(mot.i_d, mot.i_q, mot.Ihq_neg, levels=[0], colors='b')
plt.xlabel(r'$i_d$ (A)')
plt.ylabel(r'$i_q$ (A)')
ax.set_aspect('equal')
plt.title('Convergence region plot')
plt.legend(loc="lower right")
plt.grid(linewidth=0.2)
plt.show()

print('Finished!')
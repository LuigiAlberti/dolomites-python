# dolomites package

![dolomites logo](logo.png)
## dolomites, a package for the design of electric machines and drives.

https://gitlab.com/LuigiAlberti/dolomites-python

## Functionalities:
The various modules in the package perform the following computations
* koil [1]:
  - design of balanced symmetrical windings
  - synthesis of custom windings
* apollo [2],[3]:\
  from flux-linkages maps of synchronous machines to:
  - apparent and incremental inductances
  - mtpa trajectory
  - self-sensing capability evaluation
* tiziano:
  some utilities to build FE meshes and run FE simulations
* fnc:
  an utility to visualize space vectors related to 3-phase systems.
  Based on PySide6 widgets


## Installation on Windows
- Download and install the latest version of Python (https://www.python.org/downloads/)
- During the installation make sure to: 1) enable the option "Add Python to PATH" 2) install pip
- Execute the following commands to check that Python and pip are correctly installed:
```console
py --version
pip --version
```
- Execute the following command to install Jupyter Notebook:
```console
pip install jupyter
```
- Execute the following command to check that Jupyter Notebook is correctly installed:
```console
py -m notebook --version
```
- Download "dolomites-python-master.zip" from https://gitlab.com/LuigiAlberti/dolomites-python **Code/Download source code/zip**
- Unzip it
- Navigate the extracted folder dolomites-python-master
- Launch the Command Prompt in that folder and execute the command:
```console
pip install .
```
- dolomites is now installed.

## Installation on Linux
- Execute the following command to create a Python virtual environment (venv) in your home folder:
```console
python3 -m venv ~/venv
```
- Execute the following commands to check that Python and pip are correctly installed in the virtual environment:
```console
~/venv/bin/python --version
~/venv/bin/pip --version
```
- Execute the following command to install Jupyter Notebook in the virtual environment:
```console
~/venv/bin/pip install jupyter
```
- Execute the following command to check that Jupyter Notebook is correctly installed in the virtual environment:
```console
~/venv/bin/jupyter-notebook --version
```
- It is suggested to create the following alias replacements in ~/.bashrc:
```console
alias venv-python='~/venv/bin/python'
alias venv-pip='~/venv/bin/pip'
alias venv-jupyter-notebook='~/venv/bin/jupyter-notebook'
```
- Download "dolomites-python-master.zip" from https://gitlab.com/LuigiAlberti/dolomites-python **Code/Download source code/zip**
- Unzip it
- Navigate the extracted folder dolomites-python-master
- Open that folder in Terminal and execute the command:
```console
venv-pip install .
```
- dolomites is now installed.

## Examples
Several examples are available in the examples directory.
Each module of dolomites has its own examples directory: please explore them all!

Both Jupyter notebooks (.ipynb) and Python scripts (.py) are available.

## FAQs
- Why is the project called dolomites?\
Dolomites are a mountain range in the northern Italian Alps, located 150 km north of the University of Padua.
Thanks to their outstanding universal value the Dolomites were declared a UNESCO World Heritage Site on 26 June 2009.

- Why one of the modules is called apollo?\
The convergence region plot of a permanent magnet synchronous motor or a synchronous reluctance motor resembles the wing of a butterfly.
We decided to call the module apollo like the mountain butterfly.

- Why one of the modules is called tiziano?\
The name is dedicated to the Italian Renaissance painter Tiziano Vecelli, born in Pieve di Cadore in 1488.


## References
[1] L. Alberti,\
"Koil: A Tool to Design the Winding of Rotating Electric Machinery,"\
2018 XIII International Conference on Electrical Machines (ICEM), Alexandroupoli, Greece, 2018, pp. 805-811,\
https://doi.org/10.1109/ICELMACH.2018.8507004

[2] M. Berto, L. Alberti, V. Manzolini and S. Bolognani,\
"Computation of Self-Sensing Capabilities of Synchronous Machines for Rotating High Frequency Voltage Injection Sensorless Control,"\
in IEEE Transactions on Industrial Electronics, vol. 69, no. 4, pp. 3324-3333, April 2022,\
https://doi.org/10.1109/TIE.2021.3071710

[3] M. Berto, L. Alberti and A. Maimeri,\
"Fast Evaluation of Self-Sensing Capability of Synchronous Machines,"\
in IEEE Transactions on Industrial Electronics (Early Access),\
https://doi.org/10.1109/TIE.2024.3497315


## License and contributors
dolomites is copyright (C) 2006-2024 by L. Alberti and other contributors, University of Padua (Università degli Studi di Padova), and is distributed under the terms of the GNU General Public License (GPL) version 3.

Contributors are:

* Matteo Berto (https://github.com/matteo-berto)
* Elia Scolaro
* Alice Maimeri
* Giuseppe Galati
* Daniele Michieletto

from PySide6 import QtWidgets, QtCore, QtGui
# from PySide6.QtWidgets import QWidget,QGraphicsObject, QGraphicsItem, QGraphicsView, QGraphicsScene, QVBoxLayout, QGraphicsSimpleTextItem, QToolBar, QAction
from PySide6.QtWidgets import *
from PySide6.QtGui import  QAction, QPen, QColor, QPainterPath, QPainter, QPainterPathStroker
from PySide6.QtPrintSupport import QPrinter
import time,math

from dolomites import ui_6 as ui, koil
import numpy as np

class koil_widget (QtWidgets.QWidget):
    """
    A widget to visualize a winding based on dolomites/koil
    No calculation is performed in this class, only visualization purposes
    """


    def __init__(self, parent=None):
           QWidget.__init__(self)
           self.setWindowTitle("dolomites - koil ui")

           self.slots = list() # a list for the slots
           self.coils = list() # a list for the coils

           self.scene = ui.dolomites_scene()
           self.view = ui.dolomites_view(self.scene)
           self.win = koil.m_phase_winding()


           # Create a toolbar
           toolbar = QToolBar("Main Toolbar")

           # Add actions to the toolbar
           action_open = QAction("&Open", self)
           action_open.triggered.connect(self.open)
           toolbar.addAction(action_open)

           action_save = QAction("&Save", self)
           action_save.triggered.connect(self.save)
           toolbar.addAction(action_save)

           action_export = QAction("&Export as PDF", self)
           action_export.triggered.connect(self.export_pdf)
           toolbar.addAction(action_export)


           layout = QVBoxLayout();
           layout.addWidget(toolbar);
           layout.addWidget(self.view);
           self.setLayout(layout);

    @QtCore.Slot()
    def export_pdf(self):
        """Export a PDF of the current drawing."""

         # Get the file path using a file dialog
        file_dialog = QFileDialog(self)
        file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        file_dialog.setNameFilter("PDF files (*.pdf)")
        file_dialog.setDefaultSuffix("pdf")

        if file_dialog.exec():
            file_path = file_dialog.selectedFiles()[0]
            self.view.save_PDF(file_path)
        return

    # to be implemented...
    @QtCore.Slot()
    def open(self):
        """Open a previously saved scheme."""
        return

    # to be implemented...
    @QtCore.Slot()
    def save(self):
        """Savethe current scheme on a file."""
        return





    def draw_slots(self, Q, angles = None, R_slot = 20, R = -1):
        """
        Draw the Q slots.
        angles: custom values in radians wor uneven saced slots
        """

        if angles is None:
             angles = np.linspace(0,2 * math.pi, Q+1)

        if (R<0):
            R  = 2*R_slot*Q / math.pi;


        for i in range (Q):
            slot  = slot_item(i+1,R_slot,angles[i]);
            slot.setPos(R*math.cos(angles[i]),-R*math.sin(angles[i]))
            self.scene.addItem(slot);
            slot.set_label()
            self.slots.append(slot)


    def draw_winding(self,m_phase_winding, colors = None):

           self.win = m_phase_winding

           # if self.win.star.t < 1 :
               # print('winding not feasible!')
               # return
           # if self.win.star.t == None:
               # print('empty winding!')

           # let draw the slots
           # self.draw_slots(self.win.star.Q)
           self.draw_slots(self.win.windings[0].Q)

           for i,w_ph in enumerate(self.win.windings):
               # check if enough colors have been given for the current winding
               if (colors == None or len(colors)< len(self.win.windings)):
                    c = list(np.random.choice(range(256), size=3))
                    color = QColor(c[0],c[1],c[2])
               else:
                   color = colors[i]

               for coil in w_ph.coils:
                    begin = coil.begin
                    end   = coil.end
                    nc    = coil.nc
                    c = coil_item(color, self.slots[begin-1], self.slots[end-1],nc)
                    c.set_incidence(nc)
                    self.scene.addItem(c);
                    self.coils.append(c)


           self.view.fitInView(self.scene.sceneRect(),QtCore.Qt.KeepAspectRatio)

           # wave = mmf_item(self.win)
           # self.scene.addItem(wave);

    def add_connection(self,begin,end,layer1,layer2,R,color=None):

        if color == None:
            c = list(np.random.choice(range(256), size=3))
            color = QColor(c[0],c[1],c[2])

        # this is just to pass the correct angle for the controlpoints in the connection_item
        Q = self.win.windings[0].Q
        con = connection_item(color,self.slots[begin-1], self.slots[end-1], layer1, layer2, R, Q)
        self.scene.addItem(con);

    def clear_empty_slots(self):

       for slot in self.slots:
           slot.clear_layer()




class slot_item (ui.dolomites_item):
    """
    A graphical representation of a slot.

    There are 2 layers, L1 the inner and L2 the outer
    id:    the index of the slot 1, 2, ..., Q
    R:     the radius at which the slot is placed
    angle: the angle where the slot is located.
    """


    def __init__(self,id,R,angle):
        super().__init__()
        # self.path.addEllipse(-R,-R,2*R,2*R);
        self.id = id
        self.angle = angle

        self.R = R

        # inner layer
        self.L1 = slot_layer_item(self,0,R,id)
        # external layer
        self.L2 = slot_layer_item(self,0,R,id)

        self.L1.setPos(-R,0)
        self.L2.setPos(R,0)

        self.setRotation(-angle*180/math.pi)

    def set_label(self):
        # this must be invoked after the slot has been added to the scene
        label = QGraphicsSimpleTextItem(str(self.id),None);
        self.scene().addItem(label)
        center = label.boundingRect().center() # the center of the label

        # adjust the label position
        P = self.L1.scenePos()
        k = (P.manhattanLength()-self.R*2.)/P.manhattanLength()
        label.setPos(k*P.x()-center.x(),k*P.y()-center.y())

    def clear_layer(self):
        """Remove the empty slot layer"""

        # this work but need to fix the coils positions
        # when slot layers are removed
        # if ( (self.L1.incidence != 0) and (self.L2.incidence != 0)):
            # return
        # if ( (self.L1.incidence == 0) and (self.L2.incidence != 0)):
            # self.L1.setVisible(False)
            # self.L2.setPos(0,0)
        # if ( (self.L1.incidence != 0) and (self.L2.incidence == 0)):
            # self.L2.setVisible(False)
            # self.L1.setPos(0,0)




class slot_layer_item (ui.dolomites_item):
    """
    A graphical representation of one layer of the slot.
    The incidence describes if the coil is inwards or outwards in the layer
    """

    def __init__(self,parent,incidence,R,id):
        super().__init__(QtCore.Qt.black,parent)
        self.id = id
        self.R = R
        self.incidence = 0
        self.set_path()

        self.setAcceptHoverEvents(False)
        self.normalColor   = self.pen.color().lighter(125);
        self.brushColor    = QtCore.Qt.white;
        # self.hoverColor    = self.normalColor.lighter(150);
        # self.selectedColor = self.normalColor.darker(200);


        self.setFlag(QGraphicsItem.ItemIsSelectable, False );
        self.setFlag(QGraphicsItem.ItemIsMovable, False);

    def set_path(self):

        self.path = QPainterPath();
        self.path.setFillRule(QtCore.Qt.WindingFill);
        self.path.addEllipse(-self.R,-self.R,2*self.R,2*self.R);

        if (self.incidence > 0):
            self.path.addEllipse(-self.R/4,-self.R/4,2*self.R/4,2*self.R/4);

        if (self.incidence < 0):
            self.path.moveTo(-self.R/4,-self.R/4);
            self.path.lineTo( self.R/4, self.R/4);
            self.path.moveTo(-self.R/4, self.R/4);
            self.path.lineTo( self.R/4,-self.R/4);

    def set_incidence(self,inc):

        self.incidence = inc
        self.set_path()


class coil_item (ui.dolomites_item):
        """
        A representation of a single coil of the winding
        """

        def __repr__(self):
            return "coil (%s, %s, %s)" % (self.begin.id, self.end.id, self.turns)

        def __init__(self, color, begin, end, turns = 1, x = -1, y = -1):
            super().__init__(color,None)

            # the slots where the coil is inserted
            self.begin = begin
            self.end = end
            self.setZValue(-1); # The coil is behind the slots

            self.turns = turns;

            P1 = begin.L1.scenePos(); # recover the outer layer of slot 1
            xs1 = P1.x();
            ys1 = P1.y();
            P2 = end.L2.scenePos(); # recover the inner layer of slot 2
            xs2 = P2.x();
            ys2 = P2.y();

            self.width = math.sqrt((xs2-xs1)*(xs2-xs1)+(ys2-ys1)*(ys2-ys1));
            if (x == -1):
                 self.x = -self.width / 2; # set the default values
            if (y == -1):
                 self.y =  self.width / 2;

            self.cp = control_point_item(self,self.x,self.y);
            # self.cp.setPos(self.x,self.y);

            self.cp.moved.connect(self.cp_moved);

            self.center = QtCore.QPointF( (xs1+xs2)/2 , (ys2+ys1)/2 )

            self.set_path();

            # move and rotate the coil on the two slots and rotate the label
            self.moveBy(self.center.x(),self.center.y());
            self.angle = math.atan2(ys2-ys1,xs2-xs1)*180/math.pi;
            self.setRotation(self.angle);

            self.itemChange(QGraphicsItem.ItemSelectedHasChanged, False);

            # some variables to track mouse movement
            self.mouse_press = False;
            self.xold  = self.x
            self.yold  = self.y
            self.xmouse = 0
            self.ymouse = 0


        @QtCore.Slot(float,float)
        def cp_moved(self,x,y):
            # print(x,y)
            self.prepareGeometryChange();
            self.x = x;
            self.y = y;
            self.set_path();
            self.update();

            return

        def set_path(self):
            stroke = QPainterPathStroker();
            _path = QPainterPath();

            stroke.setWidth(8);
            _path.moveTo(-self.width/2, 0);
            _path.cubicTo(-self.width/2,0,self.x,self.y,0,self.y);
            _path.cubicTo(0,self.y,-self.x,self.y,self.width/2,0);

            self.path = QPainterPath(stroke.createStroke (  _path )) ;
            self.path.closeSubpath();


        def set_incidence(self,inc):
            self.begin.L1.set_incidence(inc)
            self.end.L2.set_incidence(-inc)

            # makes the slots color as the coils
            self.begin.L1.pen = self.pen
            self.end.L2.pen = self.pen


        def itemChange(self, change, value):

            if (change == QGraphicsItem.ItemSelectedHasChanged):
                if (value == True):
                    self.cp.setVisible(True);
                else:
                    self.cp.setVisible(False);
            self.update();

            return super().itemChange(change, value);


        def mousePressEvent (self, event ):
            if(event.button()==QtCore.Qt.LeftButton):
                self.mouse_press = True;
                self.xold  = self.cp.pos().x();
                self.yold  = self.cp.pos().y();
                self.xmouse = event.pos().x();
                self.ymouse = event.pos().y();
            return ui.dolomites_item.mousePressEvent(self,event);

        def mouseReleaseEvent(self,event):
            if(event.button()==QtCore.Qt.LeftButton):
                self.mouse_press = False;
            return ui.dolomites_item.mouseReleaseEvent(self,event);

        def mouseMoveEvent(self,event):
            x = event.pos().x();
            y = event.pos().y();
            if (self.mouse_press):
                if (self.xmouse <=0):
                     self.cp.setX(self.xold+x-self.xmouse);
                else:
                     self.cp.setX(self.xold-x+self.xmouse);
            self.cp.setY(self.yold+y-self.ymouse);

            return ui.dolomites_item.mouseMoveEvent(self,event);


class connection_item (ui.dolomites_item):
        """
        A representation of a connection of the winding
        """
        def __repr__(self):
            return "connection (%s, %s, %s,%s,%s,%s,%s,%s)" % (self.begin, self.end, self.layer1,self.layer2,self.R,self.cp1,self.cp2, self.cp3)

        def __init__(self, color, begin, end, layer1, layer2, R,Q=-1,x1 = -1, y1 = -1, x2 = -1, y2 = -1, x3 = -1, y3 = -1):
            super().__init__(color,None)

            self.layer1  = layer1;
            self.layer2  = layer2;
            self.R       = R;

            self.cp1 = 0
            self.cp2 = 0
            self.cp3 = 0

            # the slots where the connection is connected
            self.begin = begin
            self.end = end
            self.setZValue(-1); # The coil is behind the slots

            P1 = begin.L1.scenePos(); # recover the outer layer of slot 1
            P2 = end.L2.scenePos(); # recover the inner layer of slot 2

            angle1 = self.begin.angle;
            angle2 = self.end.angle;

            # TODO fix this angle as half the slot pitch
            dangle = math.pi/Q;

            R1    = math.sqrt(P1.x()*P1.x()+P1.y()*P1.y());

            # set the default values for the control points
            if (x1 == -1):
                 x1 = R*math.cos(angle1);
            if (y1 == -1):
                 y1 = R*math.sin(angle1);
            if (x2 == -1):
                 x2 = R1*math.cos((angle1+dangle));
            if (y2 == -1):
                 y2 = R1*math.sin((angle1+dangle));
            if (x3 == -1):
                 x3 = R1*math.cos((angle2-dangle));
            if (y3 == -1):
                 y3 = R1*math.sin((angle2-dangle));

            print('x1, y1: '+str(x1)+', '+str(y1))
            self.cp1 = control_point_item(self,x1,-y1);
            self.cp1.moved.connect(self.cp_moved);

            if (layer1==2):
                self.cp2 = control_point_item(self,x2,-y2);
                self.cp2.moved.connect(self.cp_moved);

            if (layer2==2):
                self.cp3 = control_point_item(self,x3,-y3);
                self.cp3.moved.connect(self.cp_moved);

            self.set_path();
            self.itemChange(QGraphicsItem.ItemSelectedHasChanged, False);





        @QtCore.Slot(float,float)
        def cp_moved(self):
            # print(x,y)
            self.prepareGeometryChange();
            self.set_path();
            self.update();

            return

        def set_path(self):
            stroke = QPainterPathStroker();
            stroke.setWidth(4);
            _path = QPainterPath();


            P11   = self.begin.L1.scenePos()
            P12   = self.begin.L2.scenePos()
            P21   = self.end.L1.scenePos();
            P22   = self.end.L2.scenePos();
            R   = math.sqrt(self.cp1.x()*self.cp1.x()+self.cp1.y()*self.cp1.y());

            if (self.layer1==1):
                angle1 = self.begin.angle;
            elif (self.layer1==2):
                angle1 = math.atan2(-self.cp2.y(),self.cp2.x());
                if angle1 < 0:
                    angle1 = angle1 + 2*math.pi


            if (self.layer2==1):
                angle2 = self.end.angle;
            elif (self.layer2==2):
                angle2 = math.atan2(-self.cp3.y(),self.cp3.x());
                if angle2 < 0 :
                    angle2 = angle2 + 2*math.pi
            dangle = (angle1-angle2);

            # if dangle < 0:
                # dangle = -dangle

            if (self.layer2 == 1):
                 _path.moveTo(P21);
            elif(self.layer2 == 2):
                _path.moveTo(P22);
                _path.lineTo(self.cp3.x(),self.cp3.y());

            # if (dangle < math.pi):
                # _path.arcTo(-R,-R,2*R,2*R,angle2*180/math.pi,dangle*180/math.pi);
            # else:
                # _path.arcTo(-R,-R,2*R,2*R,angle2*180/math.pi,360.0-dangle*180/math.pi);
            _path.arcTo(-R,-R,2*R,2*R,angle2*180/math.pi,dangle*180/math.pi);

            if(self.layer1 == 2):
                _path.lineTo(self.cp2.x(),self.cp2.y());
                _path.lineTo(P12);
            elif (self.layer1 == 1):
                _path.lineTo(P11);


            self.path = QPainterPath(stroke.createStroke (  _path )) ;
            print('angle1: '+str(angle1*180/math.pi))
            print('angle2: '+str(angle2*180/math.pi))
            print('dangle: '+str(dangle*180/math.pi))
            # self.path.closeSubpath();

        def itemChange(self, change, value):

            if (change == QGraphicsItem.ItemSelectedHasChanged):
                if (value == True):
                    self.cp1.setVisible(True);
                    if self.cp2:
                        self.cp2.setVisible(True);
                    if self.cp3:
                        self.cp3.setVisible(True);
                else:
                    self.cp1.setVisible(False);
                    if self.cp2:
                        self.cp2.setVisible(False);
                    if self.cp3:
                        self.cp3.setVisible(False);
            self.update();

            return super().itemChange(change, value);


        def mousePressEvent (self, event ):
            if(event.button()==QtCore.Qt.LeftButton):
                print(self.__repr__)
            return ui.dolomites_item.mousePressEvent(self,event);
        #
        # def mouseReleaseEvent(self,event):
        #     if(event.button()==QtCore.Qt.LeftButton):
        #         self.mouse_press = False;
        #     return ui.dolomites_item.mouseReleaseEvent(self,event);
        #
        # def mouseMoveEvent(self,event):
        #     x = event.pos().x();
        #     y = event.pos().y();
        #     if (self.mouse_press):
        #         if (self.xmouse <=0):
        #              self.cp.setX(self.xold+x-self.xmouse);
        #         else:
        #              self.cp.setX(self.xold-x+self.xmouse);
        #     self.cp.setY(self.yold+y-self.ymouse);
        #
        #     return ui.dolomites_item.mouseMoveEvent(self,event);




class control_point_item (ui.dolomites_item):
        """
        An helper class to manage coil_item and connection_item transformations
        """

        moved = QtCore.Signal(float, float)

        def __init__(self,parent, x, y):
            super().__init__(QtCore.Qt.red,parent)


            self.setFlags(QGraphicsItem.ItemIsMovable|QGraphicsItem.ItemSendsGeometryChanges);

            self.dx = 10;
            self.dy = 10;

            self.path.addEllipse(-self.dx/2,-self.dy/2,self.dx,self.dy);
            self.setPos(x,y);




        def itemChange(self, change, value):
             if change == QGraphicsItem.ItemPositionHasChanged:
                self.moved.emit(self.pos().x(),self.pos().y())
             return ui.dolomites_item.itemChange(self, change, value);







class mmf_item (ui.dolomites_item):
    """
    The representation of the mmf over the winding layout
    """
    def __init__(self,win,parent = None):
            super().__init__(QtCore.Qt.black,parent)

            x,mmf = win.calc_mmf()

            # self.path = QPainterPath();

            self.path.moveTo(x[0]*100, mmf[0]*100);
            for _x , _mmf in np.nditer([x,mmf]):
                self.path.lineTo(_x, _mmf);

            self.path.closeSubpath();

"""
apollo.py

apollo: from flux-linkages maps of synchronous machines to:
- apparent and incremental inductances
- mtpa trajectory
- self-sensing capability evaluation [Berto2022Computation]

dolomites-python: a package for the design of electric machines and drives
https://gitlab.com/LuigiAlberti/dolomites-python
"""

# References:

# [Berto2022Computation] M. Berto, L. Alberti, V. Manzolini and S. Bolognani,
# "Computation of Self-Sensing Capabilities of Synchronous Machines for Rotating High Frequency Voltage Injection Sensorless Control,"
# in IEEE Transactions on Industrial Electronics, vol. 69, no. 4, pp. 3324-3333, April 2022,
# https://doi.org/10.1109/TIE.2021.3071710

# [Berto2021Online] M. Berto, L. Alberti, F. Martin and M. Hinkkanen,
# "Online Incremental Inductance Identification for Reluctance Synchronous Motors,"
# IECON 2021 – 47th Annual Conference of the IEEE Industrial Electronics Society, Toronto, ON, Canada, 2021, pp. 1-6,
# https://doi.org/10.1109/IECON48115.2021.9589537

# [Tinazzi2019Classification] F. Tinazzi, S. Bolognani, S. Calligaro, P. Kumar, R. Petrella and M. Zigliotto,
# "Classification and review of MTPA algorithms for synchronous reluctance and interior permanent magnet motor drives,"
# 2019 21st European Conference on Power Electronics and Applications (EPE '19 ECCE Europe), Genova, Italy, 2019, pp. P.1-P.10,
# https://doi.org/10.23919/EPE.2019.8915144

# [Varatharajan2017Predictive] A. Varatharajan, S. Cruz, H. Hadla and F. Briz,
# "Predictive torque control of SynRM drives with online MTPA trajectory tracking and inductances estimation,"
# 2017 IEEE International Electric Machines and Drives Conference (IEMDC), Miami, FL, USA, 2017, pp. 1-7,
# https://doi.org/10.1109/IEMDC.2017.8002104

# [Hinkkanen2017Sensorless] M. Hinkkanen, P. Pescetto, E. Mölsä, S. E. Saarakkala, G. Pellegrino and R. Bojoi,
# "Sensorless Self-Commissioning of Synchronous Reluctance Motors at Standstill Without Rotor Locking,"
# in IEEE Transactions on Industry Applications, vol. 53, no. 3, pp. 2120-2129, May-June 2017,
# https://doi.org/10.1109/TIA.2016.2644624

# [Awan2019Flux] H. A. A. Awan, S. E. Saarakkala and M. Hinkkanen,
# "Flux-Linkage-Based Current Control of Saturated Synchronous Motors,"
# in IEEE Transactions on Industry Applications, vol. 55, no. 5, pp. 4762-4769, Sept.-Oct. 2019,
# https://doi.org/10.1109/TIA.2019.2919258

# [Woo2023Flux] T. -G. Woo, S. -W. Park, S. -C. Choi, H. -J. Lee and Y. -D. Yoon,
# "Flux Saturation Model Including Cross Saturation for Synchronous Reluctance Machines and Its Identification Method at Standstill,"
# in IEEE Transactions on Industrial Electronics, vol. 70, no. 3, pp. 2318-2328, March 2023,
# https://doi.org/10.1109/TIE.2022.3174233

# [Lelli2024Saturation] F. Lelli, M. Hinkkanen and F. G. Capponi,
# "A Saturation Model Based on a Simplified Equivalent Magnetic Circuit for Permanent Magnet Machines,"
# 2024 International Conference on Electrical Machines (ICEM), Torino, Italy, 2024, pp. 1-7,
# https://doi.org/10.1109/ICEM60801.2024.10700403

# [Lee2023Standstill] H. -J. Lee, J. -E. Joo and Y. -D. Yoon,
# "Standstill Sensorless Self-Commissioning Strategy of Synchronous Machines Including Initial Positioning and Torque Canceling Techniques,"
# in IEEE Transactions on Industry Applications, vol. 59, no. 6, pp. 6817-6825, Nov.-Dec. 2023,
# https://doi.org/10.1109/TIA.2023.3308090

# [Maimeri2022Fast] A. Maimeri, L. Alberti, S. Bolognani and M. Berto,
# "Fast Evaluation of Self-Sensing Control Capability for a Synchronous Reluctance Motor,"
# 2022 IEEE Energy Conversion Congress and Exposition (ECCE), Detroit, MI, USA, 2022, pp. 1-6,
# https://doi.org/10.1109/ECCE50734.2022.9947762.

# [Maimeri2022Fasta] A. Maimeri, L. Alberti and M. Berto,
# "Fast Computation of Self-Sensing Capability of Synchronous Machines,"
# IECON 2022 – 48th Annual Conference of the IEEE Industrial Electronics Society, Brussels, Belgium, 2022, pp. 1-6,
# https://doi.org/10.1109/IECON49645.2022.9968380

# [Carbonieri2023Fast] M. Carbonieri, L. D. Leonardo, A. Mahmoudi, M. Tursini, N. Bianchi and W. L. Soong,
# "Fast Flux Mapping Technique for Synchronous Reluctance Machines: Method Description and Comparison With Full FEA and Measurements,"
# in IEEE Transactions on Industry Applications, vol. 59, no. 4, pp. 4056-4065, July-Aug. 2023,
# https://doi.org/10.1109/TIA.2023.3269023

# [Ortombina2023Standstill] L. Ortombina, N. Bianchi and L. Alberti,
# "Standstill Self-Commissioning Procedure for Synchronous Reluctance Motors based on Coenergy Model,"
# 2023 IEEE International Electric Machines & Drives Conference (IEMDC), San Francisco, CA, USA, 2023, pp. 1-6,
# https://doi.org/10.1109/IEMDC55163.2023.10238999

# [Vagati2000Impact] A. Vagati, M. Pastorelli, F. Scapino and G. Franceschini,
# "Impact of cross saturation in synchronous reluctance motors of the transverse-laminated type,"
# in IEEE Transactions on Industry Applications, vol. 36, no. 4, pp. 1039-1046, July-Aug. 2000,
# https://doi.org/10.1109/28.855958


'''
LIST OF FUNCTIONS:
self.create_maps()
self.create_maps_from_domain_borders()
self.calc_apparent_inductances()
self.calc_incremental_inductances()
self.calc_inverse_incremental_inductances()
self.calc_MTPA()
self.export_MTPA()
self.calc_saliency()
self.calc_sensored_error()
self.calc_sensored_trajectory()
self.calc_convergence_region()
self.calc_coenergy_SynRM()
self.calc_coenergy_variation()
self.plot_ellipses()
self.fit_flux_SynRM()
self.fit_flux_SynRM_arctan()
self.fit_flux_PMA_SynRM()
self.fit_flux_PMA_SynRM_arctan()
self.calc_fourier_inverse_inductances()
self.calc_Ihq_few_points()
self.save_maps()
apollo.create_dataframe_SPMSM_ideal()
apollo.create_dataframe_IPMSM_ideal()
apollo.create_dataframe_SynRM_ideal()
apollo.create_dataframe_SynRM_saturated()
apollo.create_dataframe_SynRM_saturated_arctan()
apollo.create_dataframe_PMA_SynRM_saturated()
apollo.create_dataframe_PMA_SynRM_saturated_arctan()
apollo.griddata_wrapper()
apollo.cartesian_to_polar()
apollo.polar_to_cartesian()
apollo.intgrad1()
apollo.intgrad2()
apollo.export_2d_data()
apollo.export_gnuplot_1d_data()
apollo.export_gnuplot_2d_data()
apollo.export_gnuplot_contour_0_data()
'''


'''
LIST OF VARIABLES:
self.data
self.i_d
self.i_q
self.lambda_d
self.lambda_q
self.torque
self.lambda_d0
self.lambda_q0
self.Ld
self.Lq
self.ldd
self.ldq
self.lqd
self.lqq
self.lsigma
self.ldelta
self.lneg
self.gamma_dd
self.gamma_dq
self.gamma_qd
self.gamma_qq
self.gamma_sigma
self.gamma_delta
self.gamma_neg
self.i_d_MTPA
self.i_q_MTPA
self.i_MTPA
self.theta_MTPA
self.xi
self.epsilon
self.epsilon_deg
self.i_REF
self.theta_REF
self.i_d_REF
self.i_q_REF
self.epsilon_REF
self.i_d_sensored
self.i_q_sensored
self.I
self.alpha_ie
self.delta_theta
self.Ihq
self.Ihq_d_alpha_ie
self.Ihq_neg
self.Ihq_in
self.Ihq_in_d_alpha_ie
self.Ihq_in_neg
self.Uh
self.fh
self.coenergy
self.lambda_d_self
self.lambda_d_cross
self.delta_lambda_d
self.lambda_q_self
self.lambda_q_cross
self.delta_lambda_q
self.delta_coenergy
self.f_id
self.g_iq
self.alpha_breakpoints
self.gamma_delta_polar_I
self.gamma_delta_fourier_coef
self.gamma_delta_fourier_rec
self.gamma_dq_polar_I
self.gamma_dq_fourier_coef
self.gamma_dq_fourier_rec
self.Ihq_polar_I
self.Ihq_fourier
self.alpha_breakpoints_NaN
self.gamma_delta_a0
self.gamma_delta_a2
self.gamma_dq_b1
self.gamma_dq_b2
self.Ihq_fourier_2meas
'''


import copy
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import numpy.matlib
import pandas as pd
import math
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.interpolate import griddata
from scipy.interpolate import CubicSpline
from scipy.interpolate import interpn
from scipy.fftpack import fft
from scipy.optimize import curve_fit
from scipy.spatial import Delaunay
from scipy.sparse import csr_matrix


print('imported apollo (rev 09 January 2025)')


class fm():
    """
    A class to represent a flux map
    fm is a pandas dataframe which represents a set of simulations/measures on an electric machine
    The dataframe is expected to have the following fields required for the subsequent computations:
    ["theta_m", "i_d", "i_q", "lambda_d", "lambda_q", "torque"]
    Torque is expected to be the "Maxwell" torque (in case of Finite Element Analysis) or measured torque.
    If torque has neither been simulated on FEA or measured on test bench, please put a column of zeros.
    """
        
    def __init__(self, data):
        self.data = data  # a pandas dataframe used to store all the available data
      
        # some pivot tables to re-organize the data
        self._lambda_d = pd.pivot_table(self.data, index=['theta_m', 'i_d'], columns=['i_q'], values=['lambda_d'])
        self._lambda_q = pd.pivot_table(self.data, index=['theta_m', 'i_d'], columns=['i_q'], values=['lambda_q'])
        self._torque   = pd.pivot_table(self.data, index=['theta_m', 'i_d'], columns=['i_q'], values=['torque'])
       
    def __repr__(self):

        return self.data.to_string()
        
    def create_maps(self, _theta_m=0):
        """
        Compute the maps for the given rotor position _theta_m.
        The following maps are created as numpy arrays:
        lambda_d(i_d,i_q)
        lambda_q(i_d,i_q)
        torque(i_d,i_q)     Maxwell (FEA)/ measured torque
        
        After the call of this function all the quantities are matrices
        (i_d, i_q, lambda_d, lambda_q, torque)
        """

        try:
            lambda_d = self._lambda_d.xs(_theta_m)
        except LookupError:
            print('  APOLLO ERROR in create_maps:')
            print('  the given rotor position (%s) is not in the dataset!' %(_theta_m))
            exit()
            return

        self.i_d = lambda_d.index.get_level_values('i_d').to_numpy()
        self.i_q = lambda_d.columns.get_level_values('i_q').to_numpy()
        
        ## hereafter i_d and i_q will represent matrices so that we can manage scattered data
        self.i_d, self.i_q = np.meshgrid(self.i_d, self.i_q, indexing='ij')
        
        self.lambda_d = lambda_d.to_numpy()
        
        self.lambda_q = self._lambda_q.xs(_theta_m).to_numpy()
        self.torque = self._torque.xs(_theta_m).to_numpy()
        # Fix for self.torque:
        # count the number of zero- and nan-elements
        counted_zeros = self.torque[np.where(self.torque == 0)].size
        counted_nans = self.torque[np.isnan(self.torque)].size
        # if the sum of zero- and nan-elements is equal to the whole size,
        # generate self.torque from scretch
        if counted_zeros + counted_nans == self.torque.size:
            self.torque = np.zeros(self.i_d.shape)
        
        print('created maps for theta_m = %s' % (_theta_m))
        
        
        ## When a map is scattered it may contain NaNs which have to be interpolated
        ## If any NaN is in lambda_d (and probably so also in the other data) fill the missing values
        if (np.any(np.isnan(self.lambda_d))):

            ## Fill missing values INSIDE the convex hull with griddata
            ## Interpolate with griddata on the original (self.i_d, self.i_q) grid           
            self.lambda_d = griddata_wrapper(self.i_d, self.i_q, self.lambda_d, self.i_d, self.i_q)
            self.lambda_q = griddata_wrapper(self.i_d, self.i_q, self.lambda_q, self.i_d, self.i_q)
            self.torque   = griddata_wrapper(self.i_d, self.i_q, self.torque,   self.i_d, self.i_q)
            
            print('filled missing values in the maps')
            
             
    def create_maps_from_domain_borders(self, _theta_m=0):
        """
        Compute the maps for the given rotor position _theta_m.
        Input are the flux linkages simulated/measured along the borders of the current domain (fast mapping) [Carbonieri2023Fast].
        Output are the flux maps reconstructed in the whole current domain Id-Iq [Carbonieri2023Fast],[Ortombina2023Standstill].
        """

        try:
            lambda_d = self._lambda_d.xs(_theta_m)
        except LookupError:
            print('  APOLLO ERROR in create_maps:')
            print('  the given rotor position (%s) is not in the dataset!' %(_theta_m))
            exit()
            return
            
        self.i_d = lambda_d.index.get_level_values('i_d').to_numpy()
        self.i_q = lambda_d.columns.get_level_values('i_q').to_numpy()
            
        ## hereafter i_d and i_q will represent matrices
        self.i_d, self.i_q = np.meshgrid(self.i_d, self.i_q, indexing='ij')
        
        self.lambda_d = lambda_d.to_numpy()
        self.lambda_q = self._lambda_q.xs(_theta_m).to_numpy()
        
        # Check if maps are on quadrant 1 or 2
        if np.any(self.i_q < 0):
            print('  APOLLO ERROR in create_maps_from_domain_borders:')
            print('  impossible to reconstruct the maps from the given domain borders')
            print('  the function create_maps_from_domain_borders is meant to be used on maps on 1st or 2nd quadrant only')
            exit()
        
        ## extract fluxes along the current domain borders
        lambda_d_at_Iq_0 = self.lambda_d[:,0]
        lambda_d_at_Iq_max = self.lambda_d[:,-1]
        lambda_q_at_Id_0 = self.lambda_q[0,:]
        lambda_q_at_Id_max = self.lambda_q[-1,:]
        
        ## compute the coenergy variation due to the cross saturation
        delta_coenergy_d = 3/2*(intgrad1(lambda_d_at_Iq_0, self.i_d[:,0]) - intgrad1(lambda_d_at_Iq_max, self.i_d[:,0])) # eq. 5 of [Carbonieri2023Fast]
        delta_coenergy_q = 3/2*(intgrad1(lambda_q_at_Id_0, self.i_q[0,:]) - intgrad1(lambda_q_at_Id_max, self.i_q[0,:])) # eq. 6 of [Carbonieri2023Fast]

        ## reconstruct function f
        if not delta_coenergy_d.any(): # if numpy array contains only zeros
            f_id = np.zeros(delta_coenergy_d.size) # workaround
        else:
            f_id = delta_coenergy_d/delta_coenergy_d[-1] # eq. 5a of [Ortombina2023Standstill]
        f_id_map = np.matlib.repmat(np.atleast_2d((f_id)).T, 1, self.i_q[0,:].size)
        
        ## reconstruct function g
        if not delta_coenergy_q.any(): # if numpy array contains only zeros
            g_iq = np.zeros(delta_coenergy_q.size) # workaround
        else:
            g_iq = delta_coenergy_q/delta_coenergy_q[-1] # eq. 6a of [Ortombina2023Standstill]
        g_iq_map = np.matlib.repmat(g_iq, self.i_d[:,0].size, 1)
        
        ## compute the derivatives of functions f and g
        f_id_der_map = np.matlib.repmat(np.atleast_2d((np.gradient(f_id, self.i_d[:,0]))).T, 1, self.i_q[0,:].size)
        g_iq_der_map = np.matlib.repmat(np.gradient(g_iq, self.i_q[0,:]), self.i_d[:,0].size, 1)
        
        ## reconstruct the flux linkages in the whole dq plane
        self.lambda_d = np.matlib.repmat(np.atleast_2d((lambda_d_at_Iq_0)).T, 1, self.i_q[0,:].size) - 2/3*f_id_der_map*g_iq_map*delta_coenergy_d[-1] # eq. 6a of [Ortombina2023Standstill]
        self.lambda_q = np.matlib.repmat(lambda_q_at_Id_0, self.i_d[:,0].size, 1) - 2/3*g_iq_der_map*f_id_map*delta_coenergy_d[-1] # eq. 6b of [Ortombina2023Standstill]
        
        ## set the Maxwell (FEA)/ measured torque as an array of zeros
        self.torque = np.zeros(self.i_d.shape)
        
        print('created maps from the borders of the current domain for theta_m = %s' % (_theta_m))
        
     
     
    def save_maps(self, theta_m=0, filename="apollo_maps.txt"):
        """
        Export maps (for the chosen rotor position) in a text format compatible with apollo.
        If not specified, the output rotor position is 0.
        This is useful to save maps after elaborating them (for example with
        interpolation) for further use
        """
        #fix in case theta_m is not defined
        if type(theta_m) == str and filename=="apollo_maps.txt":
            filename = theta_m
            theta_m=0
        
        # flatten output arrays
        theta_m = np.ones(self.i_d.shape) * theta_m
        theta_m = theta_m.flatten()
        i_d = self.i_d.flatten()
        i_q = self.i_q.flatten()
        lambda_d = self.lambda_d.flatten()
        lambda_q = self.lambda_q.flatten()
        torque = self.torque.flatten()
        # define output matrix
        mat_out = np.transpose([theta_m, i_d, i_q, lambda_d, lambda_q, torque])
        
        # avoid missing .txt extension in filename
        filename = filename.replace(".txt", "")
        filename = filename + ".txt"
        
        # save text
        np.savetxt(filename, mat_out, fmt='%1.10e')
        print('maps saved as "', filename, '"')


    def calc_apparent_inductances(self):
        """
        Starting from the flux linkages, compute the apparent inductances
        Refer to the equations in the example
        """
        
        # evaluate lambda_d for id=0
        if 0 in self.i_d: # use the data in Lambda_d if available
            index_id0 = np.where(self.i_d[:,0] == 0)
            lambda_d0 = self.lambda_d[index_id0[0], :]
            lambda_d0 = lambda_d0.flatten()
        else: # otherwise estimate the values. In this case we need negative d-axis currents
            lambda_d0 = griddata_wrapper(self.i_d, self.i_q, self.lambda_d, 0, self.i_q[0,:])

        # Fix for lambda_d0:
        if np.isnan(lambda_d0).all():
            lambda_d0 = np.zeros(lambda_d0.shape)

        # make lambda_d0 a 2D array
        self.lambda_d0 = np.matlib.repmat(lambda_d0, self.i_d.shape[0], 1)
        
        
        # evaluate lambda_q for iq=0
        if 0 in self.i_q: # use the data in Lambda_q if available
            index_iq0 = np.where(self.i_q[0,:] == 0)
            lambda_q0 = self.lambda_q[:, index_iq0[0]]
            lambda_q0 = lambda_q0.flatten()
        else: # otherwise estimate the values. In this case we need negative q-axis currents
            lambda_q0 = griddata_wrapper(self.i_d, self.i_q, self.lambda_q, self.i_d[:,0], 0)
            
        
        # Fix for lambda_q0:
        if np.isnan(lambda_q0).all():
            lambda_q0 = np.zeros(lambda_q0.shape)

        # make lambda_q0 a 2D array
        self.lambda_q0 = np.matlib.repmat(np.array([lambda_q0]).T, 1, self.i_q.shape[1])


        # compute Ld and Lq
        self.Ld = (self.lambda_d - self.lambda_d0) / self.i_d
        self.Lq = (self.lambda_q - self.lambda_q0) / self.i_q

        
        # If any NaN is in Ld fill the missing values (fill the discontinuities)
        if (np.any(np.isnan(self.Ld))):
            # Interpolate with griddata on the original (self.i_d, self.i_q) grid
            self.Ld = griddata_wrapper(self.i_d, self.i_q, self.Ld, self.i_d, self.i_q)
        
        # If any NaN is in the first row of Ld (Id=0 axis) fill the missing values
        if (np.all(np.isnan(self.Ld[0,:]))):
            # copy values from second row
            self.Ld[0,:] = self.Ld[1,:]
            
        # If any NaN is in the last row of Ld (Id=0 axis) fill the missing values
        if (np.all(np.isnan(self.Ld[-1,:]))):
            # copy values from second to last row
            self.Ld[-1,:] = self.Ld[-2,:]
        
        # If any NaN is in Lq fill the missing values (fill the discontinuities)        
        if (np.any(np.isnan(self.Lq))):
            # Interpolate with griddata on the original (self.i_d, self.i_q) grid
            self.Lq = griddata_wrapper(self.i_d, self.i_q, self.Lq, self.i_d, self.i_q)
        
        # If any NaN is in the first column of Lq (Iq=0 axis) fill the missing values
        if (np.all(np.isnan(self.Lq[:,0]))):
            # copy values from second column
            self.Lq[:,0] = self.Lq[:,1]
        
        # If any NaN is in the last column of Lq (Iq=0 axis) fill the missing values
        if (np.all(np.isnan(self.Lq[:,-1]))):
            # copy values from second to last column
            self.Lq[:,-1] = self.Lq[:,-2]


        print('computed apparent inductances')

    def calc_incremental_inductances(self, method='gradient'):
        """
        Starting from the flux linkages, compute the incremental inductances
        with the given method:
            -) gradient: use the numpy gradient algorithm
            -) diff: use the numpy diff algorithm. Last row and last column of
               the inductances matrices are duplicated in order the mantain the
               same dimension of the flux maps.
        NOTE: 'diff' calculates the differences between adjacent elements, while 'gradient'
        computes the 'central differences'.
        -> 'gradient' should be preferred to make convergence region symmetrical.
        """

        if method == 'gradient':
            (self.ldd, self.ldq) = np.gradient(self.lambda_d, self.i_d[:,0], self.i_q[0,:])
            (self.lqd, self.lqq) = np.gradient(self.lambda_q, self.i_d[:,0], self.i_q[0,:])
            print('computed incremental inductances using gradient')

        elif method == 'diff':
            # compute incremental inductances using diff
            
            self.ldd = ((np.diff(self.lambda_d, axis=0)).T / np.diff(self.i_d[:,0])).T
            # duplicate last row
            self.ldd = np.concatenate((self.ldd, np.matrix(self.ldd[-1, :])), axis=0)
            # numpy matrix to array
            self.ldd = np.array(self.ldd)

            self.ldq = ((np.diff(self.lambda_d, axis=1)) / np.diff(self.i_q[0,:]))
            # duplicate last column
            self.ldq = np.concatenate((self.ldq, np.matrix(self.ldq[:, -1]).T), axis=1)
            # numpy matrix to array
            self.ldq = np.array(self.ldq)

            self.lqd = ((np.diff(self.lambda_q, axis=0)).T / np.diff(self.i_d[:,0])).T
            # duplicate last row
            self.lqd = np.concatenate((self.lqd, np.matrix(self.lqd[-1, :])), axis=0)
            # numpy matrix to array
            self.lqd = np.array(self.lqd)

            self.lqq = ((np.diff(self.lambda_q, axis=1)) / np.diff(self.i_q[0,:]))
            # duplicate last column
            self.lqq = np.concatenate((self.lqq, np.matrix(self.lqq[:, -1]).T), axis=1)
            # numpy matrix to array
            self.lqq = np.array(self.lqq)
            print('computed incremental inductances using diff')

        else:
            print('  APOLLO ERROR in calc_incremental_inductances:')
            print('  impossible to compute the incremental inductances')
            print('  please use "diff" or "gradient" method')
            exit()

        self.lsigma = (self.lqq + self.ldd) / 2
        self.ldelta = (self.lqq - self.ldd) / 2

        # the incremental inductance lsigma is known to be related to the positive-sequence current response.
        # on the other hand, the negative-sequence incremental inductance is [Berto2021Online]:
        self.lneg = np.sqrt(self.ldelta**2 + self.ldq**2)

    def calc_inverse_incremental_inductances(self):
        """
        Starting from the incremental inductances, compute the inverse incremental inductances (gamma)
        """

        # check if incremental inductances have been computed
        try:
            d = np.multiply(self.ldd, self.lqq) - np.multiply(self.ldq, self.lqd)
        except AttributeError:
            print('  APOLLO ERROR in calc_inverse_incremental_inductances:')
            print('  incremental inductances not yet computed!')
            print('  consider to call calc_incremental_inductances() first')
            exit()
            return
        else:
            self.gamma_dd = np.divide(self.lqq, d)
            self.gamma_dq = np.divide(-self.ldq, d)
            self.gamma_qd = np.divide(-self.lqd, d)
            self.gamma_qq = np.divide(self.ldd, d)
            self.gamma_sigma = (self.gamma_dd + self.gamma_qq) / 2
            self.gamma_delta = (self.gamma_dd - self.gamma_qq) / 2
            self.gamma_neg = np.sqrt(self.gamma_delta**2 + self.gamma_dq**2)
            
        print('computed inverse incremental inductances')

    def calc_MTPA(self, method="gradient", quadrant=None):
        """
        Compute the MTPA locus, both in cartesian and polar coordinates, in the
        selected quadrant.
        Two different methods to compute MTPA can be choosen:
        "gradient": tangential gradient of the FEA (Maxwell) torque or measured torque, if available
        "analytical": incremental inductances equation, based on dq torque
        """
        
        ## if quadrant is not set, auto-detect MTPA quadrant
        if quadrant is None:
            
            # check if incremental inductances have been computed
            try:
                d = np.multiply(self.ldd, self.lqq) - np.multiply(self.ldq, self.lqd)
            except AttributeError:
                print('  APOLLO ERROR in calc_MTPA:')
                print('  impossible to auto-detect the MTPA quadrant')
                print('  incremental inductances not yet computed!')
                print('  consider to call calc_incremental_inductances() first')
                print('  otherwise, set the MTPA quadrant manually')
                exit()
                return
            else:
            
                # auto-detect convention, and then set the MTPA quadrant
                if np.mean(self.ldelta[~np.isnan(self.ldelta)]) > 0:
                    # detected IPMSM/PMA-SynRM convention. MTPA will be computed on quadrant 2
                    quadrant = 2
                    print('auto-detected MTPA quadrant using IPMSM/PMA-SynRM convention')
                else:
                    # detected SynRM convention. MTPA will be computed on quadrant 1
                    quadrant = 1
                    print('auto-detected MTPA quadrant using SynRM convention')
            
                   
            
        ## compute torque derivative torque_d_alpha_ie
        if method == "gradient":
        
            ## test if self.torque contains only zeros or NaNs
            if np.any(np.nan_to_num(self.torque, nan=0)):
                torque_dx = np.gradient(self.torque, self.i_d[:,0], axis=0)
                torque_dy = np.gradient(self.torque, self.i_q[0,:], axis=1)
                torque_d_alpha_ie = - torque_dx * self.i_q + torque_dy * self.i_d
            
            else:
                print('  APOLLO ERROR in calc_MTPA:')
                print('  impossible to compute MTPA on quadrant', quadrant, 'using gradient')
                print('  the FEA (Maxwell) torque/ measured torque is not available!')
                print('  consider to use analytical method instead')
                exit()

            
        elif method == "analytical":
            # check if incremental inductances have been computed
            try:
                self.ldd
            except AttributeError:
                print('  APOLLO ERROR in calc_MTPA:')
                print('  impossible to compute MTPA on quadrant', quadrant, 'using analythical')
                print('  incremental inductances not yet computed!')
                print('  consider to call calc_incremental_inductances() first')
                exit()
                return
            else:
                # compute torque derivative using eq (11) in [Tinazzi2019Classification], based on eq (10) in [Varatharajan2017Predictive]
                torque_d_alpha_ie = 3/2*(((self.ldq + self.lqd) * self.i_d) * self.i_q \
                - (self.ldd * self.i_q**2 + (self.lqq * self.i_d**2)) \
                + (self.lambda_d * self.i_d) + self.lambda_q * self.i_q) #*pole_pairs

        else:
            print('  APOLLO ERROR in calc_MTPA:')
            print('  impossible to compute the MTPA trajectory')
            print('  please use method "gradient" or "analytical"')
            exit()

        # contour of torque_d_alpha_ie=0 (MTPA region)
        cn = plt.contour(self.i_d, self.i_q, torque_d_alpha_ie, levels=[0])
        plt.close() # don't show contour plot

        # ectract coordinates of the MTPA region from contour data
        C = np.vstack(cn.allsegs[0])
        Id_MTPA = C[:, 0]
        Iq_MTPA = C[:, 1]

        # occasional out of bounds fix
        Id_MTPA = np.clip(Id_MTPA, np.min(self.i_d[:,0]), np.max(self.i_d[:,0]))
        Iq_MTPA = np.clip(Iq_MTPA, np.min(self.i_q[0,:]), np.max(self.i_q[0,:]))

        if quadrant == 1:
            # select quadrants 1 and 2
            Id_MTPA = Id_MTPA[Iq_MTPA > 0]
            Iq_MTPA = Iq_MTPA[Iq_MTPA > 0]
            # select quadrant 1
            Iq_MTPA = Iq_MTPA[Id_MTPA > 0]
            Id_MTPA = Id_MTPA[Id_MTPA > 0]
            print('computed MTPA on quadrant 1 using', method)

        elif quadrant == 2:
            # select quadrants 1 and 2
            Id_MTPA = Id_MTPA[Iq_MTPA > 0]
            Iq_MTPA = Iq_MTPA[Iq_MTPA > 0]
            # select quadrant 2
            Iq_MTPA = Iq_MTPA[Id_MTPA < 0]
            Id_MTPA = Id_MTPA[Id_MTPA < 0]
            print('computed MTPA on quadrant 2 using', method)

        elif quadrant == 3:
            # select quadrants 3 and 4
            Id_MTPA = Id_MTPA[Iq_MTPA < 0]
            Iq_MTPA = Iq_MTPA[Iq_MTPA < 0]
            # select quadrant 3
            Iq_MTPA = Iq_MTPA[Id_MTPA < 0]
            Id_MTPA = Id_MTPA[Id_MTPA < 0]
            print('computed MTPA on quadrant 3 using', method)

        elif quadrant == 4:
            # select quadrants 3 and 4
            Id_MTPA = Id_MTPA[Iq_MTPA < 0]
            Iq_MTPA = Iq_MTPA[Iq_MTPA < 0]
            # select quadrant 4
            Iq_MTPA = Iq_MTPA[Id_MTPA > 0]
            Id_MTPA = Id_MTPA[Id_MTPA > 0]
            print('computed MTPA on quadrant 4 using', method)

        else:
            print('  APOLLO ERROR in calc_MTPA:')
            print('  wrong quadrant for MTPA computation!')
            print('  set a quadrant between 1 and 4')
            exit()


        # if the coordinates of the extracted MTPA trajectory are empty,
        # select another quadrant
        if (Id_MTPA.size == 0) and (Iq_MTPA.size == 0):
            print('  APOLLO ERROR in calc_MTPA:')
            print('  the considered motor has no MTPA on quadrant ', quadrant)
            print('  please select another quadrant')
            exit()


        # transform cartesian coordinates to polar
        I_MTPA = np.sqrt(Id_MTPA**2 + Iq_MTPA**2)
        theta_MTPA = np.arctan2(Iq_MTPA, Id_MTPA)
        
        # remove non-monotic elements from the MTPA trajectory
        if quadrant == 1 or quadrant == 2:
            if I_MTPA[0] < I_MTPA[-1]:
                monotic_indices = np.argwhere(np.diff(Iq_MTPA) > 0)
            else:
                monotic_indices = np.argwhere(np.diff(Iq_MTPA) < 0)
        elif quadrant == 3 or quadrant == 4:
            if I_MTPA[0] < I_MTPA[-1]:
                monotic_indices = np.argwhere(np.diff(Iq_MTPA) < 0)
            else:
                monotic_indices = np.argwhere(np.diff(Iq_MTPA) > 0)
                
        I_MTPA = I_MTPA[monotic_indices].flatten()
        theta_MTPA = theta_MTPA[monotic_indices].flatten()
        Id_MTPA = Id_MTPA[monotic_indices].flatten()
        Iq_MTPA = Iq_MTPA[monotic_indices].flatten()

        # sort vectors with increasing I_MTPA
        order = np.argsort(I_MTPA)
        I_MTPA = I_MTPA[order]
        theta_MTPA = theta_MTPA[order]
        Id_MTPA = Id_MTPA[order]
        Iq_MTPA = Iq_MTPA[order]
        
        # add 0,0 point
        Id_MTPA = np.hstack((0, Id_MTPA))
        Iq_MTPA = np.hstack((0, Iq_MTPA))
        
        # compute MTPA angle in 0,0 with linear extrapolation
        y_extrap = theta_MTPA[0] + (0-I_MTPA[0])/(I_MTPA[-1]-I_MTPA[0]) * (theta_MTPA[-1]-theta_MTPA[0])
        
        # limit initial MTPA angle
        if quadrant == 1:
            if y_extrap <= 0:
                y = 0
            elif y_extrap >= np.pi/2:
                y = np.pi/2
            else:
                y = y_extrap

        if quadrant == 2:
            if y_extrap <= np.pi/2:
                y = np.pi/2
            elif y_extrap >= np.pi:
                y = np.pi
            else:
                y = y_extrap
                
        if quadrant == 3:
            if y_extrap <= -np.pi:
                y = -np.pi
            elif y_extrap >= -np.pi/2:
                y = -np.pi/2
            else:
                y = y_extrap
                
        if quadrant == 4:
            if y_extrap <= -np.pi/2:
                y = -np.pi/2
            elif y_extrap >= 0:
                y = 0
            else:
                y = y_extrap
          
        # print(y_extrap*180/np.pi, '->',y*180/np.pi) #for debug
                
        
        # set initial MTPA angle (possible values: 0°, 45°, 135°, -135°, -90°, and -45°)
        if y >= -np.pi and y < -5/8*np.pi:          #if -180° <= y < -112.5°
            y_out = -3/4*np.pi                          #y_out = -135°
            # print('sector 4')
        elif y >= -5/8*np.pi and y < -3/8*np.pi:    #if -112.5° <= y < -67.5°
            y_out = -np.pi/2                            #y_out = -90°
            # print('sector 5')
        elif y >= -3/8*np.pi and y < -np.pi/8:      #if -67.5° <= y < -22.5°
            y_out = -np.pi/4                            #y_out = -45°
            # print('sector 6')
        elif y >= -np.pi/8 and y < np.pi/8:         #if -22.5° <= y < 22.5°
            y_out = 0                                   #y_out = 0°
            # print('sector 0')    
        elif y >= 0 and y < 3/8*np.pi:              #if 0 <= y < 67.5°
            y_out = np.pi/4                             #y_out = 45°
            # print('sector 1')
        elif y >= 3/8*np.pi and y < 5/8*np.pi:      #if 67.5° <= y < 112.5°
            y_out = np.pi/2                             #y_out = 90°
            # print('sector 2')
        elif y >= 5/8*np.pi and y <= np.pi:         #if 112.5° <= y <= 180°
            y_out = 3/4*np.pi                           #y_out = 135°
            # print('sector 3')
        else:
            y_out = y
            # print('sector ?')
            
        # print(y*180/np.pi, '->',y_out*180/np.pi) #for debug
        
        # horizontally stack the initial value
        theta_MTPA = np.hstack((y_out, theta_MTPA))
        I_MTPA = np.hstack((0, I_MTPA))

        # save the results
        self.i_d_MTPA = Id_MTPA
        self.i_q_MTPA = Iq_MTPA
        self.i_MTPA = I_MTPA
        self.theta_MTPA = theta_MTPA
        
    def export_MTPA(self, pole_pairs, interp=True, num=None, plot=False):
        """
        This functions exports the MTPA coordinates as function of the MTPA torque.
        You can set the number of LUT breakpoints passing 'num'.
        CARTESIAN COORDINATES
        LUT_id_MTPA(:,1) is the MTPA torque (Nm)
        LUT_id_MTPA(:,2) is the MTPA d-current vector component (A)
        LUT_iq_MTPA(:,1) is the MTPA torque (Nm)
        LUT_iq_MTPA(:,2) is the MTPA q-current vector component (A)
        POLAR COORDINATES
        LUT_i_MTPA(:,1) is the MTPA torque (Nm)
        LUT_i_MTPA(:,2) is the MTPA current vector amplitude (A)
        LUT_theta_MTPA(:,1) is the MTPA torque (Nm)
        LUT_theta_MTPA(:,1) is the MTPA current vector angle (rad el)
        
        self.torque is used by default, but in case it is zero the dq torque is computed and used.
        """
        
        try:
            self.i_d_MTPA
        except AttributeError:
            print('  APOLLO ERROR in export_MTPA:')
            print('  impossible to evaluate the MTPA torque')
            print('  MTPA not yet computed!')
            print('  consider to call calc_MTPA() first')
            exit()
            return
            
        if np.all(self.torque) == 0: #if Maxwell/measured torque not available
            torque_dq = 3/2 * pole_pairs * (self.lambda_d * self.i_q - self.lambda_q * self.i_d)
            # torque_MTPA = interpolate.RectBivariateSpline(mot.i_d, mot.i_q, torque_dq)
            torque_MTPA = interpn((self.i_d[:,0], self.i_q[0,:]), torque_dq, (self.i_d_MTPA, self.i_q_MTPA))
            print('MTPA torque computed with dq torque')
        else:
            torque_MTPA = interpn((self.i_d[:,0], self.i_q[0,:]), self.torque, (self.i_d_MTPA, self.i_q_MTPA))
            print('MTPA torque computed with Maxwell/measured torque')
        
        # re-interpolate with uniformely distributed breakpoints
        if interp==True:
            if num==None: # keep the original number of breakpoints
                new_torque_MTPA = np.linspace(start=torque_MTPA[0], stop=torque_MTPA[-1], num=torque_MTPA.size)
            else: # pass the number of breakpoints
                new_torque_MTPA = np.linspace(start=torque_MTPA[0], stop=torque_MTPA[-1], num=num)

            LUT_id_MTPA = numpy.interp(new_torque_MTPA, torque_MTPA, self.i_d_MTPA)
            LUT_iq_MTPA = numpy.interp(new_torque_MTPA, torque_MTPA, self.i_q_MTPA)
            LUT_i_MTPA = numpy.interp(new_torque_MTPA, torque_MTPA, self.i_MTPA)
            LUT_theta_MTPA = numpy.interp(new_torque_MTPA, torque_MTPA, self.theta_MTPA)
            
            if plot==True:
                plt.plot(torque_MTPA, self.i_d_MTPA, label='id_MTPA raw')
                plt.plot(new_torque_MTPA, LUT_id_MTPA, label='id_MTPA interp')
                plt.legend()
                plt.show()
                
                plt.plot(torque_MTPA, self.i_q_MTPA, label='iq_MTPA raw')
                plt.plot(new_torque_MTPA, LUT_iq_MTPA, label='iq_MTPA interp')
                plt.legend()
                plt.show()
                
                plt.plot(torque_MTPA, self.i_MTPA, label='i_MTPA raw')
                plt.plot(new_torque_MTPA, LUT_i_MTPA, label='i_MTPA interp')
                plt.legend()
                plt.show()
                
                plt.plot(torque_MTPA, self.theta_MTPA, label='theta_MTPA raw')
                plt.plot(new_torque_MTPA, LUT_theta_MTPA, label='theta_MTPA interp')
                plt.legend()
                plt.show()
            
            self.export_gnuplot_1d_data(new_torque_MTPA, LUT_id_MTPA, "LUT_id_MTPA.txt")
            self.export_gnuplot_1d_data(new_torque_MTPA, LUT_iq_MTPA, "LUT_iq_MTPA.txt")
            self.export_gnuplot_1d_data(new_torque_MTPA, LUT_i_MTPA, "LUT_i_MTPA.txt")
            self.export_gnuplot_1d_data(new_torque_MTPA, LUT_theta_MTPA, "LUT_theta_MTPA.txt")
        
        else:
            self.export_gnuplot_1d_data(torque_MTPA, self.i_d_MTPA, "LUT_id_MTPA.txt")
            self.export_gnuplot_1d_data(torque_MTPA, self.i_q_MTPA, "LUT_iq_MTPA.txt")
            self.export_gnuplot_1d_data(torque_MTPA, self.i_MTPA, "LUT_i_MTPA.txt")
            self.export_gnuplot_1d_data(torque_MTPA, self.theta_MTPA, "LUT_theta_MTPA.txt")
        # print('exported MTPA trajectory')
        

    def calc_saliency(self):
        """
        Compute the hf-saliency (xi)
        """

        # calc hf-saliency
        self.xi = np.divide((self.lsigma + np.sqrt(self.ldelta**2 + self.ldq**2)), (self.lsigma - np.sqrt(self.ldelta**2 + self.ldq**2)))
        print('computed saliency')
                            
    def calc_sensored_error(self):
        """
        Compute the estimation error when the observer is in open loop (epsilon)
        The convention (SynRM or IPMSM/PMA-SynRM) is automatically detected by default
        """
        # calc estimation error in electrical radiants
        if np.mean(self.ldelta[~np.isnan(self.ldelta)]) > 0:
            # use IPMSM/PMA-SynRM convention
            self.epsilon = 0.5 * np.arctan2(-self.ldq, self.ldelta)
            print('computed sensored error using IPMSM/PMA-SynRM convention')
        else:
            # use SynRM convention
            self.epsilon = 0.5 * np.arctan2(self.ldq, -self.ldelta)
            print('computed sensored error using SynRM convention')

        # compute estimation error also in electrical degrees
        self.epsilon_deg = self.epsilon * 180 / np.pi
                
        
    def calc_sensored_trajectory(self):
        """
        Compute the trajectory of the estimated position during a sensored test
        (estimator in open loop), given a current reference trajectory.
        The sensored trajectory is labeled as "t1" in [Berto2022Computation]
        """
        
        # compute i_d_REF and i_q_REF if they are not defined
        if (not hasattr(self, 'i_d_REF')) or (not hasattr(self, 'i_q_REF')):
            try:
                self.i_d_REF = self.i_REF * np.cos(self.theta_REF)
                self.i_q_REF = self.i_REF * np.sin(self.theta_REF)
            except :
                print('  APOLLO ERROR in calc_sensored_trajectory:')
                print('  reference trajectory seems not assigned')
                print('  consider to assign i_REF and theta_REF values (or i_d_REF and i_q_REF) to fm object first')
                exit()
                return
                    
        # compute i_REF and theta_REF if they are not defined
        if (not hasattr(self, 'i_REF')) or (not hasattr(self, 'theta_REF')):
            try:
                self.i_REF = np.sqrt(self.i_d_REF**2 + self.i_q_REF**2)
                self.theta_REF = np.arctan2(self.i_q_REF, self.i_d_REF)
            except :
                print('  APOLLO ERROR in calc_sensored_trajectory:')
                print('  reference trajectory seems not assigned')
                print('  consider to assign i_REF and theta_REF values (or i_d_REF and i_q_REF) to fm object first')
                exit()
                return
                
        # sort vectors with increasing i_REF
        order = np.argsort(self.i_REF)
        self.i_REF = self.i_REF[order]
        self.theta_REF = self.theta_REF[order]
        self.i_d_REF = self.i_d_REF[order]
        self.i_q_REF = self.i_q_REF[order]

        # FIX: Cut the REF trajectory if it oversteps the convex hull
        # first, prepare data to compute the convex hull
        # flatten
        i_d_vec = self.i_d.flatten()
        i_q_vec = self.i_q.flatten()
        lambda_d_vec = self.lambda_d.flatten()
        # remove nans
        NaN_mask = ~np.isnan(lambda_d_vec) # indices of the non-NaNs
        i_d_vec = i_d_vec[NaN_mask] # apply mask to keep the non-NaNs, i.e. remove NaNs
        i_q_vec = i_q_vec[NaN_mask]
        lambda_d_vec = lambda_d_vec[NaN_mask]
        # these are the data points inside the convex hull
        points = np.vstack((i_d_vec, i_q_vec)).T
        # compute the convex hull
        hull = Delaunay(points)
        # rearrange the REF trajectory points
        REF_points = np.vstack((self.i_d_REF.flatten(), self.i_q_REF.flatten())).T
        # check which points of the REF trajectory are located inside the convex hull
        in_hull = hull.find_simplex(REF_points)>=0
        # keep only the points of the REF trajectory that are located inside the convex hull
        self.i_d_REF = self.i_d_REF[in_hull]
        self.i_q_REF = self.i_q_REF[in_hull]
        self.i_REF = self.i_REF[in_hull]
        self.theta_REF = self.theta_REF[in_hull]

        
        # evaluate epsilon along the reference trajectory
        try:
            epsilon_REF = interpn((self.i_d[:,0], self.i_q[0,:]), self.epsilon, (self.i_d_REF, self.i_q_REF))
        except ValueError:
            # if one of the requested xi is out of bounds in dimension 1:
            # remove the point of the REF trajectory with bigger amplitude
            self.i_REF = self.i_REF[0:-2]
            self.theta_REF = self.theta_REF[0:-2]
            self.i_d_REF = self.i_d_REF[0:-2]
            self.i_q_REF = self.i_q_REF[0:-2]
            # compute again the interpolation
            epsilon_REF = interpn((self.i_d[:,0], self.i_q[0,:]), self.epsilon, (self.i_d_REF, self.i_q_REF))
        
        # store the result
        self.epsilon_REF = epsilon_REF
        
        # plt.plot(self.i_REF, epsilon_REF, label="epsilon_REF")
        # plt.plot(self.i_REF, self.theta_REF, label="theta_REF")
        # plt.plot(self.i_REF, self.theta_REF + epsilon_REF, label="theta_REF + epsilon_REF = t1")
        # plt.legend()
        # plt.show()
        
        # add epsilon to the REF angle -> obtain the angle of sensored trajectory t1
        theta_sensored = self.theta_REF + epsilon_REF
        
        # set output (sensored trajectory) in cartesian coordinates
        self.i_d_sensored = self.i_REF * np.cos(theta_sensored)
        self.i_q_sensored = self.i_REF * np.sin(theta_sensored)
        
        # FIX: Cut the trajectory t1 if it oversteps the convex hull
        # rearrange the REF trajectory points
        t1_points = np.vstack((self.i_d_sensored.flatten(), self.i_q_sensored.flatten())).T
        # check which points of the trajectory t1 are located inside the convex hull
        in_hull = hull.find_simplex(t1_points)>=0
        # keep only the points of the trajectory t1 that are located inside the convex hull
        self.i_d_sensored = self.i_d_sensored[in_hull]
        self.i_q_sensored = self.i_q_sensored[in_hull]

        
        print('computed sensored trajectory')
        

    def calc_convergence_region(self, Uh, fh, Ihq_star=0):
        """
        Compute the signal Ihq, input of the position observer,
        considering a given reference trajectory as current reference.
        The convergence region is the locus Ihq=0 when, for increasing angles,
        the slope is negative (Ihq_neg)
        Input:
        Uh: amplitude of the HF voltage injection (V)
        fh: amplitude of the HF voltage injection (Hz)
        """
        # compute i_d_REF and i_q_REF if they are not defined
        if (not hasattr(self, 'i_d_REF')) or (not hasattr(self, 'i_q_REF')):
            try:
                self.i_d_REF = self.i_REF * np.cos(self.theta_REF)
                self.i_q_REF = self.i_REF * np.sin(self.theta_REF)
            except :
                print('  APOLLO ERROR in calc_convergence_region:')
                print('  reference trajectory seems not assigned')
                print('  consider to assign i_REF and theta_REF values (or i_d_REF and i_q_REF) to fm object first')
                exit()
                return
                    
        # compute i_REF and theta_REF if they are not defined
        if (not hasattr(self, 'i_REF')) or (not hasattr(self, 'theta_REF')):
            try:
                self.i_REF = np.sqrt(self.i_d_REF**2 + self.i_q_REF**2)
                self.theta_REF = np.arctan2(self.i_q_REF, self.i_d_REF)
            except :
                print('  APOLLO ERROR in calc_convergence_region:')
                print('  reference trajectory seems not assigned')
                print('  consider to assign i_REF and theta_REF values (or i_d_REF and i_q_REF) to fm object first')
                exit()
                return
                
                

        # Obtain coordinate matrices in polar form
        self.I = np.sqrt(self.i_d**2 + self.i_q**2)
        alpha_ie = np.arctan2(self.i_q, self.i_d)
        self.alpha_ie = alpha_ie % (2 * np.pi)  # modulus at 2 pi

        # compute Ihq amplitude
        amplitude = Uh / (2 * np.pi * fh) * np.divide(np.sqrt(self.ldelta**2 + self.ldq**2), self.ldd * self.lqq - self.ldq * self.lqd)
        
        # compute delta_theta
        self.delta_theta = self.alpha_ie - np.interp(self.I, self.i_REF, self.theta_REF)
        
        # compute Ihq
        self.Ihq = - amplitude * np.sin(2 * self.delta_theta - 2 * self.epsilon)

        # calc Ihq_d_alpha_ie, tangential derivative (with respect to angle) of Ihq
        Ihq_dx = np.gradient(self.Ihq, self.i_d[:,0], axis=0)
        Ihq_dy = np.gradient(self.Ihq, self.i_q[0,:], axis=1)
        Ihq_d_alpha_ie = - Ihq_dx * self.i_q + Ihq_dy * self.i_d
        self.Ihq_d_alpha_ie = Ihq_d_alpha_ie

        # extract Ihq locus with negative slope
        self.Ihq_neg = copy.deepcopy(self.Ihq)
        self.Ihq_neg[Ihq_d_alpha_ie > 0] = np.nan


        # add Ihq_star compensation

        # compute Ihq_in
        self.Ihq_in = self.Ihq + Ihq_star

        # calc Ihq_in_d_alpha_ie, tangential derivative (with respect to angle) of Ihq_in
        Ihq_in_dx = np.gradient(self.Ihq_in, self.i_d[:,0], axis=0)
        Ihq_in_dy = np.gradient(self.Ihq_in, self.i_q[0,:], axis=1)
        Ihq_in_d_alpha_ie = - Ihq_in_dx * self.i_q + Ihq_in_dy * self.i_d
        self.Ihq_in_d_alpha_ie = Ihq_in_d_alpha_ie

        # extract Ihq_in locus with negative slope
        self.Ihq_in_neg = copy.deepcopy(self.Ihq_in)
        self.Ihq_in_neg[Ihq_in_d_alpha_ie > 0] = np.nan
        
        print('computed convergence region with respect to trajectory REF')
        
        self.Uh=Uh
        self.fh=fh
        
        
    def calc_coenergy_SynRM(self):
        """
        Compute the coenergy of a SynRM. Applicable only on the first quadrant
        
        """
        # Check if any element of Id or Iq is outside the quadrant
        if np.any(self.i_d < 0) or np.any(self.i_q < 0):
            print('  APOLLO ERROR in calc_coenergy_SynRM:')
            print('  impossible to compute the coenergy')
            print('  the function calc_coenergy_SynRM is meant to be used only on SynRM maps on first quadrant')
            exit()
            
        ## compute the coenergy by applying the definition
        ## the method may be slow for big maps due to the intgrad2 function
        self.coenergy = 3/2*intgrad2(self.lambda_q, self.lambda_d, self.i_q[0,:], self.i_d[:,0]) # eq. 4 of [Vagati2000Impact]
        print('computed coenergy')

        
        
    def calc_coenergy_variation(self, method="fast", quadrant=None):
        """
        Compute the coenergy variation due to the cross-saturation [Vagati2000Impact], [Carbonieri2023Fast],[Ortombina2023Standstill], and the functions f and g.
        This function is meant to be used on 1-quadrant maps.
        
        """
        ## if quadrant is not set, auto-detect maps quadrant
        if quadrant is None:
            
            # check if incremental inductances have been computed
            try:
                d = np.multiply(self.ldd, self.lqq) - np.multiply(self.ldq, self.lqd)
            except AttributeError:
                print('  APOLLO ERROR in calc_coenergy_variation:')
                print('  impossible to auto-detect the maps quadrant')
                print('  incremental inductances not yet computed!')
                print('  consider to call calc_incremental_inductances() first')
                print('  otherwise, set the quadrant manually')
                exit()
                return
            else:
            
                # auto-detect convention, and then set the quadrant
                if np.mean(self.ldelta[~np.isnan(self.ldelta)]) > 0:
                    # detected IPMSM/PMA-SynRM convention. Coenergy variation will be computed on quadrant 2
                    quadrant = 2
                    print('auto-detected quadrant ' + str(quadrant) + ' (IPMSM/PMA-SynRM convention) to compute coenergy variation')
                else:
                    # detected SynRM convention. Coenergy variation will be computed on quadrant 1
                    quadrant = 1
                    print('auto-detected quadrant ' + str(quadrant) + ' (SynRM convention) to compute coenergy variation')
        
        if quadrant == 1: # SynRM
            # Check if any element of Id or Iq is outside the quadrant
            if np.any(self.i_d < 0) or np.any(self.i_q < 0):
                print('  APOLLO ERROR in calc_coenergy_variation:')
                print('  impossible to compute the coenergy variation on quadrant ' + str(quadrant))
                print('  the function calc_coenergy_variation is meant to be used on 1-quadrant maps')
                exit()
                                
        elif quadrant == 2: # IPMSM/PMA-SynRM
            # Check if any element of Id or Iq is outside the quadrant
            if np.any(self.i_d > 0) or np.any(self.i_q < 0):
                print('  APOLLO ERROR in calc_coenergy_variation:')
                print('  impossible to compute the coenergy variation on quadrant ' + str(quadrant))
                print('  the function calc_coenergy_variation is meant to be used on 1-quadrant maps')
                exit()
        
        else:
            print('  APOLLO ERROR in calc_coenergy_variation:')
            print('  impossible to compute the coenergy variation on quadrant ' + str(quadrant))
            print('  only quadrant 1 and 2 are allowed')
            exit()
        
        ## compute  flux components
        lambda_d_self = np.matlib.repmat(np.atleast_2d((self.lambda_d[:,0])).T, 1, self.i_q[0,:].size)
        lambda_d_cross = self.lambda_d - lambda_d_self
        delta_lambda_d = - lambda_d_cross
        
        lambda_q_self = np.matlib.repmat(self.lambda_q[0,:], self.i_d[:,0].size, 1)
        lambda_q_cross = self.lambda_q - lambda_q_self
        delta_lambda_q = - lambda_q_cross
        
        ## save results
        self.lambda_d_self = lambda_d_self
        self.lambda_d_cross = lambda_d_cross
        self.delta_lambda_d = delta_lambda_d
        self.lambda_q_self = lambda_q_self
        self.lambda_q_cross = lambda_q_cross
        self.delta_lambda_q = delta_lambda_q
        
        if method == "full":
            ## compute the coenergy variation by applying the definition
            ## the method may be slow for big maps due to the intgrad2 function
            self.delta_coenergy = 3/2*intgrad2(delta_lambda_q, delta_lambda_d, self.i_q[0,:], self.i_d[:,0]) # eq. 6 of [Vagati2000Impact]

        elif method == "fast":
            ## use a fast (computationally efficient) method instead
            
            ## extract fluxes along the current domain borders
            lambda_d_at_Iq_0 = self.lambda_d[:,0]
            lambda_d_at_Iq_max = self.lambda_d[:,-1]
            lambda_q_at_Id_0 = self.lambda_q[0,:]
            lambda_q_at_Id_max = self.lambda_q[-1,:]
            
            ## compute the coenergy variation due to the cross saturation (on axes d and q)
            delta_coenergy_d = 3/2*(intgrad1(lambda_d_at_Iq_0, self.i_d[:,0]) - intgrad1(lambda_d_at_Iq_max, self.i_d[:,0])) # eq. 5 of [Carbonieri2023Fast]
            delta_coenergy_q = 3/2*(intgrad1(lambda_q_at_Id_0, self.i_q[0,:]) - intgrad1(lambda_q_at_Id_max, self.i_q[0,:])) # eq. 6 of [Carbonieri2023Fast]

            ## reconstruct function f
            if not delta_coenergy_d.any(): # if numpy array contains only zeros
                f_id = np.zeros(delta_coenergy_d.size) # workaround
            else:
                f_id = delta_coenergy_d/delta_coenergy_d[-1] # eq. 5a of [Ortombina2023Standstill]
            f_id_map = np.matlib.repmat(np.atleast_2d((f_id)).T, 1, self.i_q[0,:].size)
            
            ## reconstruct function g
            if not delta_coenergy_q.any(): # if numpy array contains only zeros
                g_iq = np.zeros(delta_coenergy_q.size) # workaround
            else:
                g_iq = delta_coenergy_q/delta_coenergy_q[-1] # eq. 6a of [Ortombina2023Standstill]
            g_iq_map = np.matlib.repmat(g_iq, self.i_d[:,0].size, 1)
            
            ## coenergy variation reconstruction
            delta_coenergy = f_id_map*g_iq_map*delta_coenergy_d[-1] # eq. 4 of [Ortombina2023Standstill]
            
            ## save results
            self.delta_coenergy = delta_coenergy
            self.f_id = f_id_map
            self.g_iq = g_iq_map
            
        print('computed coenergy variation due to the cross-saturation')


    def plot_ellipses(self, x, y, Uh, fh, k=1):
        """
        Superimpose a plot of the hf ellipses for various points in the dq current plane
        The ellipses represent the current response to a rotating voltage injection
        Input:
        x: i_d query points (A)
        y: i_q query points (A)
        Uh: amplitude of the HF voltage injection (V)
        fh: amplitude of the HF voltage injection (Hz)
        k: scaling factor
        """
        # clip query bounds
        x = np.clip(x, np.amin(self.i_d[:,0]), np.amax(self.i_d[:,0]))
        y = np.clip(y, np.amin(self.i_q[0,:]), np.amax(self.i_q[0,:]))
        
        # flatten query breakpoints
        x, y = np.meshgrid(x, y, indexing='ij')
        x = x.flatten()
        y = y.flatten()
            
        # interpolate on the query grid points
        lsigma = interpn((self.i_d[:,0], self.i_q[0,:]), self.lsigma, (x, y), method="nearest")
        ldelta = interpn((self.i_d[:,0], self.i_q[0,:]), self.ldelta, (x, y), method="nearest")
        ldq = interpn((self.i_d[:,0], self.i_q[0,:]), self.ldq, (x, y), method="nearest")
        epsilon_deg = interpn((self.i_d[:,0], self.i_q[0,:]), self.epsilon_deg, (x, y), method="nearest")

        # compute major and minor semi-axes (and scale them with k)
        SM = Uh / (2 * np.pi * fh) * np.divide((lsigma + np.sqrt(ldelta**2 + ldq**2)), (lsigma**2 - ldelta**2 - ldq**2)) * k
        sm = Uh / (2 * np.pi * fh) * np.divide((lsigma - np.sqrt(ldelta**2 + ldq**2)), (lsigma**2 - ldelta**2 - ldq**2)) * k
        
        # define ellipses
        ells = [Ellipse(xy=(x[i], y[i]), width=SM[i], height=sm[i], angle=epsilon_deg[i], edgecolor='k', fc='None', lw=1) for i in range(x.size)]
        
        # plot ellipses
        fig = plt.figure(0)
        ax = fig.add_subplot(111, aspect='equal')
        for e in ells:
            ax.add_artist(e)
        ax.set_xlim(np.amin(x) - 2*np.amax(SM[~np.isnan(SM)]), np.amax(x) + 2*np.amax(SM[~np.isnan(SM)]))
        ax.set_ylim(np.amin(y) - 2*np.amax(SM[~np.isnan(SM)]), np.amax(y) + 2*np.amax(SM[~np.isnan(SM)]))
        
        # return the HF ellipses plot
        plot_out = plt.gcf()
        return plot_out
        
        # close plt
        plt.close()
                    

    def fit_flux_SynRM(self, method="simultaneous", S=None, T=None, U=None, V=None,
                        Id_min=None, Id_max=None, n_Id=None, Iq_min=None, Iq_max=None, n_Iq=None, force_int=False, plot=True):
        """
        Fit the flux linkages using the functions in [Hinkkanen2017Sensorless].
        The fitting has a smoothing effect on the maps.
        NOTE: the fitting procedure can be used for RELUCTANCE MOTOR ONLY
        (no permanent magnet).
        
        Two methods are available:
        "average": Two separate fitting are computed, on d and q-axis.
                   The average between the common parameters adq, U, V
                   is computed.
        "following": The fitting on axis d is done at first. The parameters
                   adq, U, V are stored and used as bounds for the following
                   fitting on axis q.
        "simultaneous": The fitting on d and q axes are done simultaneously.
                   This is the best option.
                   
        You can set manually the coefficients S, T, U, and V. For example:
        mot.fit_flux_SynRM(method = "simultaneous", S=6, T=1, U=2, V=0)
        
        You can also set the output Id-Iq grid. Otherwise, the default output grid
        is the same of the input.
        
        With force_int==True you force the exponents S, T, U, and V to be integer.
        Available only for "simultaneous" method.
        """
        
        # Define fitting functions.
        # func1 is for axis d; eq (12a) in [Hinkkanen2017Sensorless]
        # func2 is for axis q; eq (12b) in [Hinkkanen2017Sensorless]
        def func1(x, ad0, add, adq, S, U, V):
            return (ad0 + add * np.power(np.absolute(x[0]), S) + adq / (V + 2) *
                 np.power(np.absolute(x[0]), U) * np.power(np.absolute(x[1]), V + 2)) * x[0]

        def func2(x, aq0, aqq, adq, T, U, V):
            return (aq0 + aqq * np.power(np.absolute(x[1]), T) + adq / (U + 2) *
                 np.power(np.absolute(x[0]), U + 2) * np.power(np.absolute(x[1]), V)) * x[1]
        
                 
        # prepare arrays for curve_fit (flatten and remove NaNs)
        # flatten
        i_d_vec = self.i_d.flatten()
        i_q_vec = self.i_q.flatten()
        lambda_d_vec = self.lambda_d.flatten()
        lambda_q_vec = self.lambda_q.flatten()
        # remove NaNs
        i_d_vec = i_d_vec[~np.isnan(lambda_q_vec)]
        i_q_vec = i_q_vec[~np.isnan(lambda_q_vec)]
        lambda_d_vec = lambda_d_vec[~np.isnan(lambda_q_vec)]
        lambda_q_vec = lambda_q_vec[~np.isnan(lambda_q_vec)]
        
        # define the xdata
        xdata = np.vstack((lambda_d_vec, lambda_q_vec))
         
        # Define the bounds
        if S is None:
            S_min = 0
            S_max = 10
        else:
            S_min = np.clip(S - 1e-6, 0, 10)
            S_max = np.clip(S + 1e-6, 0, 10)
            
            
        if T is None:
            T_min = 0
            T_max = 8
        else:
            T_min = np.clip(T - 1e-6, 0, 8)
            T_max = np.clip(T + 1e-6, 0, 8)
        
        
        if U is None:
            U_min = 0
            U_max = 5
        else:
            U_min = np.clip(U - 1e-6, 0, 5)
            U_max = np.clip(U + 1e-6, 0, 5)
            
            
        if V is None:
            V_min = 0
            V_max = 3
        else:
            V_min = np.clip(V - 1e-6, 0, 3)
            V_max = np.clip(V + 1e-6, 0, 3)

        
        
        if method == "average":
            # interpolate separately on both d- and q-axis
            # and average the coefficients
            
            # define the ydata
            ydata1 = i_d_vec
            
            # Define the bounds
            bounds1 = ((0, 0, 0, S_min, U_min, V_min), (np.inf, np.inf, np.inf, S_max, U_max, V_max))
            
            # Compute the first fitting on axis d
            popt1, pcov1 = curve_fit(func1, xdata, ydata1, bounds = bounds1, maxfev=20000)
            #yfit1 = func1(xdata, *popt1)
            
            #print(popt1)
            ad0 = popt1[0]
            add = popt1[1]
            adq1 = popt1[2]
            S = popt1[3]
            U1 = popt1[4]
            V1 = popt1[5]
        
            # define the ydata
            ydata2 = i_q_vec
            
            # Define the bounds
            bounds2 = ((0, 0, 0, T_min, U_min, V_min), (np.inf, np.inf, np.inf, T_max, U_max, V_max))
            
            # Compute the second fitting on axis q
            popt2, pcov2 = curve_fit(func2, xdata, ydata2, bounds = bounds2, maxfev=20000)
            #yfit2 = func2(xdata, *popt2)

            #print(popt2)
            aq0 = popt2[0]
            aqq = popt2[1]
            adq2 = popt2[2]
            T = popt2[3]
            U2 = popt2[4]
            V2 = popt2[5]

            # average coefficients
            adq = (adq1+adq2)/2
            U = (U1+U2)/2
            V = (V1+V2)/2


        if method == "following":
            # fit the d-axis flux linkage and keep those values for the q-axis interpolation
            
            # define the ydata
            ydata1 = i_d_vec
            
            # Define the bounds
            bounds1 = ((0, 0, 0, S_min, U_min, V_min), (np.inf, np.inf, np.inf, S_max, U_max, V_max))
            
            # Compute the first fitting on axis d
            popt1, pcov1 = curve_fit(func1, xdata, ydata1, bounds = bounds1, maxfev=20000)
            #yfit1 = func1(xdata, *popt1)
            
            #print(popt1)
            ad0 = popt1[0]
            add = popt1[1]
            adq1 = popt1[2]
            S = popt1[3]
            U1 = popt1[4]
            V1 = popt1[5]
            
            # define the ydata
            ydata2 = i_q_vec
            
            # Define the bounds
            adq_min = adq1
            adq_max = adq1 + 1e-6
            U_min = U1
            U_max = U1 + 1e-6
            V_min = V1
            V_max = V1 + 1e-6
            bounds2 = ((0, 0, adq_min, T_min, U_min, V_min), (np.inf, np.inf, adq_max, T_max, U_max, V_max))
            
            # Compute the second fitting on axis q
            popt2, pcov2 = curve_fit(func2, xdata, ydata2, bounds = bounds2, maxfev=20000)
            #yfit2 = func2(xdata, *popt2)

            #print(popt2)
            aq0 = popt2[0]
            aqq = popt2[1]
            adq = popt2[2]
            T = popt2[3]
            U = popt2[4]
            V = popt2[5]
            
            
        if method == "simultaneous":
            # d-q simultaneous fitting with parameter sharing
            
            def mod1(x, ad0, add, aq0, aqq, adq, S, T, U, V):
                return (ad0 + add * np.power(np.absolute(x[0]), S) + adq / (V + 2) *
                     np.power(np.absolute(x[0]), U) * np.power(np.absolute(x[1]), V + 2)) * x[0]

            def mod2(x, ad0, add, aq0, aqq, adq, S, T, U, V):
                return (aq0 + aqq * np.power(np.absolute(x[1]), T) + adq / (U + 2) *
                     np.power(np.absolute(x[0]), U + 2) * np.power(np.absolute(x[1]), V)) * x[1]
            
            # define the ydata
            ydata1 = i_d_vec
            ydata2 = i_q_vec
            comboY = np.append(ydata1, ydata2)

            def comboFunc(comboData, ad0, add, aq0, aqq, adq, S, T, U, V):
                result1 = mod1(comboData, ad0, add, aq0, aqq, adq, S, T, U, V)
                result2 = mod2(comboData, ad0, add, aq0, aqq, adq, S, T, U, V)
                return np.append(result1, result2)
            
            # Define the bounds            
            bounds = ((0, 0, 0, 0, 0, S_min, T_min, U_min, V_min), (np.inf, np.inf, np.inf, np.inf, np.inf, S_max, T_max, U_max, V_max))
            
            # Compute the simultaneous fitting on axis d and q
            popt, pcov = curve_fit(comboFunc, xdata, comboY, bounds = bounds, maxfev=20000)

            #print(popt)
            ad0 = popt[0]
            add = popt[1]
            aq0 = popt[2]
            aqq = popt[3]
            adq = popt[4]
            S = popt[5]
            T = popt[6]
            U = popt[7]
            V = popt[8]
            
            if force_int == True:
                
                # force the fitting exponents to be integer
                S = np.round(S)
                T = np.round(T)
                U = np.round(U)
                V = np.round(V)
                
                # Redefine the bounds
                bounds = ((0, 0, 0, 0, 0, S, T, U, V), (np.inf, np.inf, np.inf, np.inf, np.inf, S+1e-6, T+1e-6, U+1e-6, V+1e-6))
                
                # Repeat the fitting with forced integer
                popt, pcov = curve_fit(comboFunc, xdata, comboY, bounds = bounds, maxfev=20000)
                
                #print(popt)
                ad0 = popt[0]
                add = popt[1]
                aq0 = popt[2]
                aqq = popt[3]
                adq = popt[4]
                S = popt[5]
                T = popt[6]
                U = popt[7]
                V = popt[8]
            
            
        # print resulting fitting parameters
        print('fitted maps using',method,':')
        print("ad0=", '%.2f'%ad0, "  add=", '%.2f'%add, "  aq0=", '%.2f'%aq0, "  aqq=", '%.2f'%aqq, "  adq=", '%.2f'%adq)
        print("S=", '%.2f'%S, "  T=", '%.2f'%T, "  U=", '%.2f'%U, "  V=", '%.2f'%V)
        
        # We obtained the fitting coefficients ad0,add,aq0,aqq,adq,S,T,U,V
        # With these coefficients we can express the currents
        # i_d,i_q as function of the fluxes lambda_d,lambda_q
        
        # The fitted currents will be computed on a FluxD-FluxQ grid.
        # Automatically set the FluxD-FluxQ grid properties:
        FluxD_min = -3 * np.max(np.abs(lambda_d_vec))
        FluxD_max =  3 * np.max(np.abs(lambda_d_vec))
        n_FluxD = self.i_d.shape[0]
        if self.i_d.shape[0] < 251:
            n_FluxD = 251
        FluxQ_min = -3 * np.max(np.abs(lambda_q_vec))
        FluxQ_max =  3 * np.max(np.abs(lambda_q_vec))
        n_FluxQ = self.i_q.shape[1]
        if self.i_q.shape[1] < 251:
            n_FluxQ = 251
            
        # Create the provisional FluxD-FluxQ grid
        FluxD_breakpoints_fg = np.linspace(FluxD_min, FluxD_max, n_FluxD)
        FluxQ_breakpoints_fg = np.linspace(FluxQ_min, FluxQ_max, n_FluxQ)
        FluxD_fg, FluxQ_fg = np.meshgrid(FluxD_breakpoints_fg, FluxQ_breakpoints_fg, indexing='ij')

        # Compute the fitted currents on that grid
        Id_fg = (ad0 + add * np.power(np.absolute(FluxD_fg), S) + adq / (V + 2) *
                 np.power(np.absolute(FluxD_fg), U) * np.power(np.absolute(FluxQ_fg), V + 2)) * FluxD_fg
        Iq_fg = (aq0 + aqq * np.power(np.absolute(FluxQ_fg), T) + adq / (U + 2) *
                 np.power(np.absolute(FluxD_fg), U + 2) * np.power(np.absolute(FluxQ_fg), V)) * FluxQ_fg
                 
        if np.any(np.isnan(Id_fg)) or np.any(np.isnan(Iq_fg)):
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  impossible to fit the flux maps')
            if method == 'average':
                print('  please use "following" method')
            if method == 'following':
                print('  please use "average" method')
            exit()
        
        # We computed the fitted currents on a flux grid.
        # Our aim is to invert the current maps (flux grid):
        # i_d(lambda_d,lambda_q), i_q(lambda_d,lambda_q)
        # into flux maps (current grid):
        # lambda_d(i_d,i_q), lambda_q(i_d,i_q)
        
        # set the properties of the output Id-Iq grid
        if Id_min is None:
            Id_min = np.min(self.i_d[:,0])
        if Id_max is None:
            Id_max = np.max(self.i_d[:,0])
        if n_Id is None:
            n_Id = self.i_d.shape[0]
        if Iq_min is None:
            Iq_min = np.min(self.i_q[0,:])
        if Iq_max is None:
            Iq_max = np.max(self.i_q[0,:])
        if n_Iq is None:
            n_Iq = self.i_q.shape[1]
            
        # protect against inconsistent grid values
        if Id_min>=Id_max:
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  the provided Id_min is greater than or equal to Id_max')
            print('  please provide a value of Id_max greater than Id_min')
            exit()

        if int(n_Id)!=n_Id:
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  the provided number of I_d breakpoints is not an integer')
            print('  please provide an integer value for the number of I_d breakpoints')
            exit()
            
        if n_Id<2:
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  the provided number of I_d breakpoints is lower than 2')
            print('  please provide a number I_d breakpoints which is greater than or equal to 2')
            exit()
            
        if Iq_min>=Iq_max:
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  the provided Iq_min is greater than or equal to Iq_max')
            print('  please provide a value of Iq_max greater than Iq_min')
            exit()
            
        if int(n_Iq)!=n_Iq:
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  the provided number of I_q breakpoints is not an integer')
            print('  please provide an integer value for the number of I_q breakpoints')
            exit()
            
        if n_Iq<2:
            print('  APOLLO ERROR in fit_flux_SynRM:')
            print('  the provided number of I_q breakpoints is lower than 2')
            print('  please provide a number I_q breakpoints which is greater than or equal to 2')
            exit()
        
        # create the output Id-Iq grid
        Id_breakpoints_cg = np.linspace(Id_min, Id_max, n_Id)
        Iq_breakpoints_cg = np.linspace(Iq_min, Iq_max, n_Iq)
        Id_cg, Iq_cg = np.meshgrid(Id_breakpoints_cg, Iq_breakpoints_cg, indexing='ij')

        # Invert the current maps to flux maps
        lambda_d_fit = griddata_wrapper(Id_fg, Iq_fg, FluxD_fg, Id_cg, Iq_cg)
        lambda_q_fit = griddata_wrapper(Id_fg, Iq_fg, FluxQ_fg, Id_cg, Iq_cg)
        
        # compute and print the fitting error
        lambda_d_fit_original_grid = griddata_wrapper(Id_fg, Iq_fg, FluxD_fg, self.i_d, self.i_q)
        lambda_q_fit_original_grid = griddata_wrapper(Id_fg, Iq_fg, FluxQ_fg, self.i_d, self.i_q)
        lambda_d_fit_error = lambda_d_fit_original_grid - self.lambda_d
        lambda_q_fit_error = lambda_q_fit_original_grid - self.lambda_q
        
        print('average fitting error for lambda_d: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_d_fit_error))), 'Vs')
        print('maximum fitting error for lambda_d: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_d_fit_error))), 'Vs')
        print('average fitting error for lambda_q: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_q_fit_error))), 'Vs')
        print('maximum fitting error for lambda_q: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_q_fit_error))), 'Vs')
                
        # plot the fitting results
        if plot==True:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_d, label='lambda_d')
            ax.plot_surface(Id_cg, Iq_cg, lambda_d_fit, label='lambda_d_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_q, label='lambda_q')
            ax.plot_surface(Id_cg, Iq_cg, lambda_q_fit, label='lambda_q_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_d_fit_error, label='lambda_d_fit - lambda_d')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
                   
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_q_fit_error, label='lambda_q_fit - lambda_q')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
        
        # interpolate Maxwell/measured torque on the new grid
        torque_fit = griddata_wrapper(self.i_d, self.i_q, self.torque, Id_cg, Iq_cg)

        # save fitting results
        self.i_d = Id_cg
        self.i_q = Iq_cg
        self.lambda_d = lambda_d_fit
        self.lambda_q = lambda_q_fit
        self.torque = torque_fit    #just re-interpolated
        
        
    def fit_flux_SynRM_arctan(self, Id_min=None, Id_max=None, n_Id=None, Iq_min=None, Iq_max=None, n_Iq=None, plot=True):
        """
        Fit the flux linkages using the functions in [Woo2023Flux].
        The fitting has a smoothing effect on the maps.
        NOTE: the fitting procedure can be used for RELUCTANCE MOTOR ONLY
        (no permanent magnet).
        
        You can also set the output Id-Iq grid. Otherwise, the default output grid
        is the same of the input.        
        """
                 
        # prepare arrays for curve_fit (flatten and remove NaNs)
        # flatten
        i_d_vec = self.i_d.flatten()
        i_q_vec = self.i_q.flatten()
        lambda_d_vec = self.lambda_d.flatten()
        lambda_q_vec = self.lambda_q.flatten()
        # remove NaNs
        i_d_vec = i_d_vec[~np.isnan(lambda_q_vec)]
        i_q_vec = i_q_vec[~np.isnan(lambda_q_vec)]
        lambda_d_vec = lambda_d_vec[~np.isnan(lambda_q_vec)]
        lambda_q_vec = lambda_q_vec[~np.isnan(lambda_q_vec)]
        
        # define the xdata
        xdata = np.vstack((i_d_vec, i_q_vec))
         
        # d-q simultaneous fitting with parameter sharing
        # eq (6a)-(6b) in [Woo2023Flux]
        def mod1(x, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq):
            return Ad*np.arctan(Bd*x[0]) + Cd*x[0] + Ddq*x[0]/(x[0]**2+Kd)*np.log(1+x[1]**2/Kq)

        def mod2(x, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq):
            return Aq*np.arctan(Bq*x[1]) + Cq*x[1] + Ddq*x[1]/(x[1]**2+Kq)*np.log(1+x[0]**2/Kd)
        
        # define the ydata
        ydata1 = lambda_d_vec
        ydata2 = lambda_q_vec
        comboY = np.append(ydata1, ydata2)

        def comboFunc(comboData, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq):
            result1 = mod1(comboData, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq)
            result2 = mod2(comboData, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq)
            return np.append(result1, result2)
        
        # Define the bounds            
        bounds = ((0, 0, 0, 0, 0, 0, 0, 0, -np.inf), (np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, 0))
        
        # Compute the simultaneous fitting on axis d and q
        popt, pcov = curve_fit(comboFunc, xdata, comboY, bounds = bounds, maxfev=20000)

        #print(popt)
        Ad = popt[0]
        Bd = popt[1]
        Cd = popt[2]
        Aq = popt[3]
        Bq = popt[4]
        Cq = popt[5]
        Kd = popt[6]
        Kq = popt[7]
        Ddq = popt[8]
           
        # print resulting fitting parameters
        print('fitted maps:')
        print("Ad=", '%.4f'%Ad, "  Bd=", '%.4f'%Bd, "  Cd=", '%.4f'%Cd)
        print("Aq=", '%.4f'%Aq, "  Bq=", '%.4f'%Bq, "  Cq=", '%.4f'%Cq)
        print("Kd=", '%.4f'%Kd, "  Kq=", '%.4f'%Kq, "  Ddq=", '%.4f'%Ddq)

        
        # set the properties of the output Id-Iq grid
        if Id_min is None:
            Id_min = np.min(self.i_d[:,0])
        if Id_max is None:
            Id_max = np.max(self.i_d[:,0])
        if n_Id is None:
            n_Id = self.i_d.shape[0]
        if Iq_min is None:
            Iq_min = np.min(self.i_q[0,:])
        if Iq_max is None:
            Iq_max = np.max(self.i_q[0,:])
        if n_Iq is None:
            n_Iq = self.i_q.shape[1]
            
        # protect against inconsistent grid values
        if Id_min>=Id_max:
            print('  APOLLO ERROR in fit_flux_SynRM_arctan:')
            print('  the provided Id_min is greater than or equal to Id_max')
            print('  please provide a value of Id_max greater than Id_min')
            exit()

        if int(n_Id)!=n_Id:
            print('  APOLLO ERROR in fit_flux_SynRM_arctan:')
            print('  the provided number of I_d breakpoints is not an integer')
            print('  please provide an integer value for the number of I_d breakpoints')
            exit()
            
        if n_Id<2:
            print('  APOLLO ERROR in fit_flux_SynRM_arctan:')
            print('  the provided number of I_d breakpoints is lower than 2')
            print('  please provide a number I_d breakpoints which is greater than or equal to 2')
            exit()
            
        if Iq_min>=Iq_max:
            print('  APOLLO ERROR in fit_flux_SynRM_arctan:')
            print('  the provided Iq_min is greater than or equal to Iq_max')
            print('  please provide a value of Iq_max greater than Iq_min')
            exit()
            
        if int(n_Iq)!=n_Iq:
            print('  APOLLO ERROR in fit_flux_SynRM_arctan:')
            print('  the provided number of I_q breakpoints is not an integer')
            print('  please provide an integer value for the number of I_q breakpoints')
            exit()
            
        if n_Iq<2:
            print('  APOLLO ERROR in fit_flux_SynRM_arctan:')
            print('  the provided number of I_q breakpoints is lower than 2')
            print('  please provide a number I_q breakpoints which is greater than or equal to 2')
            exit()
        
        
        # create the Id-Iq grid
        Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
        Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
        i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints, indexing='ij')
          
        # compute fluxes - eq (6a)-(6-b) in [Woo2023Flux]
        lambda_d_fit = Ad*np.arctan(Bd*i_d) + Cd*i_d + Ddq*i_d/(i_d**2+Kd)*np.log(1+i_q**2/Kq)
        lambda_q_fit = Aq*np.arctan(Bq*i_q) + Cq*i_q + Ddq*i_q/(i_q**2+Kq)*np.log(1+i_d**2/Kd)
        
        # compute and print the fitting error
        lambda_d_fit_original_grid = Ad*np.arctan(Bd*self.i_d) + Cd*self.i_d + Ddq*self.i_d/(self.i_d**2+Kd)*np.log(1+self.i_q**2/Kq)
        lambda_q_fit_original_grid = Aq*np.arctan(Bq*self.i_q) + Cq*self.i_q + Ddq*self.i_q/(self.i_q**2+Kq)*np.log(1+self.i_d**2/Kd)
        lambda_d_fit_error = lambda_d_fit_original_grid - self.lambda_d
        lambda_q_fit_error = lambda_q_fit_original_grid - self.lambda_q
        
        print('average fitting error for lambda_d: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_d_fit_error))), 'Vs')
        print('maximum fitting error for lambda_d: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_d_fit_error))), 'Vs')
        print('average fitting error for lambda_q: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_q_fit_error))), 'Vs')
        print('maximum fitting error for lambda_q: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_q_fit_error))), 'Vs')
                
        # plot the fitting results
        if plot==True:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_d, label='lambda_d')
            ax.plot_surface(i_d, i_q, lambda_d_fit, label='lambda_d_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_q, label='lambda_q')
            ax.plot_surface(i_d, i_q, lambda_q_fit, label='lambda_q_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_d_fit_error, label='lambda_d_fit - lambda_d')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
                   
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_q_fit_error, label='lambda_q_fit - lambda_q')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
        
        # interpolate Maxwell/measured torque on the new grid
        torque_fit = griddata_wrapper(self.i_d, self.i_q, self.torque, i_d, i_q)
        
        # save fitting results
        self.i_d = i_d
        self.i_q = i_q
        self.lambda_d = lambda_d_fit
        self.lambda_q = lambda_q_fit
        self.torque = torque_fit    #just re-interpolated
        
        
        
    def fit_flux_PMA_SynRM(self, S=None, T=None, U=None, V=None, W=None, Id_min=None, Id_max=None, n_Id=None, Iq_min=None, Iq_max=None, n_Iq=None, force_int=False, plot=True):
        """
        Fit the flux linkages using the functions in [Lelli2024Saturation].
        The fitting has a smoothing effect on the maps.
        
        The fitting procedure can be used for Permanent Magnet Assisted Synchronous Reluctance Motors.
        
        The fitting on d and q axes are done simultaneously.
                   
        You can set manually the coefficients S, T, U, V, and W. For example:
        mot.fit_flux_PMA_SynRM(S=6, T=1, U=2, V=0, W=1)
        
        You can also set the output Id-Iq grid. Otherwise, the default output grid
        is the same of the input.
        
        With force_int==True you force the exponents S, T, U, V, and W to be integer.
        """
        
        # prepare arrays for curve_fit (flatten and remove NaNs)
        # flatten
        i_d_vec = self.i_d.flatten()
        i_q_vec = self.i_q.flatten()
        lambda_d_vec = self.lambda_d.flatten()
        lambda_q_vec = self.lambda_q.flatten()
        # remove NaNs
        i_d_vec = i_d_vec[~np.isnan(lambda_q_vec)]
        i_q_vec = i_q_vec[~np.isnan(lambda_q_vec)]
        lambda_d_vec = lambda_d_vec[~np.isnan(lambda_q_vec)]
        lambda_q_vec = lambda_q_vec[~np.isnan(lambda_q_vec)]
        
        # define the xdata
        xdata = np.vstack((lambda_d_vec, lambda_q_vec))
         
        # Define the bounds
        if S is None:
            S_min = 0
            S_max = 10
        else:
            S_min = np.clip(S - 1e-6, 0, 10)
            S_max = np.clip(S + 1e-6, 0, 10)
            
            
        if T is None:
            T_min = 0
            T_max = 8
        else:
            T_min = np.clip(T - 1e-6, 0, 8)
            T_max = np.clip(T + 1e-6, 0, 8)
        
        
        if U is None:
            U_min = 0
            U_max = 5
        else:
            U_min = np.clip(U - 1e-6, 0, 5)
            U_max = np.clip(U + 1e-6, 0, 5)
            
            
        if V is None:
            V_min = 0
            V_max = 3
        else:
            V_min = np.clip(V - 1e-6, 0, 3)
            V_max = np.clip(V + 1e-6, 0, 3)
            
        if W is None:
            W_min = 0
            W_max = 3
        else:
            W_min = np.clip(W - 1e-6, 0, 3)
            W_max = np.clip(W + 1e-6, 0, 3)
            

        # d-q simultaneous fitting with parameter sharing
        
        def mod1(x, ad0, add, adq, aq0, aqq, a_b, a_bp, k_q, psi_f, S, T, U, V, W):
            return (ad0 + add * np.power(np.absolute(x[0]), S) + adq / (V + 2) *
                 np.power(np.absolute(x[0]), U) * np.power(np.absolute(x[1]), V + 2)) * x[0] \
                 + (a_b*np.sqrt((x[0]-psi_f)**2 + k_q*x[1]**2)**W / (1 + a_bp*np.sqrt((x[0]-psi_f)**2 + k_q*x[1]**2)**W)) * (x[0]-psi_f)

        def mod2(x, ad0, add, adq, aq0, aqq, a_b, a_bp, k_q, psi_f, S, T, U, V, W):
            return (aq0 + aqq * np.power(np.absolute(x[1]), T) + adq / (U + 2) *
                 np.power(np.absolute(x[0]), U + 2) * np.power(np.absolute(x[1]), V)) * x[1] \
                 + (a_b*np.sqrt((x[0]-psi_f)**2 + k_q*x[1]**2)**W / (1 + a_bp*np.sqrt((x[0]-psi_f)**2 + k_q*x[1]**2)**W)) * k_q * x[1]
        
        # define the ydata
        ydata1 = i_d_vec
        ydata2 = i_q_vec
        comboY = np.append(ydata1, ydata2)

        def comboFunc(comboData, ad0, add, adq, aq0, aqq, a_b, a_bp, k_q, psi_f, S, T, U, V, W):
            result1 = mod1(comboData, ad0, add, adq, aq0, aqq, a_b, a_bp, k_q, psi_f, S, T, U, V, W)
            result2 = mod2(comboData, ad0, add, adq, aq0, aqq, a_b, a_bp, k_q, psi_f, S, T, U, V, W)
            return np.append(result1, result2)
        
        # Define the bounds            
        bounds = ((0, 0, 0, 0, 0, 0, 0, 0, 0, S_min, T_min, U_min, V_min, W_min),
        (np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, S_max, T_max, U_max, V_max, W_max))
        
        # Compute the simultaneous fitting on axis d and q
        popt, pcov = curve_fit(comboFunc, xdata, comboY, bounds = bounds, maxfev=20000)

        #print(popt)
        ad0 = popt[0]
        add = popt[1]
        adq = popt[2]
        aq0 = popt[3]
        aqq = popt[4]
        a_b = popt[5]
        a_bp = popt[6]
        k_q = popt[7]
        psi_f = popt[8]
        S = popt[9]
        T = popt[10]
        U = popt[11]
        V = popt[12]
        W = popt[13]
        
        if force_int == True:
            
            # force the fitting exponents to be integer
            S = np.round(S)
            T = np.round(T)
            U = np.round(U)
            V = np.round(V)
            W = np.round(W)
            
            # Redefine the bounds
            bounds = ((0, 0, 0, 0, 0, 0, 0, 0, 0, S, T, U, V, W),
            (np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, S+1e-6, T+1e-6, U+1e-6, V+1e-6, W+1e-6))
            
            # Repeat the fitting with forced integer
            popt, pcov = curve_fit(comboFunc, xdata, comboY, bounds = bounds, maxfev=20000)
            
            #print(popt)
            ad0 = popt[0]
            add = popt[1]
            adq = popt[2]
            aq0 = popt[3]
            aqq = popt[4]
            a_b = popt[5]
            a_bp = popt[6]
            k_q = popt[7]
            psi_f = popt[8]
            S = popt[9]
            T = popt[10]
            U = popt[11]
            V = popt[12]
            W = popt[13]
            
        # print resulting fitting parameters
        print('fitted maps:')
        print("ad0=", '%.2f'%ad0, "  add=", '%.2f'%add, "  aq0=", '%.2f'%aq0, "  aqq=", '%.2f'%aqq,
        "  adq=", '%.2f'%adq, "  a_b=", '%.2f'%a_b,
        "  a_bp=", '%.2f'%a_bp, "  k_q=", '%.2f'%k_q,  "  psi_f=", '%.2f'%psi_f)
        print("S=", '%.2f'%S, "  T=", '%.2f'%T, "  U=", '%.2f'%U,
        "  V=", '%.2f'%V, "  W=", '%.2f'%W)
        
        # We obtained the fitting coefficients ad0,add,adq,aq0,aqq,a_b,a_bp,k_q,psi_f,S,T,U,V,W
        # With these coefficients we can express the currents
        # i_d,i_q as function of the fluxes lambda_d,lambda_q
        
        # The fitted currents will be computed on a FluxD-FluxQ grid.
        # Automatically set the FluxD-FluxQ grid properties:
        FluxD_min = -3 * np.max(np.abs(lambda_d_vec))
        FluxD_max =  3 * np.max(np.abs(lambda_d_vec))
        n_FluxD = self.i_d.shape[0]
        if self.i_d.shape[0] < 251:
            n_FluxD = 251
        FluxQ_min = -3 * np.max(np.abs(lambda_q_vec))
        FluxQ_max =  3 * np.max(np.abs(lambda_q_vec))
        n_FluxQ = self.i_q.shape[1]
        if self.i_q.shape[1] < 251:
            n_FluxQ = 251
            
        # Create the provisional FluxD-FluxQ grid
        FluxD_breakpoints_fg = np.linspace(FluxD_min, FluxD_max, n_FluxD)
        FluxQ_breakpoints_fg = np.linspace(FluxQ_min, FluxQ_max, n_FluxQ)
        FluxD_fg, FluxQ_fg = np.meshgrid(FluxD_breakpoints_fg, FluxQ_breakpoints_fg, indexing='ij')

        # Compute the fitted currents on that grid:
        # Inverse inductance functions for the d- and q-axis
        G_d = (ad0 + add * np.power(np.absolute(FluxD_fg), S) + adq / (V + 2) *
               np.power(np.absolute(FluxD_fg), U) * np.power(np.absolute(FluxQ_fg), V + 2))
        G_q = (aq0 + aqq * np.power(np.absolute(FluxQ_fg), T) + adq / (U + 2) *
               np.power(np.absolute(FluxD_fg), U + 2) * np.power(np.absolute(FluxQ_fg), V))
                    
        # Bridge flux
        psi_b = FluxD_fg - psi_f
        # State of the bridge saturation depends also on the q-axis flux
        psi_b_sat = np.sqrt(psi_b**2 + k_q*FluxQ_fg**2)
        # Inverse inductance function for the bridge saturation
        G_b = a_b*psi_b_sat**W/(1 + a_bp*psi_b_sat**W)
        
        # Stator current
        Id_fg = G_d * FluxD_fg + G_b * psi_b
        Iq_fg = G_q * FluxQ_fg + k_q * G_b * FluxQ_fg
                 
        if np.any(np.isnan(Id_fg)) or np.any(np.isnan(Iq_fg)):
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  impossible to fit the flux maps')
            if method == 'average':
                print('  please use "following" method')
            if method == 'following':
                print('  please use "average" method')
            exit()
        
        # We computed the fitted currents on a flux grid.
        # Our aim is to invert the current maps (flux grid):
        # i_d(lambda_d,lambda_q), i_q(lambda_d,lambda_q)
        # into flux maps (current grid):
        # lambda_d(i_d,i_q), lambda_q(i_d,i_q)
        
        # set the properties of the output Id-Iq grid
        if Id_min is None:
            Id_min = np.min(self.i_d[:,0])
        if Id_max is None:
            Id_max = np.max(self.i_d[:,0])
        if n_Id is None:
            n_Id = self.i_d.shape[0]
        if Iq_min is None:
            Iq_min = np.min(self.i_q[0,:])
        if Iq_max is None:
            Iq_max = np.max(self.i_q[0,:])
        if n_Iq is None:
            n_Iq = self.i_q.shape[1]
            
        # protect against inconsistent grid values
        if Id_min>=Id_max:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  the provided Id_min is greater than or equal to Id_max')
            print('  please provide a value of Id_max greater than Id_min')
            exit()

        if int(n_Id)!=n_Id:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  the provided number of I_d breakpoints is not an integer')
            print('  please provide an integer value for the number of I_d breakpoints')
            exit()
            
        if n_Id<2:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  the provided number of I_d breakpoints is lower than 2')
            print('  please provide a number I_d breakpoints which is greater than or equal to 2')
            exit()
            
        if Iq_min>=Iq_max:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  the provided Iq_min is greater than or equal to Iq_max')
            print('  please provide a value of Iq_max greater than Iq_min')
            exit()
            
        if int(n_Iq)!=n_Iq:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  the provided number of I_q breakpoints is not an integer')
            print('  please provide an integer value for the number of I_q breakpoints')
            exit()
            
        if n_Iq<2:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM:')
            print('  the provided number of I_q breakpoints is lower than 2')
            print('  please provide a number I_q breakpoints which is greater than or equal to 2')
            exit()
        
        # create the output Id-Iq grid
        Id_breakpoints_cg = np.linspace(Id_min, Id_max, n_Id)
        Iq_breakpoints_cg = np.linspace(Iq_min, Iq_max, n_Iq)
        Id_cg, Iq_cg = np.meshgrid(Id_breakpoints_cg, Iq_breakpoints_cg, indexing='ij')

        # Invert the current maps to flux maps
        lambda_d_fit = griddata_wrapper(Id_fg, Iq_fg, FluxD_fg, Id_cg, Iq_cg)
        lambda_q_fit = griddata_wrapper(Id_fg, Iq_fg, FluxQ_fg, Id_cg, Iq_cg)
        
        # compute and print the fitting error
        lambda_d_fit_original_grid = griddata_wrapper(Id_fg, Iq_fg, FluxD_fg, self.i_d, self.i_q)
        lambda_q_fit_original_grid = griddata_wrapper(Id_fg, Iq_fg, FluxQ_fg, self.i_d, self.i_q)
        lambda_d_fit_error = lambda_d_fit_original_grid - self.lambda_d
        lambda_q_fit_error = lambda_q_fit_original_grid - self.lambda_q
        
        print('average fitting error for lambda_d: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_d_fit_error))), 'Vs')
        print('maximum fitting error for lambda_d: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_d_fit_error))), 'Vs')
        print('average fitting error for lambda_q: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_q_fit_error))), 'Vs')
        print('maximum fitting error for lambda_q: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_q_fit_error))), 'Vs')
                
        # plot the fitting results
        if plot==True:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_d, label='lambda_d')
            ax.plot_surface(Id_cg, Iq_cg, lambda_d_fit, label='lambda_d_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_q, label='lambda_q')
            ax.plot_surface(Id_cg, Iq_cg, lambda_q_fit, label='lambda_q_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_d_fit_error, label='lambda_d_fit - lambda_d')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
                   
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_q_fit_error, label='lambda_q_fit - lambda_q')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
        
        # interpolate Maxwell/measured torque on the new grid
        torque_fit = griddata_wrapper(self.i_d, self.i_q, self.torque, Id_cg, Iq_cg)
        
        # save fitting results
        self.i_d = Id_cg
        self.i_q = Iq_cg
        self.lambda_d = lambda_d_fit
        self.lambda_q = lambda_q_fit
        self.torque = torque_fit    #just re-interpolated
        


    def fit_flux_PMA_SynRM_arctan(self, Id_min=None, Id_max=None, n_Id=None, Iq_min=None, Iq_max=None, n_Iq=None, plot=True):
        """
        Fit the flux linkages using the functions in [Lee2023Standstill].
        The fitting has a smoothing effect on the maps.
        
        The fitting procedure can be used for Interior Permanent Magnet Synchronous Motors.
        
        The fitting on d and q axes are done simultaneously.
        
        You can also set the output Id-Iq grid. Otherwise, the default output grid
        is the same of the input.        
        """
                 
        # prepare arrays for curve_fit (flatten and remove NaNs)
        # flatten
        i_d_vec = self.i_d.flatten()
        i_q_vec = self.i_q.flatten()
        lambda_d_vec = self.lambda_d.flatten()
        lambda_q_vec = self.lambda_q.flatten()
        # remove NaNs
        i_d_vec = i_d_vec[~np.isnan(lambda_q_vec)]
        i_q_vec = i_q_vec[~np.isnan(lambda_q_vec)]
        lambda_d_vec = lambda_d_vec[~np.isnan(lambda_q_vec)]
        lambda_q_vec = lambda_q_vec[~np.isnan(lambda_q_vec)]
        
        # define the xdata
        xdata = np.vstack((i_d_vec, i_q_vec))
         
        # d-q simultaneous fitting with parameter sharing
        # eq (6a)-(6b) in [Lee2023Standstill]
        def mod1(x, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, i_f):
            return Ad*np.arctan(Bd*(x[0]+i_f)) + Cd*(x[0]+i_f) + Ddq*(x[0]+i_f)/((x[0]+i_f)**2+Kd)*np.log(1+x[1]**2/Kq)

        def mod2(x, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, i_f):
            return Aq*np.arctan(Bq*x[1]) + Cq*x[1] + Ddq*x[1]/(x[1]**2+Kq)*np.log(1+(x[0]+i_f)**2/Kd)
        
        # define the ydata
        ydata1 = lambda_d_vec
        ydata2 = lambda_q_vec
        comboY = np.append(ydata1, ydata2)

        def comboFunc(comboData, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, i_f):
            result1 = mod1(comboData, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, i_f)
            result2 = mod2(comboData, Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, i_f)
            return np.append(result1, result2)
        
        # Define the bounds            
        bounds = ((0, 0, 0, 0, 0, 0, 0, 0, -np.inf, 0), (np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, 0, np.inf))
        
        # Compute the simultaneous fitting on axis d and q
        popt, pcov = curve_fit(comboFunc, xdata, comboY, bounds = bounds, maxfev=20000)

        #print(popt)
        Ad = popt[0]
        Bd = popt[1]
        Cd = popt[2]
        Aq = popt[3]
        Bq = popt[4]
        Cq = popt[5]
        Kd = popt[6]
        Kq = popt[7]
        Ddq = popt[8]
        i_f = popt[9]
           
        # print resulting fitting parameters
        print('fitted maps:')
        print("Ad=", '%.4f'%Ad, "  Bd=", '%.4f'%Bd, "  Cd=", '%.4f'%Cd)
        print("Aq=", '%.4f'%Aq, "  Bq=", '%.4f'%Bq, "  Cq=", '%.4f'%Cq)
        print("Kd=", '%.4f'%Kd, "  Kq=", '%.4f'%Kq, "  Ddq=", '%.4f'%Ddq, "  I_f=", '%.4f'%i_f)

        
        # set the properties of the output Id-Iq grid
        if Id_min is None:
            Id_min = np.min(self.i_d[:,0])
        if Id_max is None:
            Id_max = np.max(self.i_d[:,0])
        if n_Id is None:
            n_Id = self.i_d.shape[0]
        if Iq_min is None:
            Iq_min = np.min(self.i_q[0,:])
        if Iq_max is None:
            Iq_max = np.max(self.i_q[0,:])
        if n_Iq is None:
            n_Iq = self.i_q.shape[1]
            
        # protect against inconsistent grid values
        if Id_min>=Id_max:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM_arctan:')
            print('  the provided Id_min is greater than or equal to Id_max')
            print('  please provide a value of Id_max greater than Id_min')
            exit()

        if int(n_Id)!=n_Id:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM_arctan:')
            print('  the provided number of I_d breakpoints is not an integer')
            print('  please provide an integer value for the number of I_d breakpoints')
            exit()
            
        if n_Id<2:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM_arctan:')
            print('  the provided number of I_d breakpoints is lower than 2')
            print('  please provide a number I_d breakpoints which is greater than or equal to 2')
            exit()
            
        if Iq_min>=Iq_max:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM_arctan:')
            print('  the provided Iq_min is greater than or equal to Iq_max')
            print('  please provide a value of Iq_max greater than Iq_min')
            exit()
            
        if int(n_Iq)!=n_Iq:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM_arctan:')
            print('  the provided number of I_q breakpoints is not an integer')
            print('  please provide an integer value for the number of I_q breakpoints')
            exit()
            
        if n_Iq<2:
            print('  APOLLO ERROR in fit_flux_PMA_SynRM_arctan:')
            print('  the provided number of I_q breakpoints is lower than 2')
            print('  please provide a number I_q breakpoints which is greater than or equal to 2')
            exit()
            
        
        # create the Id-Iq grid
        Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
        Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
        i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints, indexing='ij')
          
        # compute fluxes - eq (8) in [Lee2023Standstill]
        i_dm = i_d + i_f
        lambda_d_fit = Ad*np.arctan(Bd*i_dm) + Cd*i_dm + Ddq*i_dm/(i_dm**2+Kd)*np.log(1+i_q**2/Kq)
        lambda_q_fit = Aq*np.arctan(Bq*i_q) + Cq*i_q + Ddq*i_q/(i_q**2+Kq)*np.log(1+i_dm**2/Kd)
        
        # compute and print the fitting error
        lambda_d_fit_original_grid = Ad*np.arctan(Bd*(self.i_d+i_f)) + Cd*(self.i_d+i_f) + Ddq*(self.i_d+i_f)/((self.i_d+i_f)**2+Kd)*np.log(1+self.i_q**2/Kq)
        lambda_q_fit_original_grid = Aq*np.arctan(Bq*self.i_q) + Cq*self.i_q + Ddq*self.i_q/(self.i_q**2+Kq)*np.log(1+(self.i_d+i_f)**2/Kd)
        lambda_d_fit_error = lambda_d_fit_original_grid - self.lambda_d
        lambda_q_fit_error = lambda_q_fit_original_grid - self.lambda_q
        
        print('average fitting error for lambda_d: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_d_fit_error))), 'Vs')
        print('maximum fitting error for lambda_d: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_d_fit_error))), 'Vs')
        print('average fitting error for lambda_q: ', '%.2e'%np.nanmean(np.nanmean(np.abs(lambda_q_fit_error))), 'Vs')
        print('maximum fitting error for lambda_q: ', '%.2e'%np.nanmax(np.nanmax(np.abs(lambda_q_fit_error))), 'Vs')
                
        # plot the fitting results
        if plot==True:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_d, label='lambda_d')
            ax.plot_surface(i_d, i_q, lambda_d_fit, label='lambda_d_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, self.lambda_q, label='lambda_q')
            ax.plot_surface(i_d, i_q, lambda_q_fit, label='lambda_q_fit')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
            ax.legend()
            plt.show()
            
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_d_fit_error, label='lambda_d_fit - lambda_d')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_d (Vs)$', rotation=90)
            ax.legend()
            plt.show()
                   
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.plot_surface(self.i_d, self.i_q, lambda_q_fit_error, label='lambda_q_fit - lambda_q')
            ax.view_init(elev=45, azim=-135)
            ax.set_xlabel(r'$i_d$ (A)')
            ax.set_ylabel(r'$i_q$ (A)')
            ax.zaxis.set_rotate_label(False)
            ax.set_zlabel(r'$\lambda_q (Vs)$', rotation=90)
            ax.legend()
            plt.show()
        
        # interpolate Maxwell/measured torque on the new grid
        torque_fit = griddata_wrapper(self.i_d, self.i_q, self.torque, i_d, i_q)
        
        # save fitting results
        self.i_d = i_d
        self.i_q = i_q
        self.lambda_d = lambda_d_fit
        self.lambda_q = lambda_q_fit
        self.torque = torque_fit    #just re-interpolated

        
        
    def calc_fourier_inverse_inductances(self, I='', k=2, method='fitting', steps=1000, plot=False):
        """
        Author: Alice Maimeri
        Institution: University of Padua (Università degli Studi di Padova)
        
        This function calculates the Fourier coefficients of the inverse incremental
        inductances gamma_delta and gamma_dq in polar coordinates for a current modulus I [Maimeri2022Fast],[Maimeri2022Fasta].
        It also reconstructed Ihq signal with the Fourier series of the inverse incremental inductances
        
        You set manually the current modulus value I in which this function calculates
        the fourier coefficients and the Ihq, the harmonic order k and the method by which it
        calculates the coefficients.
        
        Two methods are available:
        
        "fitting": fit the inverse incremental inductances using fist terms of
                   the Fourier series as a function, default
        
        "fft": compute fast Fourier trasform on the inverse incremental inductances 
               to obtain the coefficients of the Fourier series
        
        
        steps: the number of breakpoints to create the angle vector, default is 1000
        
        this function stores the results in the following variables:
        
        self.alpha_breakpoints: angles for polar coordinates
        self.gamma_delta_polar_I: gamma_delta interpolated in polar coordinates
        self.gamma_delta_fourier_coef: the computed Fourier series coefficients (k+1 elements)
        self.gamma_delta_fourier_rec: the reconstructed gamma_delta using the Fourier series
        self.gamma_dq_polar_I: gamma_dq interpolated in polar coordinates
        self.gamma_dq_fourier_coef: the computed Fourier series coefficients (k+1 elements)
        self.gamma_dq_fourier_rec: the reconstructed gamma_dq using the Fourier series
        self.Ihq_polar_I: Ihq interpolated in polar coordinates
        self.Ihq_fourier: the reconstructed Ihq using the Fourier series of inverse incremental inductances

        """
        # trasform into polar coordinates
        alpha_array = np.arctan2(self.i_q, self.i_d)
        alpha_array = np.mod(alpha_array, 2*np.pi) 
        I_array = np.sqrt(self.i_d**2 + self.i_q**2)

        # Define self.alpha_breakpoints
        self.alpha_breakpoints = math.pi/180*np.linspace(0,360,steps)
        
        # convert two-dimensional vector into one-dimensional 
        alpha_array = alpha_array.flatten()
        I_array = I_array.flatten()
        gamma_delta = self.gamma_delta.flatten()
        gamma_dq = self.gamma_dq.flatten()
        
        # remove NaN
        NaN_mask = ~np.isnan(gamma_delta) 
        I_array = I_array[NaN_mask] 
        alpha_array = alpha_array[NaN_mask] 
        gamma_delta = gamma_delta[NaN_mask]
        gamma_dq = gamma_dq[NaN_mask]
        
        
        points_I = np.vstack((alpha_array, I_array)).T
        
        # check the consistency of input data
        if method != 'fft' and method != 'fitting' :
            print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
            print('  impossible to compute the fourier coefficients of the inverse incremental inductance')
            print('  please use "fft" or "fitting" method')
            exit()
            
        if np.min(alpha_array) >= 0.2 or np.max(alpha_array) <= 6.1 :
            print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
            print('  impossible to compute the fourier coefficients of inverse incremental inductance')
            print('  Necessary to have the measurements of the inverse inductances along the entire period from 0 to 2pi')
            exit()
            
    
        if I=='' :
            print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
            print('  impossible to compute the fourier coefficients of inverse incremental inductance')
            print('  please set the current modulus value')
            exit()
            
        elif I < 0 or I > np.min([np.max(np.max(self.i_d)),np.max(np.max(self.i_q))]) :
            print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
            print('  impossible to compute the fourier coefficients of inverse incremental inductance')
            print('  please set I values between 0 and i_d_max or i_q_max')
            exit()
            
        # compute griddata for the current modulus value I
        gamma_delta_polar_I = griddata(points_I, gamma_delta, (self.alpha_breakpoints, [I] * steps), method='cubic')
        gamma_dq_polar_I    = griddata(points_I, gamma_dq, (self.alpha_breakpoints, [I] * steps), method='cubic')
        print('I=',I)
    
        if method == 'fft': #compute fast Fourier transform (fft)
        
            # coefficients of Fourier series for gamma_delta
            N = len(gamma_delta_polar_I)
            # replace Nan with values obtained through piecewise cubic spline interpolation
            gamma_delta_polar_I = np.concatenate((gamma_delta_polar_I[math.ceil(N/2):],gamma_delta_polar_I[:math.ceil(N/2)]),axis=None)
            gamma_delta_polar_I = pd.DataFrame(gamma_delta_polar_I).interpolate(method='cubic').values.ravel()
            gamma_delta_polar_I = np.concatenate((gamma_delta_polar_I[math.ceil(N/2):],gamma_delta_polar_I[:math.ceil(N/2)]),axis=None)
            gamma_delta_polar_I_mask = np.isnan(gamma_delta_polar_I)
            if np.any(gamma_delta_polar_I_mask) == True :
                print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                print('  impossible to compute the fourier coefficients of the delta inverse incremental inductance with fft method')
                print('  NaN found during the fft computation; try to use a higher current value I')
                exit()
            else:
                if k < 2 or k >((N-1)/2):
                    print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                    print('  impossible to compute the fourier coefficients of the delta inverse incremental inductance with fft')
                    print('  please use a different value of k')
                    exit()
                else:
                    Z = fft(gamma_delta_polar_I)
                    Z = Z/N
                    #create vector that will contain the first k coefficients of the Fourier series: 
                    #self.gamma_delta_fourier_coef = [a0 a1 b1 a2 b2 a3 b3 ...]
                    #a_k for cosines and b_k for sines where k is the harmonic order.
                    self.gamma_delta_fourier_coef = np.zeros (2*k+1)
                    self.gamma_delta_fourier_coef[0] = np.real(Z[0])
                    self.gamma_delta_fourier_rec = np.zeros ( ((k+1),(len(self.alpha_breakpoints))) )
                    self.gamma_delta_fourier_rec[0,:]=self.gamma_delta_fourier_coef[0] 
                    print('Gamma_delta')
                    print('a',0,'=','%.4f'%self.gamma_delta_fourier_coef[0])
                    for i in range (1,k+1):
                        self.gamma_delta_fourier_coef[2*i-1] = self.gamma_delta_fourier_coef[2*i-1] + 2 * np.real(Z[i])
                        self.gamma_delta_fourier_coef[2*i] = self.gamma_delta_fourier_coef[2*i] - 2 * np.imag(Z[i])
                        print('a',i,'=','%.4f'%self.gamma_delta_fourier_coef[2*i-1],'b',i,'=','%.4f'%self.gamma_delta_fourier_coef[2*i])
                        self.gamma_delta_fourier_rec[i,:] = self.gamma_delta_fourier_coef[2*i-1]*np.cos(i*self.alpha_breakpoints) + self.gamma_delta_fourier_coef[2*i]*np.sin(i*self.alpha_breakpoints)
                    self.gamma_delta_fourier_rec = np.sum(self.gamma_delta_fourier_rec[:,],axis=0)
            
            # coefficients of Fourier series for gamma_dq
            N = len(gamma_dq_polar_I)
            # replace Nan with values obtained through piecewise cubic spline interpolation
            gamma_dq_polar_I = np.concatenate((gamma_dq_polar_I[math.ceil(N/2):],gamma_dq_polar_I[:math.ceil(N/2)]),axis=None)
            gamma_dq_polar_I = pd.DataFrame(gamma_dq_polar_I).interpolate(method='cubic').values.ravel()
            gamma_dq_polar_I = np.concatenate((gamma_dq_polar_I[math.ceil(N/2):],gamma_dq_polar_I[:math.ceil(N/2)]),axis=None)
            gamma_dq_polar_I_mask = np.isnan(gamma_dq_polar_I)
            if np.any(gamma_dq_polar_I_mask) == True :
                print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                print('  impossible to compute the fourier coefficients of the dq inverse incremental inductance with fft method')
                print('  NaN found during the fft computation; try to use a higher current value I') 
                exit()   
            else:
                if k<2 and k>((N-1)/2):
                    print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                    print('  impossible to compute the fourier coefficients of the dq inverse incremental inductance with fft')
                    print('  please use differet value of k')
                    exit()          
                else:
                    Z = fft(gamma_dq_polar_I)
                    Z = Z/N
                    #create vector that will contain the first k coefficients of the Fourier series: 
                    #self.gamma_dq_fourier_coef = [a0 a1 b1 a2 b2 a3 b3 ...]
                    #a_k for cosines and b_k for sines where k is the harmonic order
                    self.gamma_dq_fourier_coef = np.zeros (2*k+1)
                    self.gamma_dq_fourier_coef[0] = np.real(Z[0])
                    self.gamma_dq_fourier_rec = np.zeros ( ((k+1),(len(self.alpha_breakpoints))) )
                    self.gamma_dq_fourier_rec[0,:]=self.gamma_dq_fourier_coef[0]
                    print('Gamma_dq')
                    print('a',0,'=','%.4f'%self.gamma_dq_fourier_coef[0])
                    for i in range (1,k+1):
                        self.gamma_dq_fourier_coef[2*i-1] = self.gamma_dq_fourier_coef[2*i-1] + 2 * np.real(Z[i])
                        self.gamma_dq_fourier_coef[2*i] = self.gamma_dq_fourier_coef[2*i] - 2 * np.imag(Z[i])
                        print('a',i,'=','%.4f'%self.gamma_dq_fourier_coef[2*i-1],'b',i,'=','%.4f'%self.gamma_dq_fourier_coef[2*i])
                        self.gamma_dq_fourier_rec[i,:] = self.gamma_dq_fourier_coef[2*i-1]*np.cos(i*self.alpha_breakpoints) + self.gamma_dq_fourier_coef[2*i]*np.sin(i*self.alpha_breakpoints)
                    self.gamma_dq_fourier_rec = np.sum(self.gamma_dq_fourier_rec[:,],axis=0)
                    
        #Fit the inverse incremental inductances using first terms of the Fourier series as a function  
        elif method == 'fitting':      
        
            # coefficients of Fourier series for gamma_delta
            #Remove NaN for fitting
            NaN_mask = ~np.isnan(gamma_delta_polar_I) 
            alpha_breakpoints_delta = self.alpha_breakpoints[NaN_mask] 
            gamma_delta_polar_I_nn = gamma_delta_polar_I[NaN_mask]
            if len(alpha_breakpoints_delta) == 0 :
                print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                print('  impossible to compute the fourier coefficients of the delta inverse incremental inductance with fitting method')
                print('  NaN found during the fitting computation; try to use a higher current value I')
                exit()
            else:
                if k == 2 :
                    def fun(x, a0, a1, b1, a2, b2):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x)
                    self.gamma_delta_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_delta, gamma_delta_polar_I_nn,maxfev=20000)
                    print('Gamma_delta',"a0=", '%.4f'%self.gamma_delta_fourier_coef[0], "a1=", '%.4f'%self.gamma_delta_fourier_coef[1],"b1=", '%.4f'%self.gamma_delta_fourier_coef[2], \
                    "a2=", '%.4f'%self.gamma_delta_fourier_coef[3],"b2=", '%.4f'%self.gamma_delta_fourier_coef[4])
                    self.gamma_delta_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_delta_fourier_coef)
                    
                elif k == 3 :
                    def fun(x, a0, a1, b1, a2, b2, a3, b3):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x) + a3*np.cos(3*x) + b3*np.sin(3*x)
                    self.gamma_delta_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_delta, gamma_delta_polar_I_nn,maxfev=20000)
                    print('Gamma_delta',"a0=", '%.4f'%self.gamma_delta_fourier_coef[0], "a1=", '%.4f'%self.gamma_delta_fourier_coef[1],"b1=", '%.4f'%self.gamma_delta_fourier_coef[2], \
                            "a2=", '%.4f'%self.gamma_delta_fourier_coef[3],"b2=", '%.4f'%self.gamma_delta_fourier_coef[4],"a3=", '%.4f'%self.gamma_delta_fourier_coef[5],"b3=", '%.4f'%self.gamma_delta_fourier_coef[6])
                    self.gamma_delta_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_delta_fourier_coef)
                    
                elif k ==4 :
                    def fun(x, a0, a1, b1, a2, b2, a3, b3, a4, b4):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x) + a3*np.cos(3*x) + b3*np.sin(3*x)+ a4*np.cos(4*x) + b4*np.sin(4*x)
                    self.gamma_delta_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_delta, gamma_delta_polar_I_nn,maxfev=20000)
                    print('Gamma_delta',"a0=", '%.4f'%self.gamma_delta_fourier_coef[0], "a1=", '%.4f'%self.gamma_delta_fourier_coef[1],"b1=", '%.4f'%self.gamma_delta_fourier_coef[2], \
                    "a2=", '%.4f'%self.gamma_delta_fourier_coef[3],"b2=", '%.4f'%self.gamma_delta_fourier_coef[4],"a3=", '%.4f'%self.gamma_delta_fourier_coef[5],"b3=", '%.4f'%self.gamma_delta_fourier_coef[6],\
                    "a4=", '%.4f'%self.gamma_delta_fourier_coef[7],"b4=", '%.4f'%self.gamma_delta_fourier_coef[8])
                    self.gamma_delta_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_delta_fourier_coef)
                    
                elif k==5:
                    def fun(x, a0, a1, b1, a2, b2, a3, b3, a4, b4, a5, b5):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x) + a3*np.cos(3*x) + b3*np.sin(3*x)+ a4*np.cos(4*x) + b4*np.sin(4*x) + a5*np.cos(5*x) + b5*np.sin(5*x)
                    self.gamma_delta_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_delta, gamma_delta_polar_I_nn,maxfev=20000)
                    print('Gamma_delta',"a0=", '%.4f'%self.gamma_delta_fourier_coef[0], "a1=", '%.4f'%self.gamma_delta_fourier_coef[1],"b1=", '%.4f'%self.gamma_delta_fourier_coef[2],\
                    "a2=", '%.4f'%self.gamma_delta_fourier_coef[3],"b2=", '%.4f'%self.gamma_delta_fourier_coef[4],"a3=", '%.4f'%self.gamma_delta_fourier_coef[5],"b3=", '%.4f'%self.gamma_delta_fourier_coef[6],\
                    "a4=", '%.4f'%self.gamma_delta_fourier_coef[7],"b4=", '%.4f'%self.gamma_delta_fourier_coef[8],"a5=", '%.4f'%self.gamma_delta_fourier_coef[9],"b5=", '%.4f'%self.gamma_delta_fourier_coef[10])
                    self.gamma_delta_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_delta_fourier_coef)
                    
                else :
                    print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                    print('  impossible to compute the fourier coefficients of the delta inverse incremental inductance with fitting method')
                    print('  please use k between 2 and 5')
                    exit()
                    
            # coefficients of Fourier series for gamma_dq
            #Remove NaN for fitting
            NaN_mask = ~np.isnan(gamma_dq_polar_I) 
            alpha_breakpoints_dq = self.alpha_breakpoints[NaN_mask]
            gamma_dq_polar_I_nn = gamma_dq_polar_I[NaN_mask]
            if len(alpha_breakpoints_dq) == 0 :
                print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                print('  impossible to compute the fourier coefficients of the dq inverse incremental inductance with fitting method')
                print('  NaN found during the fitting computation; try to use a higher current value I')
                exit()
            else:    
                if k == 2 :
                    def fun(x, a0, a1, b1, a2, b2):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x)
                    self.gamma_dq_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_dq, gamma_dq_polar_I_nn,maxfev=20000)
                    print('Gamma_dq',"a0=", '%.4f'%self.gamma_dq_fourier_coef[0], "a1=", '%.4f'%self.gamma_dq_fourier_coef[1],"b1=", '%.4f'%self.gamma_dq_fourier_coef[2],\
                    "a2=", '%.4f'%self.gamma_dq_fourier_coef[3],"b2=", '%.4f'%self.gamma_dq_fourier_coef[4])
                    self.gamma_dq_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_dq_fourier_coef)
                    
                elif k == 3 :
                    def fun(x, a0, a1, b1, a2, b2, a3, b3):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x) + a3*np.cos(3*x) + b3*np.sin(3*x)
                    self.gamma_dq_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_dq, gamma_dq_polar_I_nn,maxfev=20000)
                    print('Gamma_dq',"a0=", '%.4f'%self.gamma_dq_fourier_coef[0], "a1=", '%.4f'%self.gamma_dq_fourier_coef[1],"b1=", '%.4f'%self.gamma_dq_fourier_coef[2], \
                    "a2=", '%.4f'%self.gamma_dq_fourier_coef[3],"b2=", '%.4f'%self.gamma_dq_fourier_coef[4],"a3=", '%.4f'%self.gamma_dq_fourier_coef[5],"b3=", '%.4f'%self.gamma_dq_fourier_coef[6])
                    self.gamma_dq_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_dq_fourier_coef)
                    
                elif k == 4 :
                    def fun(x, a0, a1, b1, a2, b2, a3, b3, a4, b4):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x) + a3*np.cos(3*x) + b3*np.sin(3*x)+ a4*np.cos(4*x) + b4*np.sin(4*x)
                    self.gamma_dq_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_dq, gamma_dq_polar_I_nn,maxfev=20000)
                    print('Gamma_dq',"a0=", '%.4f'%self.gamma_dq_fourier_coef[0], "a1=", '%.4f'%self.gamma_dq_fourier_coef[1],"b1=", '%.4f'%self.gamma_dq_fourier_coef[2], \
                    "a2=", '%.4f'%self.gamma_dq_fourier_coef[3],"b2=", '%.4f'%self.gamma_dq_fourier_coef[4],"a3=", '%.4f'%self.gamma_dq_fourier_coef[5],"b3=", '%.4f'%self.gamma_dq_fourier_coef[6],\
                    "a4=", '%.4f'%self.gamma_dq_fourier_coef[7],"b4=", '%.4f'%self.gamma_dq_fourier_coef[8])
                    self.gamma_dq_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_dq_fourier_coef)
                    
                elif k == 5 :
                    def fun(x, a0, a1, b1, a2, b2, a3, b3, a4, b4, a5, b5):
                        return a0 + a1*np.cos(x) + b1*np.sin(x) + a2*np.cos(2*x) + b2*np.sin(2*x) + a3*np.cos(3*x) + b3*np.sin(3*x)+ a4*np.cos(4*x) + b4*np.sin(4*x) + a5*np.cos(5*x) + b5*np.sin(5*x)
                    self.gamma_dq_fourier_coef, pcov1 = curve_fit(fun, alpha_breakpoints_dq, gamma_dq_polar_I_nn,maxfev=20000)
                    print('Gamma_dq',"a0=", '%.4f'%self.gamma_dq_fourier_coef[0], "a1=", '%.4f'%self.gamma_dq_fourier_coef[1],"b1=", '%.4f'%self.gamma_dq_fourier_coef[2], \
                    "a2=", '%.4f'%self.gamma_dq_fourier_coef[3],"b2=", '%.4f'%self.gamma_dq_fourier_coef[4],"a3=", '%.4f'%self.gamma_dq_fourier_coef[5],"b3=", '%.4f'%self.gamma_dq_fourier_coef[6],\
                    "a4=", '%.4f'%self.gamma_dq_fourier_coef[7],"b4=", '%.4f'%self.gamma_dq_fourier_coef[8],"a5=", '%.4f'%self.gamma_dq_fourier_coef[9],"b5=", '%.4f'%self.gamma_dq_fourier_coef[10])
                    self.gamma_dq_fourier_rec = fun(self.alpha_breakpoints, *self.gamma_dq_fourier_coef)
                  
                    
                else :
                    print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
                    print('  impossible to compute the fourier coefficients of the dq inverse incremental inductance with fitting method')
                    print('  please use k between 2 and 5')
                    exit()
            
        # plot
        # if you need to plot outside, it is necessary to store alpha_breakpoints_delta
        # and gamma_delta_polar_I (there are some Nan to take into account)
        self.gamma_delta_polar_I = gamma_delta_polar_I
        self.gamma_dq_polar_I = gamma_dq_polar_I
        
        if plot:
            fig, ax = plt.subplots()
            ax.plot(self.alpha_breakpoints, gamma_delta_polar_I, lw=2, label="$\\gamma_\\Delta$ input")
            ax.plot(self.alpha_breakpoints, self.gamma_delta_fourier_rec, label="$\\gamma_\\Delta$ Fourier")
            ax.set(xlabel='alpha (rad)', ylabel='$\\gamma_\\Delta (H^{-1})$')
            plt.legend(loc="upper right")
            plt.show()
            fig, ax = plt.subplots()
            ax.plot(self.alpha_breakpoints, gamma_dq_polar_I, lw=2, label="$\\gamma_{dq}$ input")
            ax.plot(self.alpha_breakpoints, self.gamma_dq_fourier_rec, label="$\\gamma_{dq}$ Fourier")
            ax.set(xlabel='alpha (rad)', ylabel='$\\gamma_{dq} (H^{-1})$')
            plt.legend(loc="upper right")
            plt.show()
        
           
        # Reconstruction of signal Ihq
        try : 
            self.Ihq
        except: 
            print('  APOLLO ERROR in calc_fourier_inverse_inductances:')
            print('  impossible to compute the Ihq reconstruction ')
            print('  please compute function calc_convergence_region first')
            exit()
            
        # trasform Ihq into polar coordinates
        Ihq = self.Ihq.flatten()
        NaN_mask = ~np.isnan(Ihq) 
        Ihq=Ihq[NaN_mask]
        self.alpha_breakpoints = math.pi/180*np.linspace(0,360,1000) 
        self.Ihq_polar_I = griddata(points_I, Ihq, (self.alpha_breakpoints, [I] * 1000), method='cubic')
        theta_star = np.interp(I, self.i_MTPA, self.theta_MTPA) #MTPA angle for I current

        
        # Reconstruct Ihq with Fourier series of inverse incremental inductances
        if  np.mean(self.ldelta[~np.isnan(self.ldelta)]) < 0: 
            print('computed Ihq_fourier using REL convention')
            a=(self.gamma_dq_fourier_coef[4]+self.gamma_delta_fourier_coef[3])/2 
            c=self.gamma_delta_fourier_coef[0]
            e=(-self.gamma_dq_fourier_coef[4]+self.gamma_delta_fourier_coef[3])/2
            self.Ihq_fourier=self.Uh/(2*math.pi*self.fh)*(a*np.sin(-2*theta_star)+c*np.sin(2*self.alpha_breakpoints-2*theta_star)+e*np.sin(4*self.alpha_breakpoints-2*theta_star))
            
        else :
            print('computed Ihq_fourier using PMAREL convention')
            a=(-self.gamma_dq_fourier_coef[4]-self.gamma_delta_fourier_coef[3])/2 
            b=-self.gamma_dq_fourier_coef[2]/2
            c=-self.gamma_delta_fourier_coef[0]
            d=-b
            e=(self.gamma_dq_fourier_coef[4]-self.gamma_delta_fourier_coef[3])/2
            self.Ihq_fourier=self.Uh/(2*math.pi*self.fh)*(a*np.sin(-2*theta_star)+b*np.sin(self.alpha_breakpoints-2*theta_star)+\
            c*np.sin(2*self.alpha_breakpoints-2*theta_star)+d*np.sin(3*self.alpha_breakpoints-2*theta_star)+e*np.sin(4*self.alpha_breakpoints-2*theta_star))
            
        #plot real and approximated Ihq
        if plot:
            plt.plot(self.alpha_breakpoints, self.Ihq_polar_I, label="$I_{hq}$ input")
            plt.plot(self.alpha_breakpoints, self.Ihq_fourier, label="$I_{hq}$ Fourier")
            plt.plot(self.alpha_breakpoints, np.zeros(self.Ihq_polar_I.size), 'gray') # Ihq=0
            plt.plot(np.interp(I, self.i_MTPA, self.theta_MTPA), 0, 'ko') #MTPA angle for I current
            plt.xlabel(r'alpha (rad)'); plt.ylabel(r'$I_{hq}$ (A)')
            plt.legend(loc="upper right")
            plt.show() 
        
        self.I=I

    def calc_Ihq_few_points(self, I='', steps=1000, plot=False):
    
        """
        Author: Alice Maimeri
        Institution: University of Padua (Università degli Studi di Padova)
        
        This function calculates the reconstructed Ihq signal with only two gamma_delta and 
        gamma_dq measurements for a current modulus I [Maimeri2022Fast],[Maimeri2022Fasta].
        It uses the Ihq reconstructed function obtained with gamma_delta and gamma_dq Fourier analysis.
        Gamma_delta and gamma_dq measurements are for alpha = 45° and alpha =90°
        
        You set manually:
        
        I: the current modulus value I in which this function calculates the Ihq
        
        steps: the number of breakpoints to create the angle vector, default is 1000
        
        This function stores the results in the following variables:
        self.alpha_breakpoints: angles for polar coordinates
        self.alpha_breakpoints_NaN: : angles for polar coordinates without Nans
        self.gamma_delta_a0: constant term of Fourier analysis for gamma_delta
        self.gamma_delta_a2: second-order harmonic term of Fourier analysis for gamma_delta
        self.gamma_dq_b1: first-order harmonic term of Fourier analysis for gamma_dq
        self.gamma_dq_b2: second-order harmonic term of Fourier analysis for gamma_dq
        self.Ihq_fourier_2meas: the reconstructed Ihq using the Fourier series of inverse incremental inductances

        """
    
        # trasform into polar coordinates
        alpha_array = np.arctan2(self.i_q, self.i_d)
        alpha_array = np.mod(alpha_array, 2*np.pi) 
        I_array = np.sqrt(self.i_d**2 + self.i_q**2)

        # Define self.alpha_breakpoints
        self.alpha_breakpoints = math.pi/180*np.linspace(0,360,steps)
        
        # convert two-dimensional vector into one-dimensional 
        alpha_array = alpha_array.flatten()
        I_array = I_array.flatten()
        gamma_delta = self.gamma_delta.flatten()
        gamma_dq = self.gamma_dq.flatten()
        
        # remove NaN
        NaN_mask = ~np.isnan(gamma_delta) 
        I_array = I_array[NaN_mask]
        alpha_array = alpha_array[NaN_mask] 
        gamma_delta = gamma_delta[NaN_mask]
        gamma_dq = gamma_dq[NaN_mask]
        
        points_I = np.vstack((alpha_array, I_array)).T    
    
        if I=='' :
            print('  APOLLO ERROR in calc_Ihq_few_points:')
            print('  impossible to compute the Ihq with few measurements')
            print('  please set the current modulus value')
            exit()
            
        elif I < 0 or I > np.min([np.max(np.max(self.i_d)),np.max(np.max(self.i_q))]) :
            print('  APOLLO ERROR in calc_Ihq_few_points:')
            print('  impossible to compute the Ihq with few measurements')
            print('  please set I values between 0 and i_d_max or i_q_max')
            exit()
            
        # compute griddata for the current modulus value I
        gamma_delta_polar_I = griddata(points_I, gamma_delta, (self.alpha_breakpoints, [I] * steps), method='cubic')
        gamma_dq_polar_I    = griddata(points_I, gamma_dq, (self.alpha_breakpoints, [I] * steps), method='cubic')
        print('I=',I)
        
        # remove NaN
        NaN_mask = ~np.isnan(gamma_delta_polar_I) 
        gamma_delta_polar_I = gamma_delta_polar_I[NaN_mask]
        gamma_dq_polar_I = gamma_dq_polar_I[NaN_mask]
        self.alpha_breakpoints_NaN = self.alpha_breakpoints[NaN_mask] 
        
        try :
            self.Ihq
        except : 
            print('  APOLLO ERROR in calc_Ihq_few_points:')
            print('  impossible to compute the Ihq with few measurements')
            print('  please compute function calc_convergence_region first')
            exit()
            
        # trasform Ihq into polar coordinates
        Ihq = self.Ihq.flatten()
        NaN_mask = ~np.isnan(Ihq) 
        Ihq=Ihq[NaN_mask]
        Ihq_polar_I = griddata(points_I, Ihq, (self.alpha_breakpoints, [I] * steps), method='cubic')
        # Ihq_polar_I =Ihq_polar_I [NaN_mask]
        theta_star = np.interp(I, self.i_MTPA, self.theta_MTPA) #MTPA angle for I current  
        
        # Reconstruct Ihq with two measurements of the inverse incremental inductances gamma_delta and gamma_dq
        if  np.mean(self.ldelta[~np.isnan(self.ldelta)]) < 0: 
            print('computed Ihq_fourier_2meas using REL convention')
        
            gamma_delta_90=np.interp(math.pi/2,self.alpha_breakpoints_NaN,gamma_delta_polar_I)          
            gamma_delta_45=np.interp(math.pi/4,self.alpha_breakpoints_NaN,gamma_delta_polar_I)          
            gamma_dq_45=np.interp(math.pi/4,self.alpha_breakpoints_NaN,gamma_dq_polar_I)
            
            self.gamma_delta_a0=gamma_delta_45                 
            self.gamma_delta_a2=gamma_delta_45-gamma_delta_90
            self.gamma_dq_b2=gamma_dq_45

            a_rec=(self.gamma_delta_a2+self.gamma_dq_b2)/2 
            c_rec=self.gamma_delta_a0
            e_rec=(-self.gamma_dq_b2+self.gamma_delta_a2)/2

            self.Ihq_fourier_2meas=self.Uh/(2*math.pi*self.fh)*(a_rec*np.sin(-2*theta_star)+c_rec*np.sin(2*self.alpha_breakpoints-2*theta_star)+e_rec*np.sin(4*self.alpha_breakpoints-2*theta_star))
        
        else:
            print('computed Ihq_fourier_2meas using PMAREL convention')
        
            gamma_delta_90 = np.interp(math.pi/2,self.alpha_breakpoints_NaN,gamma_delta_polar_I)
            gamma_delta_135 = np.interp(3*math.pi/4,self.alpha_breakpoints_NaN,gamma_delta_polar_I)
            gamma_dq_90 = np.interp(math.pi/2,self.alpha_breakpoints_NaN,gamma_dq_polar_I)
            gamma_dq_135 = np.interp(3*math.pi/4,self.alpha_breakpoints_NaN,gamma_dq_polar_I)
            
            self.gamma_delta_a0=gamma_delta_135
            self.gamma_delta_a2=gamma_delta_135-gamma_delta_90
            self.gamma_dq_b1=gamma_dq_90
            self.gamma_dq_b2=-(gamma_dq_135-(gamma_dq_90*np.sin(3*math.pi/4)))

            a_rec=(-self.gamma_delta_a2-self.gamma_dq_b2)/2 
            b_rec=-self.gamma_dq_b1/2
            c_rec=-self.gamma_delta_a0
            d_rec=-b_rec
            e_rec=(-self.gamma_delta_a2+self.gamma_dq_b2)/2

            self.Ihq_fourier_2meas=self.Uh/(2*math.pi*self.fh)*(a_rec*np.sin(-2*theta_star)+b_rec*np.sin(self.alpha_breakpoints-2*theta_star)+c_rec*np.sin(2*self.alpha_breakpoints-2*theta_star)
            +d_rec*np.sin(3*self.alpha_breakpoints-2*theta_star)+e_rec*np.sin(4*self.alpha_breakpoints-2*theta_star))
            
        # plot
        if plot:
              
            try :
                self.Ihq_fourier
                if I==self.I :
                    plt.plot(self.alpha_breakpoints, Ihq_polar_I, label="$I_{hq}$ input")
                    plt.plot(self.alpha_breakpoints, self.Ihq_fourier, label="$I_{hq}$ Fourier")
                    plt.plot(self.alpha_breakpoints, self.Ihq_fourier_2meas, label="$I_{hq}$ TwoMeas")
                    plt.plot(self.alpha_breakpoints, np.zeros(Ihq_polar_I.size), 'gray') # Ihq=0
                    plt.plot(np.interp(I, self.i_MTPA, self.theta_MTPA), 0, 'ko') #MTPA angle for I current
                    plt.xlabel(r'alpha (rad)'); plt.ylabel(r'$I_{hq}$ (A)')
                    plt.legend(loc="upper right")
                    plt.show()                
                else:
                    print('  APOLLO ERROR in calc_Ihq_few_points:')
                    print('  impossible to show plots with Ihq_fourier')
                    print('  please set the same current amplitude I')
                    exit()
 
            except :
                plt.plot(self.alpha_breakpoints, Ihq_polar_I, label="$I_{hq}$ input")
                plt.plot(self.alpha_breakpoints, self.Ihq_fourier_2meas, label="$I_{hq}$ TwoMeas")
                plt.plot(self.alpha_breakpoints, np.zeros(Ihq_polar_I.size), 'gray') # Ihq=0
                plt.plot(np.interp(I, self.i_MTPA, self.theta_MTPA), 0, 'ko') #MTPA angle for I current
                plt.xlabel(r'alpha (rad)'); plt.ylabel(r'$I_{hq}$ (A)')
                plt.legend(loc="upper right")
                plt.show() 



def create_dataframe_SPMSM_ideal(L, lambda_PM, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of an ideal Surface Permanent Magnet Synchronous Motor (no saturation)
    """
    
    # protect against negative parameters
    if L<0:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the inductance L that has been provided is negative')
        print('  please provide a positive value of L')
        exit()
        
    if lambda_PM<0:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the permanent magnet flux lambda_PM that has been provided is negative')
        print('  please provide a positive value of lambda_PM')
        exit()
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_SPMSM_ideal:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()   

    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
    
    # compute the flux linkages
    lambda_d = L * i_d + lambda_PM
    lambda_q = L * i_q
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = lambda_d
    data['lambda_q'] = lambda_q
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe


def create_dataframe_IPMSM_ideal(Ld, Lq, lambda_PM, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of an ideal Interior Permanent Magnet Synchronous Motor (no saturation)
    """
    
    # protect against negative parameters
    if Ld<0:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the inductance Ld that has been provided is negative')
        print('  please provide a positive value of Ld')
        exit()
        
    if Lq<0:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the inductance Lq that has been provided is negative')
        print('  please provide a positive value of Lq')
        exit()
        
    if lambda_PM<0:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the permanent magnet flux lambda_PM that has been provided is negative')
        print('  please provide a positive value of lambda_PM')
        exit()
        
    # protect against wrong convention
    if Ld>Lq:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided inductance Ld is greater than Lq, that is not consistent with the IPMSM/PMA-SynRM convention')
        print('  please provide a value of Lq greater than Ld')
        exit()
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_IPMSM_ideal:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()
    
    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
    
    # compute the flux linkages
    lambda_d = Ld * i_d + lambda_PM
    lambda_q = Lq * i_q
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = lambda_d
    data['lambda_q'] = lambda_q
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe

    
def create_dataframe_SynRM_ideal(Ld, Lq, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of an ideal Synchronous Reluctance Motor (no saturation)
    """
    
    # protect against negative parameters
    if Ld<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the inductance Ld that has been provided is negative')
        print('  please provide a positive value of Ld')
        exit()
        
    if Lq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the inductance Lq that has been provided is negative')
        print('  please provide a positive value of Lq')
        exit()
        
    # protect against wrong convention
    if Ld<Lq:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided inductance Lq is greater than Ld, that is not consistent with the SynRM convention')
        print('  please provide a value of Ld greater than Lq')
        exit()
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_SynRM_ideal:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()
    
    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
    
    # compute the flux linkages
    lambda_d = Ld * i_d
    lambda_q = Lq * i_q
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = lambda_d
    data['lambda_q'] = lambda_q
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe


def create_dataframe_SynRM_saturated(ad0, add, aq0, aqq, adq, S, T, U, V, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of a saturated Synchronous Reluctance Motor - model in [Hinkkanen2017Sensorless]
    
    Example: 2.2 kW SynRM [Hinkkanen2017Sensorless]
    ad0= 2.41, add= 1.47, aq0= 12.8, aqq= 17.0, adq= 13.2, S= 5, T= 1, U= 1, V= 0

    Example: 6.7 kW SynRM [Awan2019Flux]. Parameters converted from p.u. to SI units
    ad0= 17.36, add= 373.25, aq0= 52.09, aqq= 658.05, adq= 1120.32, S= 5, T= 1, U= 1, V= 0
    """
    
    # protect against negative parameters
    if ad0<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the coefficient ad0 that has been provided is negative')
        print('  please provide a positive value of ad0')
        exit()
        
    if add<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the coefficient add that has been provided is negative')
        print('  please provide a positive value of add')
        exit()
        
    if aq0<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the coefficient aq0 that has been provided is negative')
        print('  please provide a positive value of aq0')
        exit()
        
    if aqq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the coefficient aqq that has been provided is negative')
        print('  please provide a positive value of aqq')
        exit()
        
    if adq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the coefficient adq that has been provided is negative')
        print('  please provide a positive value of adq')
        exit()
        
    if S<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the exponent S that has been provided is negative')
        print('  please provide a positive value of S')
        exit()
        
    if T<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the exponent T that has been provided is negative')
        print('  please provide a positive value of T')
        exit()
        
    if U<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the exponent U that has been provided is negative')
        print('  please provide a positive value of U')
        exit()
        
    if V<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the exponent V that has been provided is negative')
        print('  please provide a positive value of V')
        exit()
      
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()
    
    # compute the unsaturated inductances
    Ld0 = 1/ad0
    Lq0 = 1/aq0
    
    # set flux-linkages grid
    FluxD_min = Ld0 * Id_min
    FluxD_max = Ld0 * Id_max
    n_FluxD = 300
    FluxQ_min = Lq0 * Iq_min
    FluxQ_max = Lq0 * Iq_max
    n_FluxQ = 300
    
    FluxD_breakpoints_fg = np.linspace(FluxD_min, FluxD_max, n_FluxD)
    FluxQ_breakpoints_fg = np.linspace(FluxQ_min, FluxQ_max, n_FluxQ)
    FluxD_fg, FluxQ_fg = np.meshgrid(FluxD_breakpoints_fg, FluxQ_breakpoints_fg, indexing='ij')
    
    # compute currents - eq (12) in [Hinkkanen2017Sensorless]
    Id_fg = (ad0 + add * np.power(np.absolute(FluxD_fg), S) + adq / (V + 2) *
             np.power(np.absolute(FluxD_fg), U) * np.power(np.absolute(FluxQ_fg), V + 2)) * FluxD_fg
    Iq_fg = (aq0 + aqq * np.power(np.absolute(FluxQ_fg), T) + adq / (U + 2) *
             np.power(np.absolute(FluxD_fg), U + 2) * np.power(np.absolute(FluxQ_fg), V)) * FluxQ_fg
           
    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
    
    # maps inversion
    FluxD = griddata_wrapper(Id_fg, Iq_fg, FluxD_fg, i_d, i_q)
    FluxQ = griddata_wrapper(Id_fg, Iq_fg, FluxQ_fg, i_d, i_q)
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = FluxD
    data['lambda_q'] = FluxQ
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe
    
def create_dataframe_SynRM_saturated_arctan(Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of a saturated Synchronous Reluctance Motor - model in [Woo2023Flux]
    
    Example: 1.5 kW SynRM [Woo2023Flux]
    Ad= 0.2403, Bd= 0.3399, Cd= 0.0028, Aq= 0.0422, Bq= 0.1841, Cq= 0.0052, Kd= 225, Kq= 225, Ddq= -0.9198
    """
    
    # protect against wrong parameters sign
    if Ad<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Ad that has been provided is negative')
        print('  please provide a positive value of Ad')
        exit()
        
    if Bd<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Bd that has been provided is negative')
        print('  please provide a positive value of Bd')
        exit()
        
    if Cd<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Cd that has been provided is negative')
        print('  please provide a positive value of Cd')
        exit()
        
    if Aq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Aq that has been provided is negative')
        print('  please provide a positive value of Aq')
        exit()
        
    if Bq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Bq that has been provided is negative')
        print('  please provide a positive value of Bq')
        exit()
        
    if Cq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Cq that has been provided is negative')
        print('  please provide a positive value of Cq')
        exit()
        
    if Kd<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Kd that has been provided is negative')
        print('  please provide a positive value of Kd')
        exit()
        
    if Kq<0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Kq that has been provided is negative')
        print('  please provide a positive value of Kq')
        exit()
        
    if Ddq>0:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the coefficient Ddq that has been provided is positive')
        print('  please provide a negative value of Ddq')
        exit()
      
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_SynRM_saturated_arctan:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()
    
    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
      
    # compute fluxes - eq (6a)-(6-b) in [Woo2023Flux]
    FluxD = Ad*np.arctan(Bd*i_d) + Cd*i_d + Ddq*i_d/(i_d**2+Kd)*np.log(1+i_q**2/Kq)
    FluxQ = Aq*np.arctan(Bq*i_q) + Cq*i_q + Ddq*i_q/(i_q**2+Kq)*np.log(1+i_d**2/Kd)
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = FluxD
    data['lambda_q'] = FluxQ
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe
    
    
def create_dataframe_PMA_SynRM_saturated(ad0, add, aq0, aqq, adq, a_b, a_bp, k_q, psi_f, S, T, U, V, W, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of a saturated Permanet Magnet Assisted Synchronous Reluctance Motor - model in [Lelli2024Saturation].
    a_b, a_bp, k_q, psi_f, W relate to the bridge.
    
    Example: 5.5 kW PMA-SynRM [https://github.com/Aalto-Electric-Drives/motulator/commit/2949aa8142946e9c4dfb06252f0b25ff5e141c2c] 
    ad0= 3.96, add= 28.46, aq0= 5.89, aqq= 2.672, adq= 41.52, a_b= 81.75, a_bp= 1, k_q= 0.1, psi_f= 0.804, S = 4, T = 6, U = 1, V = 1, W= 2
    """
    
    # protect against negative parameters
    if ad0<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient ad0 that has been provided is negative')
        print('  please provide a positive value of ad0')
        exit()
        
    if add<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient add that has been provided is negative')
        print('  please provide a positive value of add')
        exit()
        
    if aq0<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient aq0 that has been provided is negative')
        print('  please provide a positive value of aq0')
        exit()
        
    if aqq<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient aqq that has been provided is negative')
        print('  please provide a positive value of aqq')
        exit()
        
    if adq<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient adq that has been provided is negative')
        print('  please provide a positive value of adq')
        exit()
        
    if a_b<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient a_b that has been provided is negative')
        print('  please provide a positive value of a_b')
        exit()
        
    if a_bp<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient a_bp that has been provided is negative')
        print('  please provide a positive value of a_bp')
        exit()    
        
    if k_q<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient k_q that has been provided is negative')
        print('  please provide a positive value of k_q')
        exit()     
        
    if psi_f<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the coefficient psi_f that has been provided is negative')
        print('  please provide a positive value of psi_f')
        exit()    
        
    if S<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the exponent S that has been provided is negative')
        print('  please provide a positive value of S')
        exit()
        
    if T<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the exponent T that has been provided is negative')
        print('  please provide a positive value of T')
        exit()
        
    if U<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the exponent U that has been provided is negative')
        print('  please provide a positive value of U')
        exit()
        
    if V<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the exponent V that has been provided is negative')
        print('  please provide a positive value of V')
        exit()
        
    if W<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the exponent W that has been provided is negative')
        print('  please provide a positive value of W')
        exit()
      
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()
        
        
    # compute the unsaturated inductances
    Ld0 = 1/ad0
    Lq0 = 1/aq0
    
    # set flux-linkages grid
    FluxD_min = Ld0 * Id_min
    FluxD_max = Ld0 * Id_max
    n_FluxD = 300
    FluxQ_min = Lq0 * Iq_min
    FluxQ_max = Lq0 * Iq_max
    n_FluxQ = 300
    
    FluxD_breakpoints_fg = np.linspace(FluxD_min, FluxD_max, n_FluxD)
    FluxQ_breakpoints_fg = np.linspace(FluxQ_min, FluxQ_max, n_FluxQ)
    FluxD_fg, FluxQ_fg = np.meshgrid(FluxD_breakpoints_fg, FluxQ_breakpoints_fg, indexing='ij')
    
    # https://github.com/Aalto-Electric-Drives/motulator/commit/2949aa8142946e9c4dfb06252f0b25ff5e141c2c
    
    # Inverse inductance functions for the d- and q-axis
    G_d = (ad0 + add * np.power(np.absolute(FluxD_fg), S) + adq / (V + 2) *
           np.power(np.absolute(FluxD_fg), U) * np.power(np.absolute(FluxQ_fg), V + 2))
    G_q = (aq0 + aqq * np.power(np.absolute(FluxQ_fg), T) + adq / (U + 2) *
           np.power(np.absolute(FluxD_fg), U + 2) * np.power(np.absolute(FluxQ_fg), V))
                
    # Bridge flux
    psi_b = FluxD_fg - psi_f
    # State of the bridge saturation depends also on the q-axis flux
    psi_b_sat = np.sqrt(psi_b**2 + k_q*FluxQ_fg**2)
    # Inverse inductance function for the bridge saturation
    G_b = a_b*psi_b_sat**W/(1 + a_bp*psi_b_sat**W)
    
    # Stator current
    Id_fg = G_d * FluxD_fg + G_b * psi_b
    Iq_fg = G_q * FluxQ_fg + k_q * G_b * FluxQ_fg
    
    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
    
    # maps inversion
    FluxD = griddata_wrapper(Id_fg, Iq_fg, FluxD_fg, i_d, i_q)
    FluxQ = griddata_wrapper(Id_fg, Iq_fg, FluxQ_fg, i_d, i_q)
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = FluxD
    data['lambda_q'] = FluxQ
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe
    
def create_dataframe_PMA_SynRM_saturated_arctan(Ad, Bd, Cd, Aq, Bq, Cq, Kd, Kq, Ddq, i_f, Id_min, Id_max, n_Id, Iq_min, Iq_max, n_Iq):
    """
    Create the flux maps of a saturated Interior Permanent Magnet Synchronous Motor - model in [Lee2023Standstill]
    
    Example: 11 kW IPMSM [Lee2023Standstill]
    Ad= 0.59, Bd= 0.0065, Cd= 0, Aq= 0.34, Bq= 0.0157, Cq= 0, Kd= 19720.1, Kq= 7021.1, Ddq= -6.28, i_f= 77.3
    """
    
    # protect against wrong parameters sign
    if Ad<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Ad that has been provided is negative')
        print('  please provide a positive value of Ad')
        exit()
        
    if Bd<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Bd that has been provided is negative')
        print('  please provide a positive value of Bd')
        exit()
        
    if Cd<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Cd that has been provided is negative')
        print('  please provide a positive value of Cd')
        exit()
        
    if Aq<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Aq that has been provided is negative')
        print('  please provide a positive value of Aq')
        exit()
        
    if Bq<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Bq that has been provided is negative')
        print('  please provide a positive value of Bq')
        exit()
        
    if Cq<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Cq that has been provided is negative')
        print('  please provide a positive value of Cq')
        exit()
        
    if Kd<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Kd that has been provided is negative')
        print('  please provide a positive value of Kd')
        exit()
        
    if Kq<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Kq that has been provided is negative')
        print('  please provide a positive value of Kq')
        exit()
        
    if Ddq>0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient Ddq that has been provided is positive')
        print('  please provide a negative value of Ddq')
        exit()
        
    if i_f<0:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the coefficient i_f that has been provided is negative')
        print('  please provide a positive value of i_f')
        exit()
      
        
    # protect against inconsistent grid values
    if Id_min>=Id_max:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the provided Id_min is greater than or equal to Id_max')
        print('  please provide a value of Id_max greater than Id_min')
        exit()

    if int(n_Id)!=n_Id:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the provided number of I_d breakpoints is not an integer')
        print('  please provide an integer value for the number of I_d breakpoints')
        exit()
        
    if n_Id<2:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the provided number of I_d breakpoints is lower than 2')
        print('  please provide a number I_d breakpoints which is greater than or equal to 2')
        exit()
        
    if Iq_min>=Iq_max:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the provided Iq_min is greater than or equal to Iq_max')
        print('  please provide a value of Iq_max greater than Iq_min')
        exit()
        
    if int(n_Iq)!=n_Iq:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the provided number of I_q breakpoints is not an integer')
        print('  please provide an integer value for the number of I_q breakpoints')
        exit()
        
    if n_Iq<2:
        print('  APOLLO ERROR in create_dataframe_PMA_SynRM_saturated_arctan:')
        print('  the provided number of I_q breakpoints is lower than 2')
        print('  please provide a number I_q breakpoints which is greater than or equal to 2')
        exit()
    
    # create the Id-Iq grid
    Id_breakpoints = np.linspace(Id_min, Id_max, n_Id)
    Iq_breakpoints = np.linspace(Iq_min, Iq_max, n_Iq)
    i_d, i_q = np.meshgrid(Id_breakpoints, Iq_breakpoints,indexing='ij')
    i_d = i_d.flatten()
    i_q = i_q.flatten()
      
    # compute fluxes - eq (8) in [Lee2023Standstill]
    i_dm = i_d + i_f
    FluxD = Ad*np.arctan(Bd*i_dm) + Cd*i_dm + Ddq*i_dm/(i_dm**2+Kd)*np.log(1+i_q**2/Kq)
    FluxQ = Aq*np.arctan(Bq*i_q) + Cq*i_q + Ddq*i_q/(i_q**2+Kq)*np.log(1+i_dm**2/Kd)
    
    # define a dataframe
    data = pd.DataFrame()
    
    # store the values in the dataframe
    data['i_d'] = i_d
    data['i_q'] = i_q
    data['lambda_d'] = FluxD
    data['lambda_q'] = FluxQ
    data['torque'] = 0
    data['theta_m'] = 0  # assign a default value
    
    return data #dataframe


def griddata_wrapper(input_grid_x, input_grid_y, input_map, query_grid_x, query_grid_y):
    """
    a wrapper function to call scipy.interpolate.griddata adjusting the inputs consistently
    
    """
    ## convert to one dimensional arrays
    input_vector_x = input_grid_x.flatten()
    input_vector_y = input_grid_y.flatten()
    input_vector_map = input_map.flatten()
    
    # remove NaNs
    NaN_mask = ~np.isnan(input_vector_map) # indices of the non-NaNs
    input_vector_x = input_vector_x[NaN_mask] # apply mask to keep the non-NaNs, i.e. remove NaNs
    input_vector_y = input_vector_y[NaN_mask]
    input_vector_map = input_vector_map[NaN_mask]
    
    # interpolate with griddata on the desired query grid
    output_map = griddata(np.vstack((input_vector_x, input_vector_y)).T, input_vector_map, (query_grid_x, query_grid_y), method='cubic')
    
    return output_map

    
def cartesian_to_polar(input_grid_x, input_grid_y, input_map, alpha_ie_min=0, alpha_ie_max=359, alpha_ie_step=1, I_min=0, I_max=None, I_step=None):
    """
    convert a map from cartesian to polar coordinates (electrical degrees)
    
    """
    ## protect against inconsistent grid values
    if alpha_ie_min<0:
        print('  APOLLO ERROR in cartesian_to_polar:')
        print('  the provided alpha_ie_min is lower than zero')
        print('  please provide a value of alpha_ie_min greater than or equal to zero')
        exit()
    
    if alpha_ie_max>360:
        print('  APOLLO ERROR in cartesian_to_polar:')
        print('  the provided alpha_ie_max is greater than 360 electrical degrees')
        print('  please provide a value of alpha_ie_max lower than or equal to 360')
        exit()
        
    alpha_ie_num = int((alpha_ie_max-alpha_ie_min)/alpha_ie_step+1) #compute the number of current angle breakpoint alpha_ie_num

    if alpha_ie_num!=(alpha_ie_max-alpha_ie_min)/alpha_ie_step+1:
        print('  APOLLO ERROR in cartesian_to_polar:')
        print('  the provided number of alpha_ie breakpoints is not an integer')
        print('  please provide an integer value for the number of alpha_ie breakpoints')
        exit()
        
    if alpha_ie_num<2:
        print('  APOLLO ERROR in cartesian_to_polar:')
        print('  the provided number of alpha_ie breakpoints is lower than 2')
        print('  please provide a number of alpha_ie breakpoints which is greater than or equal to 2')
        exit()
            
    ## compute da maximum value of current magnitude I if not passed
    if I_max==None:
        I_max = np.sqrt(np.max(np.max(input_grid_x))**2 + np.max(np.max(input_grid_y))**2)
    
    ## compute the number of the current magnitude breakpoints I_num
    if I_step==None:
        num_rows, num_cols = input_grid_x.shape
        I_num = np.max([num_rows, num_cols])
    else:
        I_num = (I_max-I_min)/I_step + 1
        
        if I_num!=(I_max-I_min)/I_step+1:
            print('  APOLLO ERROR in cartesian_to_polar:')
            print('  the provided number of I breakpoints is not an integer')
            print('  please provide an integer value for the number of I breakpoints')
            exit()
        
        if alpha_ie_num<2:
            print('  APOLLO ERROR in cartesian_to_polar:')
            print('  the provided number of I breakpoints is lower than 2')
            print('  please provide a number of I breakpoints which is greater than or equal to 2')
            exit()
            
           
    ## calc the breakpoints of the query grid
    alpha_ie_breakpoints = np.linspace(start=alpha_ie_min, stop=alpha_ie_max, num=alpha_ie_num)
    I_breakpoints = np.linspace(start=I_min, stop=I_max, num=I_num)
        
    ## create the polar grid
    alpha_ie_grid, I_grid = np.meshgrid(alpha_ie_breakpoints, I_breakpoints, indexing='ij')
    
    # trasform the input (cartesian) grid into polar coordinates
    alpha_array = np.arctan2(input_grid_y, input_grid_x)
    alpha_array = np.mod(alpha_array, 2*np.pi)*180/np.pi
    I_array = np.sqrt(input_grid_x**2 + input_grid_y**2)
    
    ## interpolate the cartesian data on the polar grid
    output_map = griddata_wrapper(alpha_array, I_array, input_map, alpha_ie_grid, I_grid)
    
    return alpha_ie_grid, I_grid, output_map
    
    
def polar_to_cartesian(input_grid_alpha_ie, input_grid_I, input_map, query_grid_x, query_grid_y):
    """
    convert a map from polar (electrical degrees) to cartesian coordinates
    
    """  
    # trasform the input (polar) grid into cartesian coordinates
    i_d_array = input_grid_I * np.cos(input_grid_alpha_ie*np.pi/180)
    i_q_array = input_grid_I * np.sin(input_grid_alpha_ie*np.pi/180)
    
    ## interpolate the cartesian data on the polar grid
    output_map = griddata_wrapper(i_d_array, i_q_array, input_map, query_grid_x, query_grid_y)
    
    return output_map
    

def intgrad1(fx,dx=1,f1=0):
    """
    Author: Matteo Berto
    Institution: University of Padua (Università degli Studi di Padova)
    Date: 10th September 2024
    https://github.com/matteo-berto/intgrad1-python
    
    Python implementation of MATLAB "intgrad1" function by John D'Errico.
    
    Note: only the method 2 (integrated spline model) has been implemented.
    
    John D'Errico (2024). Inverse (integrated) gradient
    (https://www.mathworks.com/matlabcentral/fileexchange/9734-inverse-integrated-gradient)
    MATLAB Central File Exchange. Retrieved September 10, 2024.
    
    intgrad: generates a vector, integrating derivative information.
    usage: fhat = intgrad1(dfdx)
    usage: fhat = intgrad1(dfdx,dx)
    usage: fhat = intgrad1(dfdx,dx,f1)
    usage: fhat = intgrad1(dfdx,dx,f1,method)

    arguments: (input)
     dfdx - vector of length nx, as gradient would have produced.

       dx - (OPTIONAL) scalar or vector - denotes the spacing in x
            if dx is a scalar, then spacing in x (the column index
            of fx and fy) will be assumed to be constant = dx.
            if dx is a vector, it denotes the actual coordinates
            of the points in x (i.e., the column dimension of fx
            and fy.) length(dx) == nx

            DEFAULT: dx = 1

       f1 - (OPTIONAL) scalar - defines the first eleemnt of fhat
            after integration. This is just the constant of integration.

            DEFAULT: f1 = 0

       method = 2 --> integrated spline model. This will almost always
            be the most accurate among the alternative methods.
            
    arguments: (output)
      fhat - vector of length nx, containing the integrated function

    Example usage: 
     x = 0:.001:1;
     f = exp(x) + exp(-x);
     dfdx = exp(x) - exp(-x);
     fhat = intgrad1(dfdx,.001,2,2)
    Author; John D'Errico
    Current release: 2
    Date of release: 1/27/06
    size
    """
    if len(fx.shape)>1:
        raise Exception('dfdx must be a vector.')
    sx = np.array([dx]).shape
    nx = fx.size
    if nx<2:
      raise Exception('dfdx must be a vector of length >= 2')
    # if scalar spacings, expand them to be vectors
    uniflag = 1
    if np.array([dx]).size == 1:
      mdx = dx# mean of dx
      xp = np.linspace(0,nx-1,num=nx)*dx
      dx = np.matlib.repmat(dx,nx-1,1)
    elif np.array([dx]).size==nx:
      # dx was a vector, use diff to get the spacing
      xp = dx
      dx = np.diff(dx)
      mdx = np. mean(dx)
      ddx = np.diff(dx)
      if np.any(dx<=0):
        raise Exception('x points must be monotonic increasing')
      elif np.any(np.abs(ddx)>(xp[-1]*1.e-15)):
        uniflag = 0
    else:
      raise Exception('dx is not a scalar or of length == nx')
    if np.size(f1) > 1 or np.isnan(f1) or not np.isfinite(f1): #|| ~isnumeric(f11)
      raise Exception('f1 must be a finite scalar numeric variable.')

    # case 2
    # integrate a spline model
    pp = CubicSpline(xp,fx)
    c = pp.c.T
    fhat = dx*(c[:,3] + dx*(c[:,2]/2 + dx*(c[:,1]/3 + dx*c[:,0]/4)))
    fhat = f1+np.insert(np.cumsum(fhat),0,0)
    
    return fhat


def intgrad2(fx,fy,dx=1,dy=1,f11=0):
    """
    Author: Matteo Berto
    Institution: University of Padua (Università degli Studi di Padova)
    Date: 28th August 2024
    https://github.com/matteo-berto/intgrad2-python
    
    Python implementation of MATLAB "intgrad2" function by John D'Errico.
    
    John D'Errico (2024). Inverse (integrated) gradient
    (https://www.mathworks.com/matlabcentral/fileexchange/9734-inverse-integrated-gradient)
    MATLAB Central File Exchange. Retrieved August 28, 2024.
    
    intgrad2: generates a 2-d surface, integrating gradient information.
    usage: fhat = intgrad2(fx,fy)
    usage: fhat = intgrad2(fx,fy,dx,dy)
    usage: fhat = intgrad2(fx,fy,dx,dy,f11)
    
    arguments: (input)
     fx,fy - (ny by nx) arrays, as gradient would have produced. fx and
             fy must both be the same size. Note that x is assumed to
             be the column dimension of f, in the meshgrid convention.
    
             nx and ny must both be at least 2.
    
             fx and fy will be assumed to contain consistent gradient
             information. If they are inconsistent, then the generated
             gradient will be solved for in a least squares sense.
    
             Central differences will be used where possible.
                
        dx - (OPTIONAL) scalar or vector - denotes the spacing in x
             if dx is a scalar, then spacing in x (the column index
             of fx and fy) will be assumed to be constant = dx.
             if dx is a vector, it denotes the actual coordinates
             of the points in x (i.e., the column dimension of fx
             and fy.) length(dx) == nx
    
             DEFAULT: dx = 1
                
        dy - (OPTIONAL) scalar or vector - denotes the spacing in y
             if dy is a scalar, then the spacing in x (the row index
             of fx and fy) will be assumed to be constant = dy.
             if dy is a vector, it denotes the actual coordinates
             of the points in y (i.e., the row dimension of fx
             and fy.) length(dy) == ny
    
             DEFAULT: dy = 1
                
        f11 - (OPTIONAL) scalar - defines the (1,1) element of fhat
             after integration. This is just the constant of integration.

             DEFAULT: f11 = 0
        
    arguments: (output)
      fhat - (nx by ny) array containing the integrated gradient

    Example usage 1: (Note x is uniform in spacing, y is not.)
     xp = 0:.1:1;
     yp = [0 .1 .2 .4 .8 1];
     [x,y]=meshgrid(xp,yp);
     f = exp(x+y) + sin((x-2*y)*3);
     [fx,fy]=gradient(f,.1,yp);
     fhat = intgrad2(fx,fy,.1,yp,1)

    Example usage 2: Large grid, 101x101
     xp = 0:.01:1;
     yp = 0:.01:1;
     [x,y]=meshgrid(xp,yp);
     f = exp(x+y) + sin((x-2*y)*3);
     [fx,fy]=gradient(f,.01);
     fhat = intgrad2(fx,fy,.01,.01,1)
     
    Author; John D'Errico
    Current release: 2
    Date of release: 1/27/06
    size 
    """
    print('running intgrad2...')
    
    if len(fx.shape)>2 or len(fy.shape)>2:
        raise Exception('fx and fy must be 2d arrays')
    [ny,nx] = fx.shape
    if nx!=np.shape(fy)[1] or ny!=np.shape(fy)[0]:
        raise Exception('fx and fy must be the same sizes')
    if nx<2 or ny<2:
        raise Exception('fx and fy must be at least 2x2 arrays')
    # if scalar spacings, expand them to be vectors
    # dx=dx.flatten()
    if np.size(dx) == 1:
        dx = np.matlib.repmat(dx,nx-1,1)
    elif np.size(dx)==nx:
        # dx was a vector, use diff to get the spacing
        dx = np.atleast_2d(np.diff(dx)).T
    else:
        raise Exception('dx is not a scalar or of length == nx')
    # dy=dy.flatten()
    if np.size(dy) == 1:
        dy = np.matlib.repmat(dy,ny-1,1)
    elif np.size(dy)==ny:
        # dy was a vector, use diff to get the spacing
        dy = np.atleast_2d(np.diff(dy)).T
    else:
        raise Exception('dy is not a scalar or of length == ny')
    if np.size(f11) > 1 or np.isnan(f11) or not np.isfinite(f11): #|| ~isnumeric(f11) ||
        raise Exception('f11 must be a finite scalar numeric variable')
    # build gradient design matrix, sparsely. Use a central difference
    # in the body of the array, and forward/backward differences along
    # the edges.
    # A will be the final design matrix. it will be sparse.
    # The unrolling of F will be with row index running most rapidly.
    rhs = np.zeros([2*nx*ny,1])
    # but build the array elements in Af
    Af = np.zeros([2*nx*ny,6])
    L = 0;
    # do the leading edge in x, forward difference
    indx = 1
    indy = np.atleast_2d((np.linspace(1, ny, num=ny))).T
    ind = indy + (indx-1)*ny
    rind = np.matlib.repmat(L+indy,1,2)
    cind = np.concatenate((ind,ind+ny),1)
    dfdx = np.matlib.repmat([-1, 1]/dx[0],ny,1)
    Af[(L+np.linspace(1, ny, num=ny)-1).astype(int), :] = np.concatenate((rind,cind,dfdx),1)
    rhs[(L+np.linspace(1, ny, num=ny)-1).astype(int)] = (fx[:,0]).reshape(ny,1)
    L = L+ny
    # interior partials in x, central difference
    if nx>2:
        [indx,indy] = np.meshgrid(np.linspace(2, nx-1, num=nx-2),np.linspace(1, ny, num=ny))
        indx = indx.T.reshape((nx-2)*ny,1)
        indy = indy.T.reshape((nx-2)*ny,1)
        ind = indy + (indx-1)*ny
        m = ny*(nx-2)

        rind = np.matlib.repmat(L+np.atleast_2d((np.linspace(1, m, num=m))).T,1,2)
        cind = np.concatenate((ind-ny,ind+ny),1)
        
        dfdx = 1/((dx[np.array(list(map(int, indx)))-2])+(dx[np.array(list(map(int, indx)))-1]))
        dfdx = dfdx*[-1, 1]

        Af[(L+np.linspace(1, m, num=m)-1).astype(int), :] = np.concatenate((rind,cind,dfdx),1)
        [row,col] = np.unravel_index(np.array(list(map(int, ind)))-1, fx.shape, 'F')
        rhs[(L+np.linspace(1, m, num=m)-1).astype(int)] = (fx[row,col]).reshape((nx-2)*ny,1)

        L = L+m
    # do the trailing edge in x, backward difference
    indx = nx
    indy = np.atleast_2d((np.linspace(1, ny, num=ny))).T
    ind = indy + (indx-1)*ny
    rind = np.matlib.repmat(L+indy,1,2)
    cind = np.concatenate((ind-ny,ind),1)
    dfdx = np.matlib.repmat([-1, 1]/dx[-1],ny,1)
    Af[(L+np.linspace(1, ny, num=ny)-1).astype(int), :] = np.concatenate((rind,cind,dfdx),1)
    rhs[(L+np.linspace(1, ny, num=ny)-1).astype(int)] = (fx[:,-1]).reshape(ny,1)
    L = L+ny
    # do the leading edge in y, forward difference
    indx = np.atleast_2d((np.linspace(1, nx, num=nx))).T
    indy = 1
    ind = indy + (indx-1)*ny
    rind = np.matlib.repmat(L+indx,1,2)
    cind = np.concatenate((ind,ind+1),1)
    dfdy = np.matlib.repmat([-1, 1]/dy[0],nx,1)
    Af[(L+np.linspace(1, nx, num=nx)-1).astype(int), :] = np.concatenate((rind,cind,dfdy),1)
    rhs[(L+np.linspace(1, nx, num=nx)-1).astype(int)] = (fy[0,:]).reshape(nx,1)
    L = L+nx;
    # interior partials in y, use a central difference
    if ny>2:
        [indx,indy] = np.meshgrid(np.linspace(1, nx, num=nx),np.linspace(2, ny-1, num=ny-2))
        indx = indx.T.reshape(nx*(ny-2),1)
        indy = indy.T.reshape(nx*(ny-2),1)
        ind = indy + (indx-1)*ny
        m = nx*(ny-2)

        rind = np.matlib.repmat(L+np.atleast_2d((np.linspace(1, m, num=m))).T,1,2)
        cind = np.concatenate((ind-1,ind+1),1)

        dfdy = 1/((dy[np.array(list(map(int, indy)))-2])+(dy[np.array(list(map(int, indy)))-1]))
        dfdy = dfdy*[-1, 1]

        Af[(L+np.linspace(1, m, num=m)-1).astype(int), :] = np.concatenate((rind,cind,dfdy),1)
        [row,col] = np.unravel_index(np.array(list(map(int, ind)))-1, fy.shape, 'F')
        rhs[(L+np.linspace(1, m, num=m)-1).astype(int)] = (fy[row,col]).reshape(nx*(ny-2),1)

        L = L+m
    # do the trailing edge in y, backward difference
    indx = np.atleast_2d((np.linspace(1, nx, num=nx))).T
    indy = ny
    ind = indy + (indx-1)*ny
    rind = np.matlib.repmat(L+indx,1,2)
    cind = np.concatenate((ind-1,ind),1)
    dfdy = np.matlib.repmat([-1, 1]/dy[-1],nx,1)
    Af[(L+np.linspace(1, nx, num=nx)-1).astype(int), :] = np.concatenate((rind,cind,dfdy),1)
    rhs[(L+np.linspace(1, nx, num=nx)-1).astype(int)] = (fy[-1,:]).reshape(nx,1)
    # finally, we can build the rest of A itself, in its sparse form.
    Af_row = Af[:,[0, 1]].reshape((4*nx*ny,1), order='F').flatten()
    Af_col = Af[:,[2, 3]].reshape((4*nx*ny,1), order='F').flatten()
    Af_data = Af[:,[4, 5]].reshape((4*nx*ny,1), order='F').flatten()
    A = csr_matrix((Af_data, (Af_row-1,Af_col-1)), shape=(2*nx*ny,nx*ny)).toarray()
    # Finish up with f11, the constant of integration.
    # eliminate the first unknown, as f11 is given.
    rhs = rhs - A[:,[0]]*f11
    # Solve the final system of equations. They will be of
    # full rank, due to the explicit integration constant.
    # Just use sparse \
    fhat_lstsq = np.linalg.lstsq(A[:,1:], rhs, rcond=None)
    fhat = np.insert(fhat_lstsq[0],0,[f11]).reshape((ny,nx), order='F')

    return fhat


def export_2d_data(i_d, i_q, map_2d, filename):
    """
    This function exports 2d maps in text files compatible with MATLAB (load, readtable).
    2d maps are the fluxes, the torque, the inductances, the inverse incremental inductances...
    """

    # extract breakpoints
    id_breakpoints = i_d[:,0]
    iq_breakpoints = i_q[0,:]

    # avoid missing .txt extension in filename
    filename = filename.replace(".txt", "")
    filename = filename + ".txt"

    np.savetxt("id_breakpoints.txt", id_breakpoints, fmt='%1.10e')
    np.savetxt("iq_breakpoints.txt", iq_breakpoints, fmt='%1.10e')
    np.savetxt(filename, map_2d, fmt='%1.10e')

    print('created "id_breakpoints.txt", "iq_breakpoints.txt", "', filename, '"')


def export_gnuplot_1d_data(vector_1, vector_2, filename):
    """
    This function exports 1d maps in text files compatible with gnuplot.
    1d maps are for example the trajectories (REF, MTPA and t1)
    """
    mat = np.vstack((vector_1, vector_2)).T

    # avoid missing .txt extension in filename
    filename = filename.replace(".txt", "")
    filename = filename + ".txt"

    np.savetxt(filename, mat, fmt='%1.10e')
    print('created "', filename, '"')


def export_gnuplot_2d_data(i_d, i_q, map_2d, filename):
    """
    This function exports 2d maps in text files compatible with gnuplot
    (add a blank line at the end of every row).
    2d maps are the fluxes, the torque, the inductances, the inverse incremental inductances...
    """
    X = i_d.flatten()
    Y = i_q.flatten()
    Z = map_2d.flatten()
    mat = np.transpose([X, Y, Z])

    # stackoverflow 752308
    ## Split data appending infs
    ## The infs will be removed later in order to obtain a blank line
    def split(arr, size):
        arrs = []
        while len(arr) > size:
            pice = arr[:size]
            arrs.append(pice)
            arrs.append(np.array([np.inf, np.inf, np.inf]))
            arr = arr[size:]
        arrs.append(arr)
        return arrs

    # avoid missing .txt extension in filename
    filename = filename.replace(".txt", "")
    filename = filename + ".txt"

    np.savetxt(filename, np.vstack(split(mat, i_q.shape[1])), fmt='%1.10e')

    # stackoverflow 17140886
    ## Open the saved text file. Replace infs with blanks
    ## The modified file is compatible with gnuplot
    # Read in the file
    with open(filename, 'r') as file:
        filedata = file.read()
    # Replace the target string
    filedata = filedata.replace('inf', '')
    # Write the file out again
    with open(filename, 'w') as file:
        file.write(filedata)
        
    print('created "', filename, '"')


def export_gnuplot_contour_0_data(i_d, i_q, map_2d, filename):
    """
    This function exports the isolevel 0 of 2d maps in text files compatible with gnuplot.
    It can be used on every 2d map. The level 0 is extracted from matplotlib contour plot.
    """
    cn = plt.contour(i_d, i_q, map_2d, levels=[0])
    plt.close()
    # ectract the coordinates of all the lines (closed lines and open lines)
    for i in range(len(cn.allsegs[0])):
        modified_filename = filename.replace(".txt", "")
        modified_filename = modified_filename + "_" + str(i) + ".txt"
        # print(cn.allsegs[1][i])
        np.savetxt(modified_filename, cn.allsegs[0][i], fmt='%1.10e')


## TO-DO
## 1. MTPV?
## 2. coppia-velocità? potenza-velocità? Mappe efficienza? 
